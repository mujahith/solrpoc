var routes = require('./randomDir/api/routes');
var config = require('./randomDir/api/config');
var express = require("express");
var bodyParser = require('body-parser');
var url = require('url');
var session = require('express-session');
var app = express();

var path = require('path');
var morgan = require('morgan');
var jwt = require('jsonwebtoken');
var mysql = require('mysql');
var ftl = require('node-ftl');
var freemarker = require('freemarker.js');

var request = require('request');
var multer = require("multer");
var HOST = config.host;
var PORT = config.port;
var defaultPath = "crickzz/randomDir";
var chatUserNames = [];
app.set('superSecret', config.secret);

var fm = new freemarker({
    viewRoot: path.join(process.cwd(), defaultPath + '/ftl'),
    options: {}
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(morgan('dev'));

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));

app.use(express.static(defaultPath));
var router = express.Router();

var teamStorage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, "./crickzz/randomDir/images/teams/")
    },
    filename: function(req, file, cb) {
        var fileExtn = ".png";
        var mimeType = file.mimetype;
        if (mimeType.indexOf("jpeg") > -1) {
            fileExtn = '.jpg'; //Appending .jpg	
        }
        cb(null, Date.now() + fileExtn);
    }
});

var teamUpload = multer({ storage: teamStorage });

var playerStorage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, "./crickzz/randomDir/images/players/")
    },
    filename: function(req, file, cb) {
        var fileExtn = ".png";
        var mimeType = file.mimetype;
        if (mimeType.indexOf("jpeg") > -1) {
            fileExtn = '.jpg'; //Appending .jpg	
        }
        cb(null, Date.now() + fileExtn);
    }
});

var playerUpload = multer({ storage: playerStorage });

router.post('/createuser', routes.createUser);
router.post('/login', routes.login);
router.post('/getmatchballsscore', routes.getMatchBallsScore);
router.post("/getteamsquad", routes.getTeamPlayers);
router.post("/getcurrentscorecard", routes.getCurrentScoreCard);
router.post("/getmatchscoreboard", routes.getMatchScoreboard);
router.post("/getPlayerDetails", routes.getPlayerDetails);
router.post("/serieslist", routes.seriesList);
router.post("/getteamlist", routes.getTeamList);
router.post("/getmatches", routes.getMatches);
router.post("/getteamplayers", routes.getTeamPlayers);
router.post("/getplayers", routes.getPlayers);
router.post("/getmatchplayers", routes.getMatchPlayers);

router.use(function(req, res, next) {
    var q = url.parse(req.url, true);
    fromInternal = q.query.fromInternal;
    if (!fromInternal) {
        var sess = (req.session && req.session.authToken) ? req.session : null;
        var sessStore = (req.sessionStore && req.sessionStore) ? req.sessionStore : null;
        var token = null;
        if (sess) {
            token = req.session.authToken;
        } else if (sessStore) {
            sessions = sessStore.sessions;
            var keysArray = Object.keys(sessions);
            for (var i = 0; i < keysArray.length; i++) {
                var key = keysArray[i];
                var val = sessions[key];
                value = JSON.parse(val);
                if (value["authToken"] != undefined) {
                    token = value["authToken"];
                    break;
                }
            }
        }
        if (token) {
            var decodedToken = jwt.decode(token);
            jwt.verify(token, app.get('superSecret'), function(err, decoded) {
                if (err) {
                    res.send({
                        "code": 400,
                        "message": 'Failed to authenticate token.',
                        "error": err
                    });
                } else {
                    next();
                }
            });
        } else {
            return res.status(401).send({
                messgeCode: config.responseCode["401"],
                message: 'Please login to do the process'
            });
        }
    } else {
        next();
    }
});

router.post("/logout", routes.logout);
router.post("/createseries", routes.createSeries);
router.post("/updateseries", routes.updateSeries);
router.post("/assignplayers", routes.assignPlayers);
router.post("/updatetossinfo", routes.updateTossInfo);
router.post("/updatematchscore", routes.updateMatchScore);
router.post("/registerplayer", routes.registerPlayer);
router.post("/updateplayer", routes.updatePlayer);
router.post("/registerteam", routes.registerTeam);
router.post("/updateteamlist", routes.updateTeamList);
router.post("/schedulematch", routes.scheduleMatch);
router.post("/assignmatchplayers", routes.assignMatchPlayers);
router.post("/updatebattingstatus", routes.updateBattingStatus);
router.post("/updatebowlingstatus", routes.updateBowlingStatus);
router.post("/updateballbyballscore", routes.insertScoreToMongo);
router.post("/updateTeamScoreDetails", routes.updateTeamScoreDetails);
router.post("/updateStriker", routes.updateStriker);
router.post("/changeBatsmen", routes.changeBatsmen);
router.post("/endinningsupdate", routes.endInningsUpdate);



var imgResponseHandler = function(req, res) {
    var respObj = {
        "code": "400",
        "message": "ERROR",
        "messageCode": "Image upload failed"
    };

    if (req.file && req.file.path) {
        respObj.code = 200;
        respObj.message = "Success";
        respObj.messageCode = "SUCCESS";
        respObj.imagepath = req.file.path.replace(/\\/g, "/").split("crickzz/randomDir")[1];
    }

    res.send(respObj);
};

router.post("/uploadteamimg", teamUpload.single('image'), imgResponseHandler);

router.post("/uploadplayerimg", playerUpload.single('image'), imgResponseHandler);

app.use('/api', router);


app.use("/login", function(req, res) {
    var token = (req.session && req.session.authToken) ? req.session.authToken : null;
    var renderLoginFtl = function() {
        ftl.processTemplate({
            data: {},
            settings: {
                encoding: 'utf-8',
                viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
            },
            filename: 'login.ftl'
        }).on('end', function(err, html) {
            res.send(html);
        });
    };
    if (token) {
        jwt.verify(token, app.get('superSecret'), function(err, decoded) {
            if (err) {
                renderLoginFtl();
            } else {
                res.redirect('/dashboard');
            }
        });
    } else {
        renderLoginFtl();
    }
});

app.use("/teamSquad", function(req, res) {
    var pathUrl = (req._parsedOriginalUrl && req._parsedOriginalUrl.path) ? req._parsedOriginalUrl.path : req.url;
    var q = url.parse(pathUrl, true);
    if (q.query) {
        var teamId = q.query.teamId ? q.query.teamId : "";
        var seriesId = q.query.seriesId ? q.query.seriesId : "";
    }
    var options = {
        url: "http://" + HOST + ":" + PORT + '/api/getteamplayers?fromInternal=' + true,
        method: 'POST',
        body: {
            "teamId": teamId ? teamId : "",
            "seriesId": seriesId ? seriesId : ""
        },
        json: true
    }
    request(options, function(error, response, jsonObj) {
        if (error) {
            res.send(error);
        } else {
            var dataObj = {}
            dataObj.teamSquadResp = { "obj": jsonObj, "str": JSON.stringify(jsonObj) }
            if (jsonObj.code == 200) {
                fm.render("teamSquad.ftl", dataObj, function(err, html, output) {
                    res.send(html);
                });
            } else {
                res.send(jsonObj);
            }
        }
    });
});

app.use("/teamlist", function(req, res) {
    ftl.processTemplate({
        data: {},
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'teamList.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});

app.use("/pointstable", function(req, res) {
    ftl.processTemplate({
        data: {},
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'pointsTable.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});

app.use("/gallery", function(req, res) {
    ftl.processTemplate({
        data: {},
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'gallery.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});

app.use("/schedules", function(req, res) {
    ftl.processTemplate({
        data: {},
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'clientMatchSchedule.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});

app.use("/profiles", function(req, res) {
    var pathUrl = (req._parsedOriginalUrl && req._parsedOriginalUrl.path) ? req._parsedOriginalUrl.path : req.url;
    var q = url.parse(pathUrl, true);
    if (q.query) {
        var playerid = q.query.playerId ? q.query.playerId : "";
    }
    var options = {
        url: "http://" + HOST + ":" + PORT + '/api/getPlayerDetails',
        method: 'POST',
        body: {
            "playerid": playerid ? playerid : "1"
        },
        json: true
    }
    request(options, function(error, response, jsonObj) {
        if (error) {
            res.send(error);
        } else {
            var dataObj = {}
            dataObj.data = { "obj": jsonObj, "str": JSON.stringify(jsonObj) }
            if (jsonObj.code == 200) {
                fm.render("clientPlayerProfile.ftl", dataObj, function(err, html, output) {
                    res.send(html);
                });
            } else {
                res.send(jsonObj);
            }
        }
    });
});
app.use("/scorecard", function(req, res) {
    ftl.processTemplate({
        data: {},
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'scorecard.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});
/*app.use("/livescore.html", function(req, res) {
    var pathUrl = (req._parsedOriginalUrl && req._parsedOriginalUrl.path) ? req._parsedOriginalUrl.path : req.url;
    var q = url.parse(pathUrl, true);
    res.sendFile(__dirname + '/randomDir/pages' + q.pathname);
});*/
app.use(config.clientPages, function(req, res) {
    var pathUrl = (req._parsedOriginalUrl && req._parsedOriginalUrl.path) ? req._parsedOriginalUrl.path : req.url;
    var q = url.parse(pathUrl, true);
    res.sendFile(__dirname + '/randomDir/pages' + q.pathname + ".html");
});

app.use(function(req, res, next) {
    var token = (req.session && req.session.authToken) ? req.session.authToken : null;
    if (token) {
        var decodedToken = jwt.decode(token);
        jwt.verify(token, app.get('superSecret'), function(err, decoded) {
            if (err) {
                res.redirect('/login');
            } else {
                var role = (req.session && req.session.role) ? req.session.role : null;
                var path = req.originalUrl;
                if (role == "admin") {
                    if ((config.adminRegexUrl).match(path)) {
                        next();
                    } else {
                        res.redirect('/dashboard');
                    }
                } else if (role == "teamUser") {
                    if ((config.teamUserRegexUrl).match(path)) {
                        next();
                    } else {
                        res.redirect('/dashboard');
                    }
                } else {
                    next();
                }
            }
        });
    } else {
        res.redirect('/home');
    }
});

app.use("/dashboard", function(req, res) {
    var role = (req.session && req.session.role) ? req.session.role : null;
    var jsonObj = {};
    if (role == "superadmin") {
        jsonObj = { "data": config.superAdmin };
    } else if (role == "teamUser") {
        jsonObj = { "data": config.teamUser };
    } else {
        jsonObj = { "data": config.admin };
    }
    ftl.processTemplate({
        data: jsonObj,
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'dashboard.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});

app.use("/createseries", function(req, res) {
    ftl.processTemplate({
        data: {},
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'createSeries.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});

app.use("/playeralloc", function(req, res) {
    sess = req.session;
    var options = {
        url: "http://" + HOST + ":" + PORT + '/api/getteamplayers?fromInternal=' + true,
        method: 'POST',
        body: {
            "seriesId": sess.seriesId ? sess.seriesId : "",
            "teamId": sess.teamId ? sess.teamId : ""
        },
        json: true
    }
    request(options, function(error, response, jsonObj) {
        if (error) {
            res.send(error);
        } else {
            var dataObj = {}
            dataObj.data = { "obj": jsonObj, "str": JSON.stringify(jsonObj) }
            if (jsonObj.code == 200) {
                fm.render("playeralloc.ftl", dataObj, function(err, html, output) {
                    res.send(html);
                });

            } else {
                res.send(jsonObj);
            }
        }
    });
});

app.use("/selectmatch", function(req, res) {
    ftl.processTemplate({
        data: {},
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'selectmatch.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});

app.use("/startinnings", function(req, res) {
    ftl.processTemplate({
        data: {},
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'startinnings.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});

app.use("/updatescore", function(req, res) {
    var matchplayerConfig = {
        url: "http://" + HOST + ":" + PORT + '/api/getmatchplayers',
        method: 'POST',
        body: {
            "matchId": req.query.matchid
        },
        json: true
    }
    request(matchplayerConfig, function(error, response, jsonObj) {
        if (error) {
            res.send(error);
        } else {
            var dataObj = {}
            dataObj.matchplayerResp = { "obj": jsonObj, "str": JSON.stringify(jsonObj) }
            if (jsonObj.code == 200) {
                var getmatchesConfig = {
                    url: "http://" + HOST + ":" + PORT + '/api/getmatches',
                    method: 'POST',
                    body: {
                        "matchId": req.query.matchid
                    },
                    json: true
                }
                request(getmatchesConfig, function(error, response, jsonObj) {
                    if (error) {
                        res.send(error);
                    } else {
                        dataObj.getmatchesResp = { "obj": jsonObj, "str": JSON.stringify(jsonObj) }
                        if (jsonObj.code == 200) {
                            fm.render("updatescore.ftl", dataObj, function(err, html, output) {
                                res.send(html);
                            });
                        } else {
                            res.send(jsonObj);
                        }
                    }
                });
            } else {
                res.send(jsonObj);
            }
        }
    });
});

app.use("/matchinfo", function(req, res) {
    ftl.processTemplate({
        data: {},
        settings: {
            encoding: 'utf-8',
            viewFolder: path.join(process.cwd(), defaultPath + '/ftl')
        },
        filename: 'matchinfo.ftl'
    }).on('end', function(err, html) {
        res.send(html);
    });
});
/*
app.use("/ballByballscore.html", function(req, res) {
    var pathUrl = (req._parsedOriginalUrl && req._parsedOriginalUrl.path) ? req._parsedOriginalUrl.path : req.url;
    var q = url.parse(pathUrl, true);
    res.sendFile(__dirname + '/randomDir/pages' + q.pathname);
});*/
app.use(config.superAdminHtmlRegexUrl, function(req, res) {
    var pathUrl = (req._parsedOriginalUrl && req._parsedOriginalUrl.path) ? req._parsedOriginalUrl.path : req.url;
    var q = url.parse(pathUrl, true);
    res.sendFile(__dirname + '/randomDir/pages' + q.pathname + ".html");
});

app.use("/", function(req, res) {
    var path = req.originalUrl;
    res.sendFile(__dirname + '/randomDir/pages' + path + '.html');
});

/*var server = app.listen(, HOST, function() {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)

})*/
var ioProm = require('express-socket.io');
var server = ioProm.init(app);
server.listen(PORT, HOST, function() {
    console.log('Server listening  http://%s:%s', HOST, PORT)
});

ioProm.then(function(io) {
    io.on('connection', function(socket) {
        console.log('Connected!');
        socket.on('initialload', function(data) {
            var getCurrScorecardCbk = function(data) {
                if (data.error) {
                    socket.emit('updatescore', "ERROR");
                } else {
                    socket.emit('updatescore', data);
                }
            }
            routes.getCurrentScoreCardIO(data, getCurrScorecardCbk);
        });

        socket.on('updateplayercbk', function(data) {
            var getCurrScorecardCbk = function(data) {
                if (data.error) {
                    io.emit('updatescore', "ERROR");
                } else {
                    io.emit('updatescore', data);
                }
            }
            routes.getCurrentScoreCardIO(data, getCurrScorecardCbk);
        });

        socket.on('updatematchscore', function(data) {
            var insertScoreToMongoCbk = function(data) {
                if (data.error) {
                    io.emit('updatescore', "ERROR");
                } else {
                    io.emit('updatescore', data);
                    var getLiveMatchesCbk = function(data) {
                        if (data.error) {
                            io.emit('updatelivescore', "ERROR");
                        } else {
                            io.emit('updatelivescore', data);
                        }
                    }
                    routes.getLiveMatchesIO(data, getLiveMatchesCbk);
                }
            }
            routes.insertScoreToMongoIO(data, insertScoreToMongoCbk);
        });
        socket.on('suitchstriker', function(obj, obj1) {
            var suitchStrikerCbk = function(data) {
                if (data.error) {
                    io.emit('updatescore', "ERROR");
                } else {
                    var getCurrScorecardCbk = function(data1) {
                        if (data1.error) {
                            io.emit('updatescore', "ERROR");
                        } else {
                            io.emit('updatescore', data1);
                        }
                    }
                    routes.getCurrentScoreCardIO(obj1, getCurrScorecardCbk);
                }
            }
            var batsman = obj.batsman ? obj.batsman : "";
            var matchId = obj.matchId ? obj.matchId : "";
            routes.updateStrikerIO(batsman, matchId, suitchStrikerCbk);
        });

        socket.on('getlivematches', function(data) {
            var getLiveMatchesCbk = function(data) {
                if (data.error) {
                    io.emit('updatelivescore', "ERROR");
                } else {
                    io.emit('updatelivescore', data);
                }
            }
            routes.getLiveMatchesIO(data, getLiveMatchesCbk);
        });

        //Chat2Me
        socket.on('new user', function(data, callback) {
            if (chatUserNames.indexOf(data) != -1) {
                callback(false);
            } else {
                callback(true);
                socket.username = data;
                chatUserNames.push(socket.username);
                updateUsernames();
            }
        });

        // Update Usernames
        function updateUsernames() {
            io.emit('usernames', chatUserNames);
        }

        // Send Message
        socket.on('send message', function(data) {
            io.emit('new message', { msg: data, user: socket.username });
        });

        // Disconnect
        socket.on('disconnect', function(data) {
            if (!socket.username) {
                return;
            }

            chatUserNames.splice(chatUserNames.indexOf(socket.username), 1);
            updateUsernames();
        });
    });
});