-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.17-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema crickzz
--

CREATE DATABASE IF NOT EXISTS crickzz;
USE crickzz;

--
-- Definition of table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(40) NOT NULL,
  `fname` varchar(45) NOT NULL,
  `lname` varchar(45) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `seriesId` varchar(20) DEFAULT NULL,
  `teamId` varchar(20) DEFAULT NULL,
  `pwd` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `username` (`id`),
  KEY `FK_admin_1` (`seriesId`),
  KEY `FK_admin_2` (`teamId`),
  CONSTRAINT `FK_admin_1` FOREIGN KEY (`seriesId`) REFERENCES `series_list` (`seriesId`),
  CONSTRAINT `FK_admin_2` FOREIGN KEY (`teamId`) REFERENCES `team_list` (`teamId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` (`id`,`email`,`fname`,`lname`,`username`,`password`,`role`,`created`,`seriesId`,`teamId`,`pwd`) VALUES 
 (14,'CSK_SCL-2018@mailinator.com','CSK','CSK','cskscl-2018','$2a$05$3xLVrVY/GnJgoU2JPBZmHOSwEOq4uDxqtlEyPlQ0OD7P1T6J.tPAu','teamUser','2018-01-23 13:59:47','SCL-2018','CSK-SCL-2018','CSK152'),
 (18,'DDD_SCL-2018@mailinator.com','DDD','DDD','dddscl-2018','$2a$05$izhhXy2vNyDbxZ9/TKi3Nu9L22n4gighOv0m4JjiDDvz748PCJqzu','teamUser','2018-01-25 18:02:36','SCL-2018','DDD-SCL-2018','DDD735'),
 (15,'KXP_SCL-2018@mailinator.com','KXP','KXP','kxpscl-2018','$2a$05$zYqN1uMm3SpFYF74vCjlf.iLjH7G4frpV5fc3blOGeGm/HI4zq9pC','teamUser','2018-01-23 14:02:57','SCL-2018','KXP-SCL-2018','KXP193'),
 (16,'MI_SCL-2018@mailinator.com','MI','MI','miscl-2018','$2a$05$3oXMXh6ETxLTmNOObjnl4.99ubuiAdCv2CPiCv1rArJKKB5Ds/U7C','teamUser','2018-01-23 14:03:11','SCL-2018','MI-SCL-2018','MI452'),
 (17,'RCB_SCL-2018@mailinator.com','RCB','RCB','rcbscl-2018','$2a$05$eIkN13.tapw5POVv.0dzjep9Ub8vnTmHeg1dQrT8QnGiJF5eTAnMe','teamUser','2018-01-23 14:03:34','SCL-2018','RCB-SCL-2018','RCB893'),
 (1,'solrsudo@skava.com','SolR','SU','solrsudo','$2a$05$sNRfKvhayqCWPsvt95kfj.d1yBeAndEGadntagUk7YzQVgnI8dD8.','superadmin','2018-01-17 17:53:41',NULL,NULL,NULL);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;


--
-- Definition of table `batting_career`
--

DROP TABLE IF EXISTS `batting_career`;
CREATE TABLE `batting_career` (
  `playerId` int(11) NOT NULL,
  `matches` int(11) NOT NULL DEFAULT '0',
  `innings` int(11) NOT NULL DEFAULT '0',
  `notout` int(11) NOT NULL DEFAULT '0',
  `runs` int(11) NOT NULL DEFAULT '0',
  `balls` int(11) NOT NULL DEFAULT '0',
  `hscore` int(11) NOT NULL DEFAULT '0',
  `fours` int(11) NOT NULL DEFAULT '0',
  `sixes` int(11) NOT NULL DEFAULT '0',
  `30plus` int(11) NOT NULL DEFAULT '0',
  `50plus` int(11) NOT NULL DEFAULT '0',
  `100plus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`playerId`),
  CONSTRAINT `FK_batting_profile_1` FOREIGN KEY (`playerId`) REFERENCES `player_profile` (`playerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `batting_career`
--

/*!40000 ALTER TABLE `batting_career` DISABLE KEYS */;
INSERT INTO `batting_career` (`playerId`,`matches`,`innings`,`notout`,`runs`,`balls`,`hscore`,`fours`,`sixes`,`30plus`,`50plus`,`100plus`) VALUES 
 (1,0,0,0,0,0,0,0,0,0,0,0),
 (10,1,1,1,10,3,0,0,0,0,0,0),
 (11,1,1,1,0,0,0,0,0,0,0,0),
 (12,1,0,1,0,0,0,0,0,0,0,0),
 (13,1,1,1,0,0,0,0,0,0,0,0),
 (14,1,1,1,0,0,0,0,0,0,0,0),
 (15,1,1,1,6,1,0,0,0,0,0,0),
 (16,1,0,1,0,0,0,0,0,0,0,0),
 (17,0,0,0,0,0,0,0,0,0,0,0),
 (18,1,0,1,0,0,0,0,0,0,0,0),
 (19,1,0,1,0,0,0,0,0,0,0,0),
 (20,1,0,1,0,0,0,0,0,0,0,0),
 (21,1,0,1,0,0,0,0,0,0,0,0),
 (22,1,0,1,0,0,0,0,0,0,0,0),
 (25,1,1,0,21,7,0,0,1,0,0,0),
 (27,1,0,1,0,0,0,0,0,0,0,0),
 (28,1,1,0,6,4,0,0,0,0,0,0),
 (29,1,0,1,0,0,0,0,0,0,0,0),
 (30,1,0,1,0,0,0,0,0,0,0,0),
 (31,1,1,1,25,7,0,2,0,0,0,0),
 (32,1,0,1,0,0,0,0,0,0,0,0),
 (33,1,0,1,0,0,0,0,0,0,0,0),
 (34,1,1,1,12,2,0,0,0,0,0,0),
 (35,1,1,1,0,0,0,0,0,0,0,0),
 (36,1,0,1,0,0,0,0,0,0,0,0),
 (37,1,0,1,0,0,0,0,0,0,0,0),
 (38,1,0,1,0,0,0,0,0,0,0,0),
 (39,1,1,0,0,1,0,0,0,0,0,0),
 (40,1,1,1,8,1,0,0,0,0,0,0),
 (41,1,0,1,0,0,0,0,0,0,0,0),
 (42,1,1,1,0,0,0,0,0,0,0,0),
 (43,1,1,1,0,0,0,0,0,0,0,0),
 (44,0,0,0,0,0,0,0,0,0,0,0),
 (45,1,0,1,0,0,0,0,0,0,0,0),
 (46,1,0,1,0,0,0,0,0,0,0,0),
 (47,0,0,0,0,0,0,0,0,0,0,0),
 (48,0,0,0,0,0,0,0,0,0,0,0),
 (49,1,0,1,0,0,0,0,0,0,0,0),
 (50,1,0,1,0,0,0,0,0,0,0,0),
 (51,1,0,1,0,0,0,0,0,0,0,0),
 (52,1,0,1,0,0,0,0,0,0,0,0),
 (53,0,0,0,0,0,0,0,0,0,0,0),
 (57,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `batting_career` ENABLE KEYS */;


--
-- Definition of table `bowling_career`
--

DROP TABLE IF EXISTS `bowling_career`;
CREATE TABLE `bowling_career` (
  `playerId` int(11) NOT NULL,
  `matches` int(11) DEFAULT '0',
  `innings` int(11) DEFAULT '0',
  `balls` int(11) DEFAULT '0',
  `runs` int(11) DEFAULT '0',
  `wickets` int(11) DEFAULT '0',
  `bbm` varchar(10) DEFAULT '0',
  `3plus` int(11) DEFAULT '0',
  `5plus` int(11) DEFAULT '0',
  PRIMARY KEY (`playerId`),
  CONSTRAINT `FK_bowling_career_1` FOREIGN KEY (`playerId`) REFERENCES `player_profile` (`playerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bowling_career`
--

/*!40000 ALTER TABLE `bowling_career` DISABLE KEYS */;
INSERT INTO `bowling_career` (`playerId`,`matches`,`innings`,`balls`,`runs`,`wickets`,`bbm`,`3plus`,`5plus`) VALUES 
 (1,0,0,0,0,0,'0',0,0),
 (10,1,1,1,6,0,'0/0',0,0),
 (11,1,0,1,0,1,'0/1',0,0),
 (12,1,0,0,0,0,'0/0',0,0),
 (13,1,0,0,0,0,'0/0',0,0),
 (14,1,0,16,52,1,'52/1',0,0),
 (15,1,0,1,4,0,'0/0',0,0),
 (16,1,0,0,0,0,'0/0',0,0),
 (17,0,0,0,0,0,'0',0,0),
 (18,1,0,0,0,0,'0/0',0,0),
 (19,1,0,7,26,1,'26/1',0,0),
 (20,1,0,0,0,0,'0/0',0,0),
 (21,1,0,0,0,0,'0/0',0,0),
 (22,1,0,0,0,0,'0/0',0,0),
 (25,1,0,0,0,0,'0/0',0,0),
 (27,1,0,0,0,0,'0/0',0,0),
 (28,1,0,0,0,0,'0/0',0,0),
 (29,1,0,0,0,0,'0/0',0,0),
 (30,1,0,0,0,0,'0/0',0,0),
 (31,1,0,0,0,0,'0/0',0,0),
 (32,1,0,0,0,0,'0/0',0,0),
 (33,1,0,0,0,0,'0/0',0,0),
 (34,1,0,0,0,0,'0/0',0,0),
 (35,1,0,0,0,0,'0/0',0,0),
 (36,1,0,0,0,0,'0/0',0,0),
 (37,1,0,0,0,0,'0/0',0,0),
 (38,1,0,0,0,0,'0/0',0,0),
 (39,1,0,0,0,0,'0/0',0,0),
 (40,1,0,0,0,0,'0/0',0,0),
 (41,1,0,0,0,0,'0/0',0,0),
 (42,1,0,0,0,0,'0/0',0,0),
 (43,1,0,0,0,0,'0/0',0,0),
 (44,0,0,0,0,0,'0',0,0),
 (45,1,0,0,0,0,'0/0',0,0),
 (46,1,0,0,0,0,'0/0',0,0),
 (47,0,0,0,0,0,'0',0,0),
 (48,0,0,0,0,0,'0',0,0),
 (49,1,0,0,0,0,'0/0',0,0),
 (50,1,0,0,0,0,'0/0',0,0),
 (51,1,0,0,0,0,'0/0',0,0),
 (52,1,0,0,0,0,'0/0',0,0),
 (53,0,0,0,0,0,'0',0,0),
 (57,0,0,0,0,0,'0',0,0);
/*!40000 ALTER TABLE `bowling_career` ENABLE KEYS */;


--
-- Definition of table `match_schedule`
--

DROP TABLE IF EXISTS `match_schedule`;
CREATE TABLE `match_schedule` (
  `matchId` int(11) NOT NULL AUTO_INCREMENT,
  `matchNo` varchar(30) DEFAULT NULL,
  `dateTime` datetime NOT NULL,
  `venue` varchar(200) NOT NULL,
  `teamAId` varchar(20) NOT NULL,
  `teamBId` varchar(20) NOT NULL,
  `seriesId` varchar(20) DEFAULT NULL,
  `matchType` varchar(30) DEFAULT NULL,
  `matchBalls` int(11) DEFAULT NULL,
  `maxBalls` int(11) DEFAULT NULL,
  `noBowForMaxOvers` int(11) DEFAULT NULL,
  `scorer` varchar(45) DEFAULT NULL,
  `umpire1` varchar(45) DEFAULT NULL,
  `umpire2` varchar(45) DEFAULT NULL,
  `tossWonBy` varchar(20) DEFAULT NULL,
  `electedToBat` tinyint(1) DEFAULT NULL,
  `teamAScore` int(11) DEFAULT '0',
  `teamAWicket` int(11) DEFAULT '0',
  `teamABalls` int(11) DEFAULT '0',
  `teamAInngStatus` tinyint(1) DEFAULT '0',
  `teamBScore` int(11) DEFAULT '0',
  `teamBWicket` int(11) DEFAULT '0',
  `teamBBalls` int(11) DEFAULT '0',
  `teamBInngStatus` tinyint(1) DEFAULT '0',
  `teamWon` varchar(20) DEFAULT NULL,
  `matchStatus` varchar(100) DEFAULT NULL,
  `mom` int(11) DEFAULT NULL,
  PRIMARY KEY (`matchId`),
  UNIQUE KEY `duplicateMatchNo` (`matchNo`,`seriesId`),
  KEY `teamAId` (`teamAId`),
  KEY `teamBId` (`teamBId`),
  KEY `seriesId` (`seriesId`),
  KEY `mom` (`mom`),
  KEY `teamWon` (`teamWon`),
  KEY `match_schedule_ibfk_6` (`tossWonBy`),
  CONSTRAINT `match_schedule_ibfk_1` FOREIGN KEY (`teamAId`) REFERENCES `team_list` (`teamId`),
  CONSTRAINT `match_schedule_ibfk_2` FOREIGN KEY (`teamBId`) REFERENCES `team_list` (`teamId`),
  CONSTRAINT `match_schedule_ibfk_3` FOREIGN KEY (`seriesId`) REFERENCES `series_list` (`seriesId`),
  CONSTRAINT `match_schedule_ibfk_4` FOREIGN KEY (`mom`) REFERENCES `player_profile` (`playerId`),
  CONSTRAINT `match_schedule_ibfk_5` FOREIGN KEY (`teamWon`) REFERENCES `team_list` (`teamId`),
  CONSTRAINT `match_schedule_ibfk_6` FOREIGN KEY (`tossWonBy`) REFERENCES `team_list` (`teamId`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `match_schedule`
--

/*!40000 ALTER TABLE `match_schedule` DISABLE KEYS */;
INSERT INTO `match_schedule` (`matchId`,`matchNo`,`dateTime`,`venue`,`teamAId`,`teamBId`,`seriesId`,`matchType`,`matchBalls`,`maxBalls`,`noBowForMaxOvers`,`scorer`,`umpire1`,`umpire2`,`tossWonBy`,`electedToBat`,`teamAScore`,`teamAWicket`,`teamABalls`,`teamAInngStatus`,`teamBScore`,`teamBWicket`,`teamBBalls`,`teamBInngStatus`,`teamWon`,`matchStatus`,`mom`) VALUES 
 (5,'league 1','2018-03-02 10:00:00','NGP','MI-SCL-2018','RCB-SCL-2018','SCL-2018','league',30,6,5,'fgg','','','MI-SCL-2018',0,0,0,0,0,0,0,0,0,NULL,NULL,NULL),
 (6,'league 2','2018-03-02 11:00:00','NGP','MI-SCL-2018','KXP-SCL-2018','SCL-2018','league',33,6,1,'ganth','sasi','sasi','KXP-SCL-2018',0,0,0,0,0,0,0,0,0,NULL,NULL,NULL),
 (8,'league 3','2018-03-02 13:00:00','NGP','KXP-SCL-2018','RCB-SCL-2018','SCL-2018','league',46,6,2,'sasi','liyas','balaji','RCB-SCL-2018',1,0,0,0,0,0,0,0,0,NULL,NULL,NULL),
 (11,'league 4','2018-01-24 08:00:00','ngp','KXP-SCL-2018','MI-SCL-2018','SCL-2018','league',40,6,3,'naveen','bro','liyas','MI-SCL-2018',0,0,0,0,0,0,0,0,0,NULL,NULL,NULL),
 (12,'league 5','2018-01-24 10:00:00','ngp','KXP-SCL-2018','RCB-SCL-2018','SCL-2018','league',NULL,NULL,1,'dfh','dfhg','dfh','RCB-SCL-2018',0,0,1,1,1,0,0,0,1,'KXP-SCL-2018','sdsds',11),
 (13,'league 6','2018-01-24 12:00:00','ngp','MI-SCL-2018','RCB-SCL-2018','SCL-2018','league',60,12,5,'c','a','b','MI-SCL-2018',0,0,0,0,0,0,2,4,0,NULL,NULL,NULL),
 (14,'league 7','0001-01-11 01:01:00','NGP','MI-SCL-2018','RCB-SCL-2018','SCL-2018','league',56,12,2,'jj','jjj','jj','MI-SCL-2018',1,48,11,19,1,40,0,12,1,'MI-SCL-2018','DCS won by 8 runs',12),
 (15,'league 8','2018-02-28 08:00:00','PSG','MI-SCL-2018','RCB-SCL-2018','SCL-2018','league',60,12,5,'sasi','naveen','liyas','MI-SCL-2018',1,46,1,30,1,0,0,0,0,NULL,NULL,NULL),
 (16,'league 9','2018-02-24 10:10:00','PSG','RCB-SCL-2018','MI-SCL-2018','SCL-2018','league',60,12,5,'naveen','liyas','sasi','RCB-SCL-2018',1,0,0,0,0,0,0,0,0,NULL,NULL,NULL),
 (17,'league 10','2018-02-22 08:08:00','NGP','MI-SCL-2018','KXP-SCL-2018','SCL-2018','league',212,54,11,'jtygu','tryhrt','yjut','MI-SCL-2018',0,6,0,1,1,82,2,24,1,'MI-SCL-2018','trdt',12),
 (18,'league 24','2018-02-01 14:22:00','Coimbatore','KXP-SCL-2018','RCB-SCL-2018','SCL-2018','league',98,36,8,'anu','anushiya','anush','KXP-SCL-2018',0,0,0,0,1,0,0,1,1,'KXP-SCL-2018','aasas',10),
 (19,'league 13','2018-03-01 10:00:00','NGP','KXP-SCL-2018','MI-SCL-2018','SCL-2018','league',18,6,3,'Siva','Vinoth','Vaidhees','KXP-SCL-2018',1,0,0,0,1,0,0,0,1,'MI-SCL-2018','Win',10),
 (20,'league 17','2018-03-05 08:00:00','PSG','MI-SCL-2018','RCB-SCL-2018','SCL-2018','league',18,6,3,'Siva & Mohan','Vinoth','Vaidhees','MI-SCL-2018',0,21,6,18,1,42,0,18,1,NULL,NULL,NULL),
 (21,'league 20','2018-11-11 13:01:00','POY','MI-SCL-2018','KXP-SCL-2018','SCL-2018','league',60,12,5,'abc','abc','abc','MI-SCL-2018',0,0,0,0,0,41,1,14,1,NULL,NULL,NULL),
 (22,'league 100','1111-11-11 11:11:00','AAA','RCB-SCL-2018','KXP-SCL-2018','SCL-2018','league',60,30,5,'sa','a','s','RCB-SCL-2018',0,0,0,0,0,0,0,0,1,NULL,NULL,NULL),
 (23,'league 121','2012-08-08 23:22:00','NGP','MI-SCL-2018','KXP-SCL-2018','SCL-2018','league',19,12,1,'d','d','d','KXP-SCL-2018',1,0,0,0,1,0,0,0,1,NULL,NULL,NULL),
 (24,'knockout 1','2018-03-31 10:00:00','Melbourne cricket ground','KXP-SCL-2018','MI-SCL-2018','SCL-2018','knockout',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0,0,NULL,NULL,NULL),
 (25,'knockout 2','2018-04-08 17:09:00','melbourne ground','KXP-SCL-2018','RCB-SCL-2018','SCL-2018','knockout',24,12,1,'r','r','r','RCB-SCL-2018',1,0,0,0,0,36,0,14,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `match_schedule` ENABLE KEYS */;


--
-- Definition of table `player_profile`
--

DROP TABLE IF EXISTS `player_profile`;
CREATE TABLE `player_profile` (
  `playerId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(45) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `dob` datetime NOT NULL,
  `role` varchar(20) NOT NULL,
  `batStyle` varchar(20) NOT NULL,
  `bowlStyle` varchar(30) NOT NULL,
  `playerImg` varchar(210) DEFAULT NULL,
  PRIMARY KEY (`playerId`),
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `player_profile`
--

/*!40000 ALTER TABLE `player_profile` DISABLE KEYS */;
INSERT INTO `player_profile` (`playerId`,`name`,`email`,`phone`,`dob`,`role`,`batStyle`,`bowlStyle`,`playerImg`) VALUES 
 (1,'unstated','unstated@mailinator.com','0000000000','2018-01-01 00:00:00','All Rounder','Left Handed Batsman','Right-arm fast','/images/players/1516694242228.png'),
 (10,'Karthik Devaraj','dk@skava.com','3432432432','1988-03-14 00:00:00','batsman','Right Handed Batsman','Right-arm orthodox','/images/players/1516694721132.jpg'),
 (11,'Virat Kohli','kohli@gmail.com','3465679800','1990-06-12 00:00:00','batsman','Right Handed Batsman','Right-arm medium','/images/players/1516695086385.jpg'),
 (12,'Rohit Sharma','rohitman@mailinator.com','9876541233','1987-04-30 00:00:00','All Rounder','Right Handed Batsman','Right-arm Off break','/images/players/1516695095471.jpg'),
 (13,'Chris Gayle','gayle@gmail.com','3546758545','1850-04-02 00:00:00','batsman','Left Handed Batsman','Left-arm orthodox','/images/players/1516695169834.jpg'),
 (14,'Lendl Simmons','ls@mailinator.com','7898456123','0001-01-02 00:00:00','batsman','Left Handed Batsman','Right-arm fast','/images/players/1516695165349.jpg'),
 (15,'Ambati Rayudu','a@m.com','7894561233','0003-01-02 00:00:00','batsman','Left Handed Batsman','Right-arm fast','/images/players/1516695294056.jpg'),
 (16,'Nitish Rana','n@m.com','9876543213','0003-03-03 00:00:00','All Rounder','Left Handed Batsman','Right-arm fast','/images/players/1516695321427.jpg'),
 (17,'test','test@test.com','7790796767','1997-03-06 00:00:00','batsman','Right Handed Batsman','Left-arm medium','/images/players/1516695334933.png'),
 (18,'Sarfaraz khan','khan@gmail.com','6579876543','1997-08-09 00:00:00','batsman','Right Handed Batsman','Right-arm orthodox','/images/players/1516695360365.jpg'),
 (19,'Jos Buttler','j@j.com','8965555555','0005-05-05 00:00:00','All Rounder','Left Handed Batsman','Right-arm fast','/images/players/1516695350863.jpg'),
 (20,'Lasith Malinga','m@m.com','4545454545','0009-09-08 00:00:00','WK-BATSMAN','Left Handed Batsman','Right-arm fast','/images/players/1516695414255.jpg'),
 (21,'Harbhajan Singh','h@h.com','9897665454','0008-08-08 00:00:00','All Rounder','Left Handed Batsman','Right-arm fast','/images/players/1516695447706.jpg'),
 (22,'AB De Villiers','abd@gmail.com','0989304365','1886-03-04 00:00:00','batsman','Right Handed Batsman','Right-arm medium','/images/players/1516695444464.jpg'),
 (25,'vinoth kumar','vinoth@sk.com','3432432432','1988-08-08 00:00:00','All Rounder','Right Handed Batsman','Right-arm Leg break','/images/players/1516695478276.jpg'),
 (27,'Jasprit Bumrah','j@jj.com','8784524514','0005-05-05 00:00:00','All Rounder','Left Handed Batsman','Right-arm fast','/images/players/1516695474403.jpg'),
 (28,'aravinth','aravinth@sk.com','2134243243','1988-04-04 00:00:00','All Rounder','Right Handed Batsman','Right-arm medium fast','/images/players/1516695536734.jpg'),
 (29,'Yuzvendra Chahal','chahal@gmail.com','9087943657','1990-06-09 00:00:00','bowler','Right Handed Batsman','Right-arm Leg break','/images/players/1516695525974.jpg'),
 (30,'Hardik Pandya','hh@h.com','4515121212','0042-12-15 00:00:00','batsman','Left Handed Batsman','Right-arm fast','/images/players/1516695586123.jpg'),
 (31,'ashar mohamed yusuf','ashar@sk.com','2132132132','1988-11-11 00:00:00','All Rounder','Right Handed Batsman','Right-arm fast','/images/players/1516695573471.jpg'),
 (32,' Kieron Pollard','kp@j.com','4515152121','2017-12-31 00:00:00','batsman','Right Handed Batsman','Right-arm fast medium','/images/players/1516695607280.jpg'),
 (33,'Tymal Mills','millis@gmail.com','9088769768','1997-11-29 00:00:00','bowler','Right Handed Batsman','Left-arm fast medium','/images/players/1516695595842.jpg'),
 (34,'anbazhagan','anbu@sk.com','2343243243','1988-11-11 00:00:00','All Rounder','Right Handed Batsman','Right-arm fast','/images/players/1516695620453.jpg'),
 (35,'ravi','ravi@sk.com','1234214324','1988-02-22 00:00:00','batsman','Right Handed Batsman','Right-arm medium','/images/players/1516695655941.jpg'),
 (36,'Chris Jordan','jordan@gmail.com','7890878907','1989-12-30 00:00:00','bowler','Right Handed Batsman','Right-arm medium','/images/players/1516695655486.jpg'),
 (37,'Krunal Pandya','kp@k.com','5454454545','2018-12-31 00:00:00','batsman','Left Handed Batsman','Right-arm fast','/images/players/1516695657162.jpg'),
 (38,'Samuel Badree','badree@gmail.com','8979087912','1990-08-27 00:00:00','bowler','Right Handed Batsman','Right-arm Leg break','/images/players/1516695739043.jpg'),
 (39,'Parthiv Patel','p@p.com','1512121212','2018-12-31 00:00:00','batsman','Left Handed Batsman','Right-arm fast','/images/players/1516695758626.jpg'),
 (40,'mujahith','muja@sk.cim','3243243243','3199-03-31 00:00:00','player','Right Handed Batsman','Right-arm orthodox','/images/players/1516695753970.jpg'),
 (41,'David Wiese','wiese@gmail.com','4356789756','1987-08-12 00:00:00','All Rounder','Right Handed Batsman','Right-arm fast medium','/images/players/1516695797361.jpg'),
 (42,' Karn Sharma','kk@k.com','0524215151','2017-12-31 00:00:00','batsman','Right Handed Batsman','Right-arm fast','/images/players/1516695785875.jpg'),
 (43,'rajesh','rajesh@sk.com','2132132132','1988-11-11 00:00:00','All Rounder','Right Handed Batsman','Right-arm medium','/images/players/1516695793130.jpg'),
 (44,'Mitchell McClenaghan','mm@m.com','1515212151','2017-12-30 00:00:00','batsman','Left Handed Batsman','Right-arm fast','/images/players/1516695814157.jpg'),
 (45,'elangovan','elango@sk.com','3432432432','1988-02-22 00:00:00','All Rounder','Right Handed Batsman','Right-arm medium','/images/players/1516695817678.jpg'),
 (46,'Shane Watson','watson@gmail.com','3254678904','1987-08-09 00:00:00','All Rounder','Right Handed Batsman','Right-arm fast medium','/images/players/1516695848488.jpg'),
 (47,'Mitchell Johnson','jjj@jjjjj.com','4515121541','2018-12-30 00:00:00','All Rounder','Left Handed Batsman','Right-arm fast','/images/players/1516695838432.jpg'),
 (48,'Saurabh Tiwary','st@st.com','5624253215','2017-12-31 00:00:00','batsman','Left Handed Batsman','Right-arm fast','/images/players/1516695874248.jpg'),
 (49,'naveen','naveen@sk.com','2321321321','1988-11-11 00:00:00','batsman','Right Handed Batsman','Right-arm orthodox','/images/players/1516695853077.jpg'),
 (50,'Kedar Jadhav','jadhav@gmail.com','3245678965','1990-06-09 00:00:00','All Rounder','Right Handed Batsman','Right-arm Off break','/images/players/1516695923502.jpg'),
 (51,'Lokesh Rahul','rahul@gmail.com','0987079790','1989-08-09 00:00:00','WK-BATSMAN','Right Handed Batsman','Right-arm Leg break','/images/players/1516695972958.jpg'),
 (52,'kumar','kumar@sk.com','2132132132','1988-02-22 00:00:00','bowler','Right Handed Batsman','Left-arm fast','/images/players/1516696109470.jpg'),
 (53,'testPlayer','testPlayer101@skava.com','9213123123','2017-12-12 00:00:00','P','right','right','/testPlayer.img'),
 (57,'Nnnn','dsfsf@fsd.com','3443534534','1990-03-04 00:00:00','All Rounder','Right Handed Batsman','Right-arm fast','/images/players/1516703805502.jpg');
/*!40000 ALTER TABLE `player_profile` ENABLE KEYS */;


--
-- Definition of trigger `after_player_profile_insert_bowling_career`
--

DROP TRIGGER /*!50030 IF EXISTS */ `after_player_profile_insert_bowling_career`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `after_player_profile_insert_bowling_career` AFTER INSERT ON `player_profile` FOR EACH ROW BEGIN
    INSERT INTO bowling_career
    SET playerId = NEW.playerId, matches = 0, innings = 0, balls = 0, runs = 0, wickets = 0, bbm = 0, 3plus = 0, 5plus = 0;
END $$

DELIMITER ;

--
-- Definition of trigger `after_player_profile_insert_batting_career`
--

DROP TRIGGER /*!50030 IF EXISTS */ `after_player_profile_insert_batting_career`;

DELIMITER $$

CREATE DEFINER = `root`@`localhost` TRIGGER `after_player_profile_insert_batting_career` AFTER INSERT ON `player_profile` FOR EACH ROW BEGIN
	INSERT INTO batting_career
	SET playerId = NEW.playerId, matches = 0, innings = 0, notout = 0, runs = 0, balls = 0, hscore = 0, fours = 0, sixes = 0, 30plus = 0, 50plus = 0, 100plus = 0;
END $$

DELIMITER ;

--
-- Definition of table `scoreboard`
--

DROP TABLE IF EXISTS `scoreboard`;
CREATE TABLE `scoreboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matchId` int(11) NOT NULL,
  `teamId` varchar(20) NOT NULL,
  `playerId` int(11) NOT NULL,
  `elevenType` tinyint(1) DEFAULT NULL,
  `batOrder` int(11) DEFAULT '0',
  `striker` varchar(10) DEFAULT 'false',
  `batStatus` varchar(50) DEFAULT 'yet to bat',
  `bowlOrder` int(11) DEFAULT '0',
  `bowlStatus` varchar(50) DEFAULT NULL,
  `runScored` int(11) DEFAULT '0',
  `ballsFaced` int(11) DEFAULT '0',
  `fours` int(11) DEFAULT '0',
  `sixes` int(11) DEFAULT '0',
  `outType` varchar(30) DEFAULT NULL,
  `outBowler` int(11) DEFAULT NULL,
  `outBy` int(11) DEFAULT NULL,
  `ingStatus` varchar(130) DEFAULT NULL,
  `ballsBowled` int(11) DEFAULT '0',
  `maidens` int(11) DEFAULT '0',
  `runsGiven` int(11) DEFAULT '0',
  `wickets` int(11) DEFAULT '0',
  `noBalls` int(11) DEFAULT '0',
  `wides` int(11) DEFAULT '0',
  `byes` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_scoreboard_1` (`playerId`),
  KEY `FK_scoreboard_2` (`matchId`),
  KEY `outBy` (`outBy`),
  CONSTRAINT `FK_scoreboard_1` FOREIGN KEY (`playerId`) REFERENCES `player_profile` (`playerId`),
  CONSTRAINT `FK_scoreboard_2` FOREIGN KEY (`matchId`) REFERENCES `match_schedule` (`matchId`),
  CONSTRAINT `scoreboard_ibfk_1` FOREIGN KEY (`outBy`) REFERENCES `player_profile` (`playerId`)
) ENGINE=InnoDB AUTO_INCREMENT=750 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scoreboard`
--

/*!40000 ALTER TABLE `scoreboard` DISABLE KEYS */;
INSERT INTO `scoreboard` (`id`,`matchId`,`teamId`,`playerId`,`elevenType`,`batOrder`,`striker`,`batStatus`,`bowlOrder`,`bowlStatus`,`runScored`,`ballsFaced`,`fours`,`sixes`,`outType`,`outBowler`,`outBy`,`ingStatus`,`ballsBowled`,`maidens`,`runsGiven`,`wickets`,`noBalls`,`wides`,`byes`) VALUES 
 (331,13,'MI-SCL-2018',12,1,0,'false','yet to bat',1,'',0,0,0,0,NULL,NULL,NULL,NULL,4,0,0,1,0,0,0),
 (332,13,'MI-SCL-2018',14,1,0,'false','yet to bat',2,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (333,13,'MI-SCL-2018',15,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (334,13,'MI-SCL-2018',16,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (335,13,'MI-SCL-2018',19,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (336,13,'MI-SCL-2018',20,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (337,13,'MI-SCL-2018',21,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (338,13,'MI-SCL-2018',27,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (339,13,'MI-SCL-2018',30,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (340,13,'MI-SCL-2018',32,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (341,13,'MI-SCL-2018',37,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (342,13,'RCB-SCL-2018',11,1,3,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (343,13,'RCB-SCL-2018',13,1,4,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (344,13,'RCB-SCL-2018',18,1,1,'false','Batting',0,'',0,3,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (345,13,'RCB-SCL-2018',22,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (346,13,'RCB-SCL-2018',29,1,5,'false','Batting',0,'',0,1,0,0,'BO',12,NULL,NULL,0,0,0,0,0,0,0),
 (347,13,'RCB-SCL-2018',33,1,6,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (348,13,'RCB-SCL-2018',36,1,7,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (349,13,'RCB-SCL-2018',38,1,8,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (350,13,'RCB-SCL-2018',41,1,9,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (351,13,'RCB-SCL-2018',46,1,10,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (352,13,'RCB-SCL-2018',50,1,11,'true','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (353,6,'MI-SCL-2018',12,1,1,'false','Batting',1,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (354,6,'MI-SCL-2018',14,1,3,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (355,6,'MI-SCL-2018',15,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (356,6,'MI-SCL-2018',16,1,4,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (357,6,'MI-SCL-2018',19,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (358,6,'MI-SCL-2018',20,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (359,6,'MI-SCL-2018',21,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (360,6,'MI-SCL-2018',27,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (361,6,'MI-SCL-2018',30,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (362,6,'MI-SCL-2018',32,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (363,6,'MI-SCL-2018',37,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (364,6,'MI-SCL-2018',39,0,5,'true','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (365,6,'KXP-SCL-2018',10,1,3,'false','Batting',1,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (366,6,'KXP-SCL-2018',25,1,1,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (367,6,'KXP-SCL-2018',28,1,0,'false','yet to bat',2,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (368,6,'KXP-SCL-2018',31,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (369,6,'KXP-SCL-2018',34,1,6,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (370,6,'KXP-SCL-2018',35,1,5,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (371,6,'KXP-SCL-2018',40,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (372,6,'KXP-SCL-2018',43,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (373,6,'KXP-SCL-2018',45,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (374,6,'KXP-SCL-2018',49,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (375,6,'KXP-SCL-2018',52,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (469,12,'RCB-SCL-2018',11,1,1,'true','Batting',1,'',0,0,0,0,NULL,NULL,NULL,NULL,1,0,0,1,0,0,0),
 (470,12,'RCB-SCL-2018',13,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (471,12,'RCB-SCL-2018',18,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (472,12,'RCB-SCL-2018',22,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (473,12,'RCB-SCL-2018',29,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (474,12,'RCB-SCL-2018',33,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (475,12,'RCB-SCL-2018',36,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (476,12,'RCB-SCL-2018',38,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (477,12,'RCB-SCL-2018',41,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (478,12,'RCB-SCL-2018',46,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (479,12,'RCB-SCL-2018',50,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (480,12,'RCB-SCL-2018',51,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (481,12,'KXP-SCL-2018',10,1,2,'false','Batting',2,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (482,12,'KXP-SCL-2018',25,1,3,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (483,12,'KXP-SCL-2018',28,1,1,'false','out',0,'',0,1,0,0,'BO',11,NULL,NULL,0,0,0,0,0,0,0),
 (484,12,'KXP-SCL-2018',31,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (485,12,'KXP-SCL-2018',34,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (486,12,'KXP-SCL-2018',35,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (487,12,'KXP-SCL-2018',40,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (488,12,'KXP-SCL-2018',43,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (489,12,'KXP-SCL-2018',45,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (490,12,'KXP-SCL-2018',49,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (491,12,'KXP-SCL-2018',52,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (492,14,'MI-SCL-2018',12,1,1,'false','out',0,'',1,1,0,0,'RO',NULL,11,NULL,0,0,0,0,0,0,0),
 (493,14,'MI-SCL-2018',14,1,2,'false','out',0,'',6,8,1,0,'BO',29,NULL,NULL,0,0,0,0,0,0,0),
 (494,14,'MI-SCL-2018',15,1,3,'false','out',0,'',0,1,0,0,'KO',29,NULL,NULL,0,0,0,0,0,0,0),
 (495,14,'MI-SCL-2018',16,1,4,'false','out',0,'',0,1,0,0,'CO',29,22,NULL,0,0,0,0,0,0,0),
 (496,14,'MI-SCL-2018',19,1,5,'false','out',0,'',0,1,0,0,'SO',29,29,NULL,0,0,0,0,0,0,0),
 (497,14,'MI-SCL-2018',20,1,7,'false','out',0,'',4,2,0,0,'RO',NULL,11,NULL,0,0,0,0,0,0,0),
 (498,14,'MI-SCL-2018',21,1,8,'false','out',0,'',2,1,0,0,'RO',NULL,11,NULL,0,0,0,0,0,0,0),
 (499,14,'MI-SCL-2018',27,0,16,'false','Batting',0,'',21,6,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (500,14,'MI-SCL-2018',30,0,12,'false','out',0,'',26,10,0,0,'BO',29,NULL,NULL,0,0,0,0,0,0,0),
 (501,14,'MI-SCL-2018',32,1,11,'false','out',0,'',9,3,0,0,'RO',NULL,18,NULL,0,0,0,0,0,0,0),
 (502,14,'MI-SCL-2018',37,1,14,'false','out',0,'',0,2,0,0,'BO',29,NULL,NULL,0,0,0,0,0,0,0),
 (503,14,'MI-SCL-2018',39,1,13,'false','out',0,'',0,1,0,0,'CO',29,29,NULL,0,0,0,0,0,0,0),
 (504,14,'MI-SCL-2018',42,1,17,'false','Batting',2,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (505,14,'RCB-SCL-2018',11,1,1,'true','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (506,14,'RCB-SCL-2018',13,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (507,14,'RCB-SCL-2018',18,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (508,14,'RCB-SCL-2018',22,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (509,14,'RCB-SCL-2018',29,1,0,'false','yet to bat',1,'',0,0,0,0,NULL,NULL,NULL,NULL,31,0,84,7,6,1,4),
 (510,14,'RCB-SCL-2018',33,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (511,14,'RCB-SCL-2018',36,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (512,14,'RCB-SCL-2018',38,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (513,14,'RCB-SCL-2018',41,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (514,14,'RCB-SCL-2018',46,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (515,14,'RCB-SCL-2018',50,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (516,15,'MI-SCL-2018',12,1,0,'false','yet to bat',0,'',15,9,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (517,15,'MI-SCL-2018',14,1,0,'false','yet to bat',0,'',18,17,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (518,15,'MI-SCL-2018',15,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (519,15,'MI-SCL-2018',16,1,0,'false','yet to bat',0,'',9,6,1,0,'BO',38,NULL,NULL,0,0,0,0,0,0,0),
 (520,15,'MI-SCL-2018',19,1,0,'false','yet to bat',0,'',1,1,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (521,15,'MI-SCL-2018',20,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (522,15,'MI-SCL-2018',21,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (523,15,'MI-SCL-2018',27,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (524,15,'MI-SCL-2018',30,1,0,'false','yet to bat',1,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (525,15,'MI-SCL-2018',32,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (526,15,'MI-SCL-2018',37,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (527,15,'MI-SCL-2018',39,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (528,15,'RCB-SCL-2018',11,1,1,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (529,15,'RCB-SCL-2018',13,1,2,'true','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (530,15,'RCB-SCL-2018',18,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (531,15,'RCB-SCL-2018',22,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (532,15,'RCB-SCL-2018',29,1,3,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (533,15,'RCB-SCL-2018',33,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,24,0,33,0,1,0,0),
 (534,15,'RCB-SCL-2018',36,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (535,15,'RCB-SCL-2018',38,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,6,0,13,1,2,0,0),
 (536,15,'RCB-SCL-2018',41,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (537,15,'RCB-SCL-2018',46,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (538,15,'RCB-SCL-2018',50,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (539,15,'RCB-SCL-2018',51,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (540,16,'RCB-SCL-2018',11,1,1,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (541,16,'RCB-SCL-2018',13,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (542,16,'RCB-SCL-2018',18,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (543,16,'RCB-SCL-2018',22,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (544,16,'RCB-SCL-2018',29,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (545,16,'RCB-SCL-2018',33,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (546,16,'RCB-SCL-2018',36,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (547,16,'RCB-SCL-2018',38,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (548,16,'RCB-SCL-2018',41,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (549,16,'RCB-SCL-2018',46,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (550,16,'RCB-SCL-2018',50,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (551,16,'RCB-SCL-2018',51,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (552,16,'MI-SCL-2018',12,1,4,'false','Batting',1,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (553,16,'MI-SCL-2018',14,1,3,'true','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (554,16,'MI-SCL-2018',15,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (555,16,'MI-SCL-2018',16,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (556,16,'MI-SCL-2018',19,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (557,16,'MI-SCL-2018',20,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (558,16,'MI-SCL-2018',21,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (559,16,'MI-SCL-2018',27,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (560,16,'MI-SCL-2018',30,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (561,16,'MI-SCL-2018',32,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (562,16,'MI-SCL-2018',37,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (563,16,'MI-SCL-2018',39,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (564,16,'MI-SCL-2018',42,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (565,16,'MI-SCL-2018',47,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (566,18,'KXP-SCL-2018',10,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,1,0,0,0,0,0,0),
 (567,18,'KXP-SCL-2018',25,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (568,18,'KXP-SCL-2018',28,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (569,18,'KXP-SCL-2018',31,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (570,18,'KXP-SCL-2018',34,1,0,'false','yet to bat',1,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (571,18,'KXP-SCL-2018',35,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (572,18,'KXP-SCL-2018',40,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (573,18,'KXP-SCL-2018',43,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (574,18,'KXP-SCL-2018',45,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (575,18,'KXP-SCL-2018',49,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (576,18,'KXP-SCL-2018',52,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (577,18,'RCB-SCL-2018',11,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (578,18,'RCB-SCL-2018',13,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (579,18,'RCB-SCL-2018',18,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (580,18,'RCB-SCL-2018',22,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (581,18,'RCB-SCL-2018',29,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (582,18,'RCB-SCL-2018',33,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (583,18,'RCB-SCL-2018',36,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (584,18,'RCB-SCL-2018',38,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (585,18,'RCB-SCL-2018',41,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (586,18,'RCB-SCL-2018',46,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (587,18,'RCB-SCL-2018',50,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (588,19,'KXP-SCL-2018',10,1,1,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (589,19,'KXP-SCL-2018',25,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (590,19,'KXP-SCL-2018',28,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (591,19,'KXP-SCL-2018',31,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (592,19,'KXP-SCL-2018',34,1,0,'false','yet to bat',2,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (593,19,'KXP-SCL-2018',35,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (594,19,'KXP-SCL-2018',40,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (595,19,'KXP-SCL-2018',43,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (596,19,'KXP-SCL-2018',45,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (597,19,'KXP-SCL-2018',49,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (598,19,'KXP-SCL-2018',52,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (599,19,'MI-SCL-2018',12,1,1,'true','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (600,19,'MI-SCL-2018',14,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (601,19,'MI-SCL-2018',15,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (602,19,'MI-SCL-2018',16,1,0,'false','yet to bat',1,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (603,19,'MI-SCL-2018',19,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (604,19,'MI-SCL-2018',20,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (605,19,'MI-SCL-2018',21,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (606,19,'MI-SCL-2018',27,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (607,19,'MI-SCL-2018',30,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (608,19,'MI-SCL-2018',32,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (609,19,'MI-SCL-2018',37,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (610,19,'MI-SCL-2018',39,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (611,19,'MI-SCL-2018',42,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (612,19,'MI-SCL-2018',44,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (613,20,'MI-SCL-2018',12,1,12,'false','Batting',0,'',19,9,0,1,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (614,20,'MI-SCL-2018',14,1,5,'false','out',2,'',1,3,0,0,'RO',NULL,46,NULL,0,0,0,0,0,0,0),
 (615,20,'MI-SCL-2018',15,1,6,'false','out',0,'',0,1,0,0,'CO',13,18,NULL,0,0,0,0,0,0,0),
 (616,20,'MI-SCL-2018',16,0,0,'false','yet to bat',1,'',0,0,0,0,NULL,NULL,NULL,NULL,12,0,23,0,2,3,1),
 (617,20,'MI-SCL-2018',19,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (618,20,'MI-SCL-2018',20,1,7,'false','out',0,'',0,1,0,0,'SO',13,22,NULL,0,0,0,0,0,0,0),
 (619,20,'MI-SCL-2018',21,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (620,20,'MI-SCL-2018',27,1,9,'false','out',0,'',0,1,0,0,'KO',13,NULL,NULL,0,0,0,0,0,0,0),
 (621,20,'MI-SCL-2018',30,1,10,'false','out',3,'',0,1,0,0,'BO',13,NULL,NULL,6,0,18,0,0,0,0),
 (622,20,'MI-SCL-2018',32,1,11,'false','out',0,'',0,1,0,0,'BO',13,NULL,NULL,0,0,0,0,0,0,0),
 (623,20,'MI-SCL-2018',37,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (624,20,'MI-SCL-2018',39,1,13,'true','Batting',0,'',1,1,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (625,20,'MI-SCL-2018',42,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (626,20,'MI-SCL-2018',44,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (627,20,'RCB-SCL-2018',11,1,2,'false','Batting',0,'',6,6,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (628,20,'RCB-SCL-2018',13,1,3,'false','Batting',1,'Bowling',30,14,2,0,NULL,NULL,NULL,NULL,18,0,21,5,0,0,0),
 (629,20,'RCB-SCL-2018',18,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (630,20,'RCB-SCL-2018',22,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (631,20,'RCB-SCL-2018',29,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (632,20,'RCB-SCL-2018',33,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (633,20,'RCB-SCL-2018',36,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (634,20,'RCB-SCL-2018',38,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (635,20,'RCB-SCL-2018',41,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (636,20,'RCB-SCL-2018',46,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (637,20,'RCB-SCL-2018',50,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (638,20,'RCB-SCL-2018',51,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (639,21,'MI-SCL-2018',12,1,1,'true','Batting',1,'',0,0,0,0,NULL,NULL,NULL,NULL,6,0,4,1,0,0,0),
 (640,21,'MI-SCL-2018',14,1,2,'false','Batting',2,'',0,0,0,0,NULL,NULL,NULL,NULL,6,0,24,0,2,0,0),
 (641,21,'MI-SCL-2018',15,1,0,'false','yet to bat',3,'',0,0,0,0,NULL,NULL,NULL,NULL,2,0,13,0,1,0,0),
 (642,21,'MI-SCL-2018',16,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (643,21,'MI-SCL-2018',19,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (644,21,'MI-SCL-2018',20,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (645,21,'MI-SCL-2018',21,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (646,21,'MI-SCL-2018',27,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (647,21,'MI-SCL-2018',30,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (648,21,'MI-SCL-2018',32,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (649,21,'MI-SCL-2018',37,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (650,21,'MI-SCL-2018',39,0,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (651,21,'KXP-SCL-2018',10,1,1,'false','Batting',4,'Bowling',32,8,3,1,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (652,21,'KXP-SCL-2018',25,1,2,'false','out',0,'',0,1,0,0,'BO',12,NULL,NULL,0,0,0,0,0,0,0),
 (653,21,'KXP-SCL-2018',28,1,3,'false','Batting',0,'',10,8,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (654,21,'KXP-SCL-2018',31,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (655,21,'KXP-SCL-2018',34,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (656,21,'KXP-SCL-2018',35,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (657,21,'KXP-SCL-2018',40,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (658,21,'KXP-SCL-2018',43,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (659,21,'KXP-SCL-2018',45,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (660,21,'KXP-SCL-2018',49,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (661,21,'KXP-SCL-2018',52,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (662,17,'MI-SCL-2018',12,1,0,'false','yet to bat',1,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (663,17,'MI-SCL-2018',14,1,2,'false','Batting',2,'',0,0,0,0,NULL,NULL,NULL,NULL,16,0,52,1,0,0,0),
 (664,17,'MI-SCL-2018',15,1,1,'true','Batting',4,'',6,1,0,0,NULL,NULL,NULL,NULL,1,0,4,0,0,0,0),
 (665,17,'MI-SCL-2018',16,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (666,17,'MI-SCL-2018',19,1,0,'false','yet to bat',3,'',0,0,0,0,NULL,NULL,NULL,NULL,7,0,26,1,0,0,0),
 (667,17,'MI-SCL-2018',20,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (668,17,'MI-SCL-2018',21,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (669,17,'MI-SCL-2018',27,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (670,17,'MI-SCL-2018',30,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (671,17,'MI-SCL-2018',32,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (672,17,'MI-SCL-2018',37,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (673,17,'KXP-SCL-2018',10,1,1,'false','not out',1,'Bowling',10,3,0,0,NULL,NULL,NULL,NULL,1,0,6,0,0,0,0),
 (674,17,'KXP-SCL-2018',25,1,2,'false','out',0,'',21,7,0,1,'CO',14,14,NULL,0,0,0,0,0,0,0),
 (675,17,'KXP-SCL-2018',28,1,4,'false','out',0,'',6,4,0,0,'KO',19,NULL,NULL,0,0,0,0,0,0,0),
 (676,17,'KXP-SCL-2018',31,1,3,'false','Batting',0,'',25,7,2,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (677,17,'KXP-SCL-2018',34,1,5,'false','Batting',0,'',12,2,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (678,17,'KXP-SCL-2018',35,1,6,'false','not out',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (679,17,'KXP-SCL-2018',40,1,7,'false','not out',0,'',8,1,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (680,17,'KXP-SCL-2018',43,1,8,'false','not out',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (681,17,'KXP-SCL-2018',45,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (682,17,'KXP-SCL-2018',49,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (683,17,'KXP-SCL-2018',52,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (684,22,'RCB-SCL-2018',11,1,1,'true','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (685,22,'RCB-SCL-2018',13,1,2,'false','Batting',1,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (686,22,'RCB-SCL-2018',18,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (687,22,'RCB-SCL-2018',22,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (688,22,'RCB-SCL-2018',29,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (689,22,'RCB-SCL-2018',33,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (690,22,'RCB-SCL-2018',36,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (691,22,'RCB-SCL-2018',38,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (692,22,'RCB-SCL-2018',41,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (693,22,'RCB-SCL-2018',46,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (694,22,'RCB-SCL-2018',50,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (695,22,'KXP-SCL-2018',10,1,0,'false','yet to bat',2,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (696,22,'KXP-SCL-2018',25,1,1,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (697,22,'KXP-SCL-2018',28,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (698,22,'KXP-SCL-2018',31,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (699,22,'KXP-SCL-2018',34,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (700,22,'KXP-SCL-2018',35,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (701,22,'KXP-SCL-2018',40,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (702,22,'KXP-SCL-2018',43,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (703,22,'KXP-SCL-2018',45,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (704,22,'KXP-SCL-2018',49,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (705,22,'KXP-SCL-2018',52,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (706,23,'MI-SCL-2018',12,1,0,'false','yet to bat',1,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (707,23,'MI-SCL-2018',14,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (708,23,'MI-SCL-2018',15,1,1,'true','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (709,23,'MI-SCL-2018',16,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (710,23,'MI-SCL-2018',19,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (711,23,'MI-SCL-2018',20,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (712,23,'MI-SCL-2018',21,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (713,23,'MI-SCL-2018',27,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (714,23,'MI-SCL-2018',30,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (715,23,'MI-SCL-2018',32,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (716,23,'MI-SCL-2018',37,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (717,23,'KXP-SCL-2018',10,1,1,'false','Batting',1,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (718,23,'KXP-SCL-2018',25,1,2,'false','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (719,23,'KXP-SCL-2018',28,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (720,23,'KXP-SCL-2018',31,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (721,23,'KXP-SCL-2018',34,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (722,23,'KXP-SCL-2018',35,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (723,23,'KXP-SCL-2018',40,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (724,23,'KXP-SCL-2018',43,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (725,23,'KXP-SCL-2018',45,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (726,23,'KXP-SCL-2018',49,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (727,23,'KXP-SCL-2018',52,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (728,25,'KXP-SCL-2018',10,1,0,'false','yet to bat',1,'Bowling',0,0,0,0,NULL,NULL,NULL,NULL,14,0,36,0,0,0,0),
 (729,25,'KXP-SCL-2018',25,1,0,'false','yet to bat',3,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (730,25,'KXP-SCL-2018',28,1,0,'false','yet to bat',2,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (731,25,'KXP-SCL-2018',31,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (732,25,'KXP-SCL-2018',34,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (733,25,'KXP-SCL-2018',35,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (734,25,'KXP-SCL-2018',40,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (735,25,'KXP-SCL-2018',43,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (736,25,'KXP-SCL-2018',45,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (737,25,'KXP-SCL-2018',49,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (738,25,'KXP-SCL-2018',52,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (739,25,'RCB-SCL-2018',11,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (740,25,'RCB-SCL-2018',13,1,3,'true','Batting',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (741,25,'RCB-SCL-2018',18,1,2,'false','not out',0,'',19,8,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (742,25,'RCB-SCL-2018',22,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (743,25,'RCB-SCL-2018',29,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (744,25,'RCB-SCL-2018',33,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (745,25,'RCB-SCL-2018',36,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (746,25,'RCB-SCL-2018',38,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (747,25,'RCB-SCL-2018',41,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (748,25,'RCB-SCL-2018',46,1,1,'false','Batting',0,'',17,6,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0),
 (749,25,'RCB-SCL-2018',50,1,0,'false','yet to bat',0,'',0,0,0,0,NULL,NULL,NULL,NULL,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `scoreboard` ENABLE KEYS */;


--
-- Definition of table `series_list`
--

DROP TABLE IF EXISTS `series_list`;
CREATE TABLE `series_list` (
  `seriesId` varchar(20) NOT NULL,
  `seriesName` varchar(100) NOT NULL,
  `year` int(10) unsigned NOT NULL,
  `maxPlayers` int(11) NOT NULL,
  PRIMARY KEY (`seriesId`),
  UNIQUE KEY `duplicateSeiresName` (`seriesName`,`year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `series_list`
--

/*!40000 ALTER TABLE `series_list` DISABLE KEYS */;
INSERT INTO `series_list` (`seriesId`,`seriesName`,`year`,`maxPlayers`) VALUES 
 ('SCL-2018','Skava Cricket League',2018,16),
 ('SPL-2018','Skava Premium League',2018,5);
/*!40000 ALTER TABLE `series_list` ENABLE KEYS */;


--
-- Definition of table `team_list`
--

DROP TABLE IF EXISTS `team_list`;
CREATE TABLE `team_list` (
  `teamId` varchar(20) NOT NULL,
  `teamName` varchar(50) NOT NULL,
  `teamNickName` varchar(5) NOT NULL,
  `logoUrl` varchar(210) NOT NULL,
  `seriesId` varchar(20) NOT NULL,
  `captain` varchar(45) NOT NULL,
  `viceCaptain` varchar(45) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `matches` int(11) NOT NULL DEFAULT '0',
  `won` int(11) NOT NULL DEFAULT '0',
  `lost` int(11) NOT NULL DEFAULT '0',
  `draw` int(11) NOT NULL DEFAULT '0',
  `points` int(11) NOT NULL DEFAULT '0',
  `ballsFaced` int(11) NOT NULL DEFAULT '0',
  `runsScored` int(11) NOT NULL DEFAULT '0',
  `ballsBowled` int(11) NOT NULL DEFAULT '0',
  `runsConceded` int(11) NOT NULL DEFAULT '0',
  `runRate` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`teamId`),
  KEY `seriesId` (`seriesId`),
  CONSTRAINT `team_list_ibfk_1` FOREIGN KEY (`seriesId`) REFERENCES `series_list` (`seriesId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_list`
--

/*!40000 ALTER TABLE `team_list` DISABLE KEYS */;
INSERT INTO `team_list` (`teamId`,`teamName`,`teamNickName`,`logoUrl`,`seriesId`,`captain`,`viceCaptain`,`owner`,`matches`,`won`,`lost`,`draw`,`points`,`ballsFaced`,`runsScored`,`ballsBowled`,`runsConceded`,`runRate`) VALUES 
 ('CSK-SCL-2018','chennai super kings','CSK','/images/teams/1516696182782.png','SCL-2018','','','',0,0,0,0,0,0,0,0,0,0),
 ('DDD-SCL-2018','Delhi DaraDevils','DDD','/images/teams/1516883552760.jpg','SCL-2018','','','',0,0,0,0,0,0,0,0,0,0),
 ('KXP-SCL-2018','kings eleven punjab','KXP','/images/teams/1516696375183.png','SCL-2018','25','40','1',2,1,1,0,2,25,82,1,6,19.68),
 ('MI-SCL-2018','Mumbai Indians','MI','/images/teams/1516696388715.png','SCL-2018','12','39','1',5,5,0,0,10,77,198,72,242,15.4286),
 ('RCB-SCL-2018','Royal challenger bangaluru','RCB','/images/teams/1516696410857.png','SCL-2018','11','22','1',5,0,5,0,0,48,160,77,192,20);
/*!40000 ALTER TABLE `team_list` ENABLE KEYS */;


--
-- Definition of table `team_players`
--

DROP TABLE IF EXISTS `team_players`;
CREATE TABLE `team_players` (
  `teamId` varchar(20) NOT NULL,
  `playerId` int(11) NOT NULL,
  `tRole` varchar(10) NOT NULL,
  `seriesId` varchar(20) NOT NULL,
  UNIQUE KEY `duplicatePlayers` (`playerId`,`seriesId`),
  KEY `seriesId` (`seriesId`),
  KEY `teamId` (`teamId`),
  CONSTRAINT `team_players_ibfk_1` FOREIGN KEY (`seriesId`) REFERENCES `series_list` (`seriesId`),
  CONSTRAINT `team_players_ibfk_2` FOREIGN KEY (`teamId`) REFERENCES `team_list` (`teamId`),
  CONSTRAINT `team_players_ibfk_3` FOREIGN KEY (`playerId`) REFERENCES `player_profile` (`playerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_players`
--

/*!40000 ALTER TABLE `team_players` DISABLE KEYS */;
INSERT INTO `team_players` (`teamId`,`playerId`,`tRole`,`seriesId`) VALUES 
 ('KXP-SCL-2018',10,'P','SCL-2018'),
 ('RCB-SCL-2018',11,'C','SCL-2018'),
 ('MI-SCL-2018',12,'C','SCL-2018'),
 ('RCB-SCL-2018',13,'P','SCL-2018'),
 ('MI-SCL-2018',14,'P','SCL-2018'),
 ('MI-SCL-2018',15,'P','SCL-2018'),
 ('MI-SCL-2018',16,'P','SCL-2018'),
 ('RCB-SCL-2018',18,'P','SCL-2018'),
 ('MI-SCL-2018',19,'P','SCL-2018'),
 ('MI-SCL-2018',20,'P','SCL-2018'),
 ('MI-SCL-2018',21,'P','SCL-2018'),
 ('RCB-SCL-2018',22,'VC','SCL-2018'),
 ('KXP-SCL-2018',25,'C','SCL-2018'),
 ('MI-SCL-2018',27,'P','SCL-2018'),
 ('KXP-SCL-2018',28,'P','SCL-2018'),
 ('RCB-SCL-2018',29,'P','SCL-2018'),
 ('MI-SCL-2018',30,'P','SCL-2018'),
 ('KXP-SCL-2018',31,'P','SCL-2018'),
 ('MI-SCL-2018',32,'P','SCL-2018'),
 ('RCB-SCL-2018',33,'P','SCL-2018'),
 ('KXP-SCL-2018',34,'P','SCL-2018'),
 ('KXP-SCL-2018',35,'P','SCL-2018'),
 ('RCB-SCL-2018',36,'P','SCL-2018'),
 ('MI-SCL-2018',37,'P','SCL-2018'),
 ('RCB-SCL-2018',38,'P','SCL-2018'),
 ('MI-SCL-2018',39,'VC','SCL-2018'),
 ('KXP-SCL-2018',40,'VC','SCL-2018'),
 ('RCB-SCL-2018',41,'P','SCL-2018'),
 ('MI-SCL-2018',42,'P','SCL-2018'),
 ('KXP-SCL-2018',43,'P','SCL-2018'),
 ('MI-SCL-2018',44,'P','SCL-2018'),
 ('KXP-SCL-2018',45,'P','SCL-2018'),
 ('RCB-SCL-2018',46,'P','SCL-2018'),
 ('MI-SCL-2018',47,'P','SCL-2018'),
 ('MI-SCL-2018',48,'P','SCL-2018'),
 ('KXP-SCL-2018',49,'P','SCL-2018'),
 ('RCB-SCL-2018',50,'P','SCL-2018'),
 ('RCB-SCL-2018',51,'P','SCL-2018'),
 ('KXP-SCL-2018',52,'P','SCL-2018');
/*!40000 ALTER TABLE `team_players` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
