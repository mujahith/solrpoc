<@rendermatchInfoPage/>
<#macro rendermatchInfoPage>
<html>
	<head>
		<title>Match Info</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/common.css">
		<link rel="stylesheet" type="text/css" href="/css/headeradmin.css">
		<link rel="stylesheet" type="text/css" href="/css/matchinfo.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script type="text/javascript" src="/js/headeradmin.js"></script>
		<script src="/js/matchinfo.js"></script>
	</head>
	
	<body>
		<div class="parentContMatchInfo">
			<div class="InfoCont">
				<div class="headerContainer">
					<div class="matchInfoTit">
						<h1>Match Information</h1>
					</div>
				</div>
				<div class="matchInfoCont">
				</div>
			</div>
		</div>
		<div class="parentContBatterInfo hide">
			<div class="batterInfoCont">
				<div class="headerContainer">
					<div class="matchInfoTit">
						<h1>Batter Selection</h1>
					</div>
				</div>
				<div class="batterContentCont">
					<div class="fieldGroup">
						<div class="batFirst">
							<div class="batFirstTxt">First Batter Name:</div>
							<select id="firstBatDropDown">
							</select>
						</div>
					</div>
					<div class="fieldGroup">
						<div class="batSecond">
							<div class="batSecTxt">Second Batter Name:</div>
							<select id="secBatDropDown">
							</select>
						</div>
					</div>
					<div class="fieldGroup">
						<button class="updtBattingStatus">UPDATE</button>
					</div>
				</div>
			</div>
		</div>
		<div class="parentContBowlerInfo hide">
			<div class="bowlerInfoCont">
				<div class="headerContainer">
					<div class="matchInfoTit">
						<h1>Bowler Selection</h1>
					</div>
				</div>
				<div class="bowlerContentCont">
					<div class="fieldGroup">
						<div class="bowlerCont">
							<div class="bowlerText">Bowler:</div>
							<select id="bowlerList">
							</select>
						</div>
					</div>
					<div class="fieldGroup">
						<button class="updtBowlingStatus">UPDATE</button>
					</div>
				</div>
			</div>
		</div>
		<div class="maskContainer"></div>
	</body>
</html>
</#macro>