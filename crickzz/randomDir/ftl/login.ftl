<html>
	<head>
		<title>Login Page</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/common.css">
		<link rel="stylesheet" type="text/css" href="/css/login.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script src="/js/login.js"></script>
	</head>

	<body>
		<div class="parentCont">
			<div class="loginTitle">
				<h1>Admin Login</h1>
			</div>
			<div class="loginCont">
				<div class="innerloginCont">
					<h2 class="loginSubTxt">Fill out the form below to login</h2>
					<form class="loginForm">
						<div class="fieldGroup">
							<input type="text" name="username" id="username" placeholder="Enter your user name" value="solrsudo" />
							<div class="user-icon"></div>
						</div>
						<div class="fieldGroup">
							<input type="password" name="password" id="password" placeholder="Enter a password" value="teamsolr"/>
							<div class="pwd-icon"></div>
						</div>
						<div class="submitLogin">
							<input type="submit" id="loginBtn" name="login" value="Login" class="submitButton"/>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="maskContainer"></div>
	</body>
</html>