<html>	
	<head>
		<title>Team Squad</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/header.css">
		<link rel="stylesheet" type="text/css" href="/css/teamSquad.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script src="/js/header.js"></script>
		<script src="/js/teamSquad.js"></script>
	</head>

	<body>		
		<div class="squadTitle">Squad</div>
		<div class="teamSquadCont">
			<div class="squadSubCont">
				<#if teamSquadResp?? && teamSquadResp.obj?? && teamSquadResp.obj.data?? && teamSquadResp.obj.data.assignedPlayers?? && teamSquadResp.obj.data.assignedPlayers?size gt 0>
					<#list teamSquadResp.obj.data.assignedPlayers as players>
						<div class="SquadPlayerOutCont" playerId=${players.playerId}>
							<div class="ts_playerImgCont">
								<img class="playerImg" src="${players.playerImg!}"></img>
							</div>
							<div class="ts_playerName">
								${players.name!}
							</div>
							<div class="ts_playerRole">
								${players.role!}
							</div>
						</div>
					</#list>
				</#if>
			</div>
		</div>
	</body>
</html>