<html>
	<head>
		<title>Series Creation</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/common.css">
		<link rel="stylesheet" type="text/css" href="/css/headeradmin.css">
		<link rel="stylesheet" type="text/css" href="/css/createSeries.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script type="text/javascript" src="/js/headeradmin.js"></script>
		<script src="/js/createSeries.js"></script>
	</head>
	
	<body>
	    <div class="createSeries">
			<div class="seriesCreation">
				<div class="seriesTitle" ><h1>Create Series</h1></div>
				<div class="seriesInfo">
					<div id="cr_FieldGroup_id">
						<input type="text" id="seriesName" />
						<label id="name">Series Name </label>
						<div id="borderline"></div>
					</div>
					<div id="cr_FieldGroup_id">
						<input type="text" id="seriesNickName" />
						<label id="nickName">Series Nick Name</label>
						<div id="borderline"></div>
					</div>
					<div id="cr_FieldGroup_id">
						<select id="seriesYear">
						</select>
						<label id="Year">Year</label>
						<div id="borderline"></div>
					</div>
					<div id="cr_FieldGroup_id">
						<input type="number" id="seriesMaxPlayer"/>
						<label id="maxPlayer">Squad Limit (Max Players Per Team)</label>
						<div id="borderline"></div>
					</div>
					<div class="actionBtn">
						<button id="saveBtn">ADD</button>
						<button id="cancelBtn">RESET</button>
					</div>
				</div>
			</div>
		</div>
		<div class="maskContainer"></div>
	</body>
</html>