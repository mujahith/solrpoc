<html>
	<head>
		<title>Player Profile</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/header.css">
		<link rel="stylesheet" type="text/css" href="/css/clientPlayerProfile.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script src="/js/header.js"></script>
		<script src="/js/clientPlayerProfile.js"></script>
	</head>
	
	<body>		
		<#if data?? && data.obj?? >
			<#assign response = data.obj>
		</#if>
		<#if response?? && response.data?? && response.data.playerProfile?? && response.data.playerProfile[0]?? && response.data.battingCareer?? && response.data.battingCareer[0]??>
			<div class="profileContainer">
				<div class="profile">
					<div class="profileTitle"><h1>Player Profile</h1></div>
					<div class="profileImg">
						<#if response.data.playerProfile[0].playerImg??>
							<img src="${response.data.playerProfile[0].playerImg}" height="150px" weight="150px">
						</#if>
					</div>
					<div class="profileDetails">
						<#if response.data.playerProfile[0].name??>
							<div class="playerName">${response.data.playerProfile[0].name}</div>
						</#if>
						<#if response.data.playerProfile[0].dob??>
							<div class="playerDOB"></div>
						</#if>
						<#if response.data.playerProfile[0].role??>
							<div class="playerRole">${response.data.playerProfile[0].role}</div>
						</#if>
						<#if response.data.playerProfile[0].batStyle??>
							<div class="playerBat">${response.data.playerProfile[0].batStyle}</div>
						</#if>
						<#if response.data.playerProfile[0].bowlStyle??>
							<div class="playerBowl">${response.data.playerProfile[0].bowlStyle}</div>
						</#if>
					</div>
					<div class="BattingProfile">Batting Career Summary</div>
					<div class="border">
					</div>
					<div class = "playerMatchDetails">
						<div class="inn">M</div>
						<div class="inn">Inn</div>
						<div class="inn">NO</div>
						<div class="inn">Runs</div>
						<div class="inn">HS</div>
						<div class="inn">Avg</div>
						<div class="inn">BF</div>
						<div class="inn">SR</div>
						<div class="inn">30</div>
						<div class="inn">50</div>
						<div class="inn">100</div>
						<div class="inn">4s</div>
						<div class="inn">6s</div>
					</div>
					<div class = "playerMatchDetailsInfo">
						<div class="inn">${response.data.battingCareer[0].matches!}</div>
						<div class="inn">${response.data.battingCareer[0].innings!}</div>
						<div class="inn">${response.data.battingCareer[0].notout!}</div>
						<div class="inn">${response.data.battingCareer[0].runs!}</div>
						<div class="inn">${response.data.battingCareer[0].hscore!}</div>
						<div class="inn btavg">
						<#if response.data.battingCareer[0].runs == 0 && response.data.battingCareer[0].innings == 0 >
							0
						<#elseif (response.data.battingCareer[0].innings -  response.data.battingCareer[0].notout) == 0>
							--
						<#else>
							<#assign x = response.data.battingCareer[0].runs / ( response.data.battingCareer[0].innings -  response.data.battingCareer[0].notout )>
							${x?string["0.##"]}
						</#if>
						</div>
						<div class="inn">${response.data.battingCareer[0].balls!}</div>
						<div class="inn btsr">  
							<#if response.data.battingCareer[0].balls == 0 >
							  --
							<#else>
								<#assign x = ( response.data.battingCareer[0].runs / response.data.battingCareer[0].balls )* 100>
								${x?string["0.##"]}
							</#if>
						</div>
						<div class="inn">${response.data.battingCareer[0]["30plus"]!}</div>
						<div class="inn">${response.data.battingCareer[0]["50plus"]!}</div>
						<div class="inn">${response.data.battingCareer[0]["100plus"]!}</div>
						<div class="inn">${response.data.battingCareer[0].fours!}</div>
						<div class="inn">${response.data.battingCareer[0].sixes!}</div> 
					</div> 
					<div class="BowlingProfile">Bowling Career Summary</div>
					<div class = "playerBowlingMatchDetails">
						<div class="inn">M</div>
						<div class="inn">Inn</div>
						<div class="inn">B</div>
						<div class="inn">Runs</div>
						<div class="inn">Wkts</div>
						<div class="inn">BBM</div>
						<div class="inn">Econ</div>
						<div class="inn">Avg</div>
						<div class="inn">SR</div>
						<div class="inn">3W</div>
						<div class="inn">5W</div>
					</div>
					<div class = "playerBowlingMatchDetailsInfo">
						<div class="inn">${response.data.bowlingCareer[0].matches!}</div>
						<div class="inn">${response.data.bowlingCareer[0].innings!}</div>
						<div class="inn">${response.data.bowlingCareer[0].balls!}</div>
						<div class="inn">${response.data.bowlingCareer[0].runs!}</div>
						<div class="inn">${response.data.bowlingCareer[0].wickets!}</div>
						<div class="inn">${response.data.bowlingCareer[0].bbm!}</div>
						<div class="inn bweco">
							<#if response.data.bowlingCareer[0].runs == 0 && response.data.bowlingCareer[0].balls == 0>
								--
							<#elseif response.data.bowlingCareer[0].runs == 0 && response.data.bowlingCareer[0].balls gt 0>
								0
							<#elseif response.data.bowlingCareer[0].runs gt 0 && response.data.bowlingCareer[0].balls == 0>
								${response.data.bowlingCareer[0].runs}
							<#else>
								<#assign x = (response.data.bowlingCareer[0].runs /  response.data.bowlingCareer[0].balls ) * 6>
								${x?string["0.##"]}
							</#if>
						</div>
						<div class="inn bwavg">
							<#if response.data.bowlingCareer[0].wickets == 0 && response.data.bowlingCareer[0].runs == 0>
								--
							<#elseif response.data.bowlingCareer[0].wickets == 0 && response.data.bowlingCareer[0].runs gt 0>
								--
							<#elseif response.data.bowlingCareer[0].wickets gt 0 && response.data.bowlingCareer[0].runs == 0>
								0
							<#else>
								<#assign x = response.data.bowlingCareer[0].runs / response.data.bowlingCareer[0].wickets>
								${x?string["0.##"]}
							</#if>
						</div>
						<div class="inn bwsr">
							<#if response.data.bowlingCareer[0].wickets == 0 && response.data.bowlingCareer[0].balls == 0>
								--
							<#elseif response.data.bowlingCareer[0].wickets == 0 && response.data.bowlingCareer[0].balls gt 0>
								--	
							<#else>
								<#assign x = response.data.bowlingCareer[0].balls / response.data.bowlingCareer[0].wickets>
								${x?string["0.##"]}
							</#if>
						</div>
						<div class="inn">${response.data.bowlingCareer[0]["3plus"]!}</div>
						<div class="inn">${response.data.bowlingCareer[0]["5plus"]!}</div>
					</div>
				</div>
			</div>
		</#if>
		<script>
			var  playerData = {};       
			<#if data?? && data.str??>
				playerData =  ${data.str};
			</#if>
		</script>
	</body>
</html>