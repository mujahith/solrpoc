<html>
	<head>
		<title>Points Table</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/header.css">
		<link rel="stylesheet" type="text/css" href="/css/pointsTable.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script src="/js/header.js"></script>
		<script src="/js/pointsTable.js"></script>
	</head>

	<body>		
		<div class="pointsTable">
			<div class= "tableInfoContainer">
				<div class="pointsTitle">
					<h1>Points Table</h1>
				</div>
				<div class="chooseSeriesCont">
					<div class="selectSeries">
						<h1>Series :</h1>
					</div>
					<select class="matchSeriesList">
					</select>
				</div>
				<div class="pointsList">
					<div class="pointsHeader">
						<div class="team">Teams</div>
						<div class="mat">Mat</div>
						<div class="won">Won</div>
						<div class="lost">Lost</div>
						<div class="tied">Tied</div>
						<div class="nr">NR</div>
						<div class="pts">Pts</div>
						<div class="nrr">NRR</div>
					</div>
					<div class = "pointsContent">
					</div>
				</div>
			</div>
		</div>
	</body>
</html>