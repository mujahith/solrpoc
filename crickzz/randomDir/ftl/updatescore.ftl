<!DOCTYPE html>
<html>
	<head>
		<title>Score Updation</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/headeradmin.css">
		<link rel="stylesheet" type="text/css" href="/css/common.css">
		<link rel="stylesheet" type="text/css" href="/css/updatescore.css">
		<script src="/lib/jquery.js"></script>		
		<script src="/js/common.js"></script>
		<script type="text/javascript" src="/js/headeradmin.js"></script>
		<script src="/js/updatescore.js"></script>
	</head>

	<body>
		<div id="updatescoreContainer">
			<div class="updatescoreContainer">
				<div id="updatescoreTitle">
					<h1>Score Updation</h1>
				</div>
				<div class="scoreInfo">
					<div id="cr_FieldGroup_id">
						<input type="text" id="cr_TeamAScore_id" name="teamascore" autocomplete="off" maxlength="45"/>
						<label id="cr_teamascore_id">Team A Score</label>
						<div id="borderline"></div>
					</div>
					<div id="cr_FieldGroup_id">
						<input type="text" id="cr_TeamAOvers_id" name="teamaovers" autocomplete="off" maxlength="10"/>
						<label id="cr_teamaovers_id">Team A Overs</label>
						<div id="borderline"></div>
					</div>
					<div id="cr_FieldGroup_id">
						<input type="text" id="cr_TeamBScore_id" name="teambscore" autocomplete="off" maxlength="10"/>
						<label id="cr_teambscore_id">Team B Score</label>
						<div id="borderline"></div>
					</div>
					<div id="cr_FieldGroup_id">
						<input type="text" id="cr_TeamBOvers_id" name="teambovers" autocomplete="off" maxlength="10"/>
						<label id="cr_teambovers_id">Team B Overs</label>
						<div id="borderline"></div>
					</div>
					<div id="cr_FieldGroup_id">
						<select id="cr_TeamWonVal_id">
							<option value="">&nbsp;</option>
							<#if getmatchesResp?? && getmatchesResp.obj?? && getmatchesResp.obj.data??>
								<#list getmatchesResp.obj.data as matches >
									<#if matches.teamAId?? && matches.teamAName?? && matches.teamBId?? && matches.teamBName??>
										<option value=${matches.teamAId!}>${matches.teamAName!}</option>
										<option value=${matches.teamBId!}>${matches.teamBName!}</option>
									</#if>
								</#list>
							</#if>
						</select>
						<label id="cr_series_id">Team Won</label>
						<div id="borderline"></div>
					</div>
					<div id="cr_FieldGroup_id">
						<input type="text" id="cr_MatchStatus_id" name="matchstatus" autocomplete="off" maxlength="100"/>
						<label id="cr_matchstatus_id">Match Status</label>
						<div id="borderline"></div>
					</div>
					<div id="cr_FieldGroup_id">
						<select id="cr_Mom_id">
							<option value="">&nbsp;</option>
							<#if matchplayerResp?? && matchplayerResp.obj?? && matchplayerResp.obj.data?? && matchplayerResp.obj.data.players??>
								<#list matchplayerResp.obj.data.players as players >
									<option value=${players.playerId!}>${players.name!}</option>
								</#list>
							</#if>
						</select>
						<label id="cr_mom_id">Man of the match</label>
						<div id="borderline"></div>
					</div>
					<div class="actionBtn">
						<button id="saveBtn">ADD</button>
						<button id="cancelBtn">RESET</button>
					</div>
				</div>
			</div>
		</div>
		<div class="maskContainer"></div>
	</body>
	<script>
		var  ftlResponse = {};       
		<#if getmatchesResp?? && getmatchesResp.str??>
			ftlResponse.getmatchesResp =  ${getmatchesResp.str};
		</#if>
		<#if matchplayerResp?? && matchplayerResp.str??>
			ftlResponse.matchplayerResp =  ${matchplayerResp.str};
		</#if>
	</script>
</html>