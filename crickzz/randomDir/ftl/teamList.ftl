<html>	
	<head>
		<title>Team List</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/header.css">
		<link rel="stylesheet" type="text/css" href="/css/teamList.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script src="/js/header.js"></script>
		<script src="/js/teamList.js"></script>
	</head>

	<body class="teamList">		
		<div class = "teamDetail">
			<div class="sclDrpYear">
				<div class="sclDrpShowYr">
					<div class="sclDrpSelectedYr"></div>
					<div class="sclDrpArrow bounce"></div>
				</div>
				<div class="sclYearElmCont">
				</div>
			</div>
			<div class="teamInfo">
			</div>
		</div>
	</body>
</html>