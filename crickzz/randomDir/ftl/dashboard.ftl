<html>
	<head>
		<title>Dashboard Page</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/common.css">
		<link rel="stylesheet" type="text/css" href="/css/headeradmin.css">
		<link rel="stylesheet" type="text/css" href="/css/dashboard.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script type="text/javascript" src="/js/headeradmin.js"></script>
		<script src="/js/dashboard.js"></script>
	</head>

	<body>
		<div class="dashBoardCont">			
			<div class="parentContainer">
				<div class="dashBoardDiv">
					<#list data as response>
						<div class="categoryLink">
							<div class="${response.url!}Icon"></div>
							<a href = "/${response.url!}" class="${response.url!}Tit">${response.name!}</a>
						</div>
					</#list>
				</div>
			</div>
		</div>
		<div class="maskContainer"></div>
	</body>
</html>