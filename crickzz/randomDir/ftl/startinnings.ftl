<html>
	<head>
		<title>Start Innings</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/headeradmin.css">
		<link rel="stylesheet" type="text/css" href="/css/common.css">
		<link rel="stylesheet" type="text/css" href="/css/startinnings.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script type="text/javascript" src="/js/headeradmin.js"></script>
		<script src="/js/startinnings.js"></script>		
		<script src="/socket.io/socket.io.js"></script>
	</head>

	<body>
		<div id="parentContStartInnings">
			<div class="inningsContainer">
				<div id="inningsTitle">
					<h1>Start Innings</h1>
				</div>
				<div class="inningsInfoCont">
					<div class="fieldGroup">
						<label class="tossWonTeamId">Toss Won By</label>
						<input type="radio" name="tossWonTeam" class="teamAID" id="tossWonTeamId"/><div id="teamAID" class="radioText"></div>
						<input type="radio" name="tossWonTeam" class="teamBID" id="tossWonTeamId"/><div id="teamBID" class="radioText"></div>
					</div>
					<div class="fieldGroup">
						<label class="electBatTeam">Elected To </label>
						<input type="radio" name="electedBatTeam" class="electAccept" value="Yes"/><div id="electAccept" class="radioText">Bat</div>
						<input type="radio" name="electedBatTeam" class="electReject" value = "No"/><div id="electReject" class="radioText">Field</div>
					</div>
					<div class="fieldGroup">
						<input type="text" name="umpire1" class="umpire1"/>
						<label class="umpireName1">Umpire1</label>
						<div id="borderline"></div>
					</div>
					<div class="fieldGroup">
						<input type="text" name="umpire2" class="umpire2"/>
						<label class="umpireName2">Umpire2</label>
						<div id="borderline"></div>
					</div>
					<div class="fieldGroup">
						<input type="text" name="scorer" class="scorer"/>
						<label class="scorer">Umpire 3 / Scorer</label>
						<div id="borderline"></div>
					</div>
					<div class="fieldGroup_dropdown">
						<label class="overs">Total Overs : </label>
						<select id="overs">
							<option value="" selected disabled>&nbsp;</option>
						</select>
						<label class="deliveries"><h1>.</h1></label>
						<select id="deliveries">
							<option value="" selected disabled >&nbsp;</option>
						</select>
					</div>
					<div class="fieldGroup_dropdown">
						<label class="maxOvers">Max Overs per Bowler : </label>
						<select id="maxOvers">
							<option value="" selected disabled >&nbsp;</option>
						</select>
					</div>
					<div class="fieldGroup_dropdown">
						<label class="numBowlForMaxOver">No of Bowlers for Max Overs : </label>
						<select id="numBowlForMaxOver">
							<option value="" selected disabled >&nbsp;</option>
						</select>
					</div>
					<div class="fieldGroup">
						<button id="submitBtn">NEXT</button>
					</div>
				</div>
			</div>
		</div>
		<div class="parentContPlayingEleven hide">
			<div class="InfoCont">
				<div class="headerContainer">
					<div class="playingLevenTit">
						<h1>Playing Squad (11 Players + Max 3 Substitutes)</h1>
					</div>
				</div>
				<div class="playingLevenInfoCont">
				</div>
			</div>
		</div>
		<div class="maskContainer"></div>
	</body>
</html>