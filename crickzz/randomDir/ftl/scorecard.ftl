<html>
	<head>
		<title>Scorecard</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/header.css">
		<link rel="stylesheet" type="text/css" href="/css/scorecard.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script src="/js/header.js"></script>
		<script src="/js/scorecard.js"></script>
	</head>
	
	<body>		
		<div class="parentContScoreCard">
			<div class="parentCont">
				<div class="scoreCardHeader">
					<div class="scoreCardTit">				
					</div>
				</div>
				<div class="scoreCardInfoCont">
				</div>
			</div>
		</div>
	</body>
</html>