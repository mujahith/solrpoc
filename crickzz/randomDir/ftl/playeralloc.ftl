<html>
	<head>
		<title>Player Allocation</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/common.css">
		<link rel="stylesheet" type="text/css" href="/css/headeradmin.css">
		<link rel="stylesheet" type="text/css" href="/css/playeralloc.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script type="text/javascript" src="/js/headeradmin.js"></script>
		<script src="/js/playeralloc.js"></script>
	</head>

	<body>		
		<div class="playerAllocation">
			<div class="selectedPlayer">
				<#if data?? && data.obj??>
					<#assign response = data.obj>
				</#if>
				<#if response?? && response.data?? && response.data.assignedPlayers?? >
					<div class="playerList" ><h1>Selected Player List (<span class="playerlistCount">${response.data.assignedPlayers?size}</span>)</h1></div>
					<div class="teamAllocation">
						<#list response.data.assignedPlayers as players >
							<div class="playerDetails" id="${players.playerId!}">
								<input type="checkbox" name="player" value="${players.playerId!}">
								<div class="playerImg">
									<img src="${players.playerImg!}" height="60px" weight="60px"></img>
								</div>
								<div class="playerName">${players.name!}</div>
								<div class="playerRole" id="${players.playerId!}">
									<select>
									  <option value="" disabled>Player Role</option>
									  <option value="captain" <#if players.tRole == "C"> selected</#if> >captain</option>
									  <option value="vice-captain" <#if players.tRole == "VC"> selected</#if> >vice-captain</option>
									  <option value="owner" <#if players.tRole == "O"> selected</#if> >owner</option>
									  <option value="player" <#if players.tRole == "P"> selected</#if> >player</option>
									</select>
								</div>
								<br>
							</div>
						</#list>
					</div>
				</#if>
				<div class="actionBtn">
					<button id="saveBtn">UPDATE</button>
					<button id="cancelBtn">RESET</button>
				</div>
			</div>
			<div class = "AvailablePlayer">
				<div class="playerList"><h1>Available Player List</h1></div>
				<div class="teamAllocation">
					<#if response?? && response.data?? && response.data.remainingPlayers?? && response.data.remainingPlayers[0]??>
						<#list response.data.remainingPlayers as players >
							<div class="playerDetails" id="${players.playerId!}">
								<input type="checkbox" name="player" value="${players.playerId!}">
								<div class="playerImg">
									<img src="${players.playerImg!}" height="60px" weight="60px"></img>
								</div>
								<div class="playerName">${players.name!}</div>
								<div class="playerRole" id="${players.playerId!}">
									<select>
									  <option value="" disabled selected>Player Role</option>
									  <option value="captain">captain</option>
									  <option value="vice-captain">vice-captain</option>
									  <option value="owner">owner</option>
									  <option value="player">player</option>
									</select>
								</div>
								<br>
							</div>
						</#list>
					</#if>
				</div>
			</div>
		</div>
		<div class="maskContainer"></div>
		<script>
			var  productListData = {};       
			<#if data?? && data.str??>
				productListData =  ${data.str};
			</#if>
		</script>
	</body>
</html>
