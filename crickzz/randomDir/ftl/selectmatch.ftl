<html>
	<head>
		<title>Match List</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/common.css">
		<link rel="stylesheet" type="text/css" href="/css/headeradmin.css">
		<link rel="stylesheet" type="text/css" href="/css/selectmatch.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script type="text/javascript" src="/js/headeradmin.js"></script>
		<script src="/js/selectmatch.js"></script>
	</head>

	<body>
		<div class="parentCont">
			<div class="parentMatchListCont">
				<div class="headerContainer">
					<div class="matchlistTitle">
						<h1>Match List</h1>
					</div>
				</div>
				<div class="chooseSeriesCont">
					<div class="selectSeries">
						<div class="matchSeriesId">Series</div>
					</div>
					<select class="matchSeriesList">
					</select>
				</div>
				<div class="matchListCont hide">
					<div class="matchListDetail">
						<div class="matchHeaderContainer">
							<div class="matchDate">Date & Time</div>
							<div class="matchVenueDet">Matches</div>
							<div class="matchId">Match Details</div>
						</div>
						<div class="matchDetailContainer"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="maskContainer"></div>
	</body>
</html>