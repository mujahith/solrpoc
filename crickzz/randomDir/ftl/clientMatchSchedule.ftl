<html>
	<head>
		<title>Schedules & Results</title>
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<link rel="stylesheet" type="text/css" href="/css/header.css">
		<link rel="stylesheet" type="text/css" href="/css/clientMatchSchedule.css">
		<script src="/lib/jquery.js"></script>
		<script src="/js/common.js"></script>
		<script src="/js/header.js"></script>
		<script src="/js/clientMatchSchedule.js"></script>
	</head>
	
	<body>		
		<div class="schedule">
			<div class= "tableInfoContainer">
				<div class="scheduleTitle">
					<h1>Fixtures & Results</h1>
				</div>
				<div class="chooseSeriesCont">
					<div class="selectSeries">
						<h1>Series :</h1>
					</div>
					<select class="matchSeriesList">
					</select>
				</div>
				<div class="MatchList">
					<div class="scheduleHeader">
						<div class="dateTime">Date & Time</div>
						<div class="matchDetails">Match Details</div>
						<div class="result">Result</div>
					</div>
					<div class="ScheduleCont" >
					</div>
				</div>
			</div>
		</div>
	</body>
</html>