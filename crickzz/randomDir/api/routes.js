var mysql = require('mysql');
var bcrypt = require('bcrypt');
var path = require('path');
var url = require('url');
var session = require('express-session');
var jwt = require('jsonwebtoken');
var express = require("express");
var multer = require("multer");
var app = express();
var MongoClient = require('mongodb').MongoClient;
var fs = require('fs');

var config = require('./config');
var mongoDBUrl = config.mongoDBUrl;
var mongoDBName = config.mongoDBName;
app.set('superSecret', config.secret);


var HOST = config.host;
var mysqlDBHost = config.mysqlDBHost;
var PORT = config.port;

var connection = mysql.createConnection({
    host: mysqlDBHost,
    user: config.dbUser,
    password: config.dbPwd,
    database: config.dbName
});
connection.connect(function(err) {
    if (!err) {
        console.log("Database is connected ... nn");
    } else {
        console.log("Error connecting database ... nn");
    }
});

var generateToken = function(username, role) {
    const payload = {
        name: username,
        role: role
    };
    var token = jwt.sign(payload, app.get('superSecret'), {
        expiresIn: 60 * config.sessionExpTime
    });
    return token;
};

var tokenMaker = function(req, res, username, role) {
    sess = req.session;
    username = sess.username;

    var token = generateToken(username, role);
    sess.authToken = token;
    res.send({
        "code": 302,
        "success": "Login Success",
        "redirectUrl": "dashboard",
    });
};

/*var writeImageFile = function(playerImgData,imgType,imageFileName)
{
	playerImgData = (playerImgData) ? playerImgData : "";
	var imageFormat = "", base64Data = "", imageFilePath = "", imageFolderPath = "";
    
	if(playerImgData && imageFileName)
	{
		try
		{
			imageFormat = "."+playerImgData.split(";")[0].split("/")[1];
		    base64Data = playerImgData.replace(/^data:image\/png;base64,/, "");
		    imageFileName += imageFormat;
		    
		    if(imgType == "player")
		    {
		    	imageFolderPath = "/images/players/"+imageFileName;
		    }
		    else if(imgType == "team")
		    {
		    	imageFolderPath = "/images/teams/"+imageFileName;
		    }
		    
		    imageFilePath = __dirname+"/.."+imageFolderPath;
		    
		    var cbk = function(err){
		    	console.log(err);
		    };
		    
		    fs.writeFile(imageFilePath, base64Data, 'base64', cbk);
		    return imageFolderPath;
		}
		catch(err)
		{
			console.log(err);
		}
	}
};*/

var responseFormatter = function(code, message, data, messgeCode) {
    var obj = {
        "code": code,
        "message": message
    }
    if (messgeCode) {
        obj.messgeCode = messgeCode;
    } else {
        obj.messgeCode = config.responseCode[code];
    }

    if (data) {
        obj.data = data;
    }
    return obj;
};

exports.getMatchBallsScore = function(req, res) {

    var reqBody = req.body;
    var data = {};
    data.matchId = reqBody.matchid;
    data.teamId = reqBody.teamid;
    data.seriesId = reqBody.seriesid;

    MongoClient.connect(mongoDBUrl, function(err, db) {
        var collection = db.db(mongoDBName).collection("match" + data.matchId + data.seriesId.replace("-", ""));
        collection.find().toArray(function(err, results) {
            if (results && results.length) {
                res.json(results);
                db.close();
            }
        });
    });
}

exports.createUser = function(req, res) {
    var today = new Date();
    if (req.body) {
        reqBody = req.body;
        var data = {
            "email": reqBody.email,
            "fname": reqBody.fname,
            "lname": reqBody.lname,
            "username": reqBody.username,
            "password": bcrypt.hashSync(reqBody.password, 5),
            "role": reqBody.role,
            "created": today
        }
    }

    connection.query('INSERT INTO admin SET ?', data, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            if (results) {
                res.send(responseFormatter(201, "User created successfully"));
            } else {
                res.send(responseFormatter(400, "User creation failed"));
            }
        }
    });
}

exports.login = function(req, res) {
    var username = req.body.username;
    var password = req.body.password;
    sess = req.session;
    connection.query('SELECT * FROM admin WHERE username = "' + username + '"', function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            if (results.length == 1) {
                bcrypt.compare(password, results[0].password, function(err, doesMatch) {
                    if (doesMatch) {
                        sess.username = results[0].username;
                        sess.role = results[0].role;
                        if (results[0].seriesId != null) {
                            sess.seriesId = results[0].seriesId;
                            res.cookie('seriesId', results[0].seriesId);
                        }
                        if (results[0].teamId != null) {
                            sess.teamId = results[0].teamId;
                            res.cookie('teamId', results[0].teamId);
                        }
                        tokenMaker(req, res, username, results[0].role);
                    } else {
                        res.send(responseFormatter(204, "Email and password does not match"));
                    }
                });
            } else {
                res.send(responseFormatter(204, "Email does not exists"));
            }
        }
    });
}

exports.createSeries = function(req, res) {
    seriesId = req.body.seriesnickname + "-" + req.body.year;
    var data = {
        "seriesId": seriesId,
        "seriesName": req.body.seriesname,
        "year": req.body.year,
        "maxPlayers": req.body.maxPlayers
    }
    connection.query('INSERT INTO series_list SET ?', data, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            res.send(responseFormatter(201, "Series created Successfully"));
        }
    });
}

exports.updateSeries = function(req, res) {
    var seriesId = req.body.seriesnickname + "-" + req.body.year;
    if (req.body && req.body.oldSeriesId) {
        var data = {
            "seriesId": seriesId,
            "seriesName": req.body.seriesname,
            "year": req.body.year,
            "maxPlayers": req.body.maxPlayers
        }
        connection.query('UPDATE series_list SET ? WHERE seriesId = ?', [data, req.body.oldSeriesId], function(error, results, fields) {
            if (error) {
                res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
            } else if (results.affectedRows && results.affectedRows > 0) {
                res.send(responseFormatter(200, "Series Details Updated Successfully."));
            } else {
                res.send(responseFormatter(204, "Series Id does not exists"));
            }
        });
    } else {
        res.send(responseFormatter(400, "Invalid data"));
    }
}

exports.seriesList = function(req, res) {
    connection.query('SELECT * FROM series_list', function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            res.send(responseFormatter(200, "Success", results));
        }
    });
}

exports.assignPlayers = function(req, res) {
    if (req.body) {
        var teamId = req.body.teamId ? req.body.teamId : "";
        var seriesId = req.body.seriesId ? req.body.seriesId : "";
        var players = req.body.players ? req.body.players : [];
        var roles = req.body.roles ? req.body.roles : {};
        var captain = roles.captain ? roles.captain : "";
        var viceCaptain = roles.viceCaptain ? roles.viceCaptain : "";
        var owner = roles.owner ? roles.owner : "";
    }
    var str = "";
    if (players.length) {
        for (i = 0; i < players.length; i++) {
            str = str + ("('" + teamId + "'," + players[i].playerId + ",'" + players[i].role + "','" + seriesId + "')");
            str = str + ((i == (players.length - 1)) ? ";" : ",");
        }
    }
    connection.query('DELETE FROM team_players where teamId = ? and seriesId = ?', [teamId, seriesId], function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            connection.query('INSERT INTO team_players (teamId, playerId, tRole, seriesId) values ' + str, function(error, results, fields) {
                if (error) {
                    res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                } else {
                    connection.query('UPDATE team_list set captain = ' + captain + ', viceCaptain = ' + viceCaptain + ', owner = ' + owner + ' where teamId = "' + teamId + '" and seriesId = "' + seriesId + '"', function(error, results, fields) {
                        if (error) {
                            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                        } else {
                            res.send(responseFormatter(201, "Team players details created successfully."));
                        }
                    });
                }
            });
        }
    });
}


exports.getTeamPlayers = function(req, res) {
    var q = url.parse(req.url, true);
    type = q.query.type;

    if (req.body) {
        var teamId = req.body.teamId ? req.body.teamId : "";
        var seriesId = req.body.seriesId ? req.body.seriesId : "";
    }
    connection.query('SELECT * FROM player_profile WHERE playerId NOT IN (SELECT playerId FROM team_players where seriesId = ?)  and playerId > 9', [seriesId], function(error, remainingPlayers, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            connection.query('SELECT t1.tRole, p.* FROM player_profile p INNER JOIN team_players t1 ON (t1.playerId = p.playerId) where t1.seriesId = "' + seriesId + '" and t1.teamId = "' + teamId + '"', function(error, assignedPlayers, fields) {
                if (error) {
                    res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                } else {
                    var data = {};
                    if (type != "squad") {
                        data.remainingPlayers = remainingPlayers ? remainingPlayers : [];
                    }
                    data.assignedPlayers = assignedPlayers ? assignedPlayers : [];
                    connection.query('SELECT maxPlayers from series_list where seriesId = "' + seriesId + '"', function(error, maxPlayers, fields) {
                        if (error) {
                            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                        } else {
                            if (maxPlayers.length > 0) {
                                data.maxPlayers = maxPlayers[0].maxPlayers ? maxPlayers[0].maxPlayers : {};

                            }

                            res.send(responseFormatter(200, "Success", data));
                        }
                    });
                }
            });
        }
    });
}

exports.registerPlayer = function(req, res) {

    //Commented to handle image separately
    //var imageFileName = req.body.name.replace(/\s/g,'_')+"_"+req.body.phone;
    //var imageFolderPath = writeImageFile(req.body.playerImg,"player",imageFileName) || "";

    var postData = {
        "name": req.body.name,
        "email": req.body.email,
        "phone": req.body.phone,
        "dob": req.body.dob,
        "role": req.body.role,
        "batStyle": req.body.batStyle,
        "bowlStyle": req.body.bowlStyle,
        "playerImg": req.body.playerImg
    };

    connection.query('INSERT INTO player_profile SET ?', postData, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            res.send(responseFormatter(201, "Player profile created successfully."));
        }
    });
}

exports.updatePlayer = function(req, res) {
    var updateData = {
        "name": req.body.name,
        "email": req.body.email,
        "phone": req.body.phone,
        "dob": req.body.dob,
        "role": req.body.role,
        "batStyle": req.body.batStyle,
        "bowlStyle": req.body.bowlStyle,
        "playerImg": req.body.playerImg
    };

    var respObj = {};
    connection.query('UPDATE player_profile SET ? WHERE email = ? or phone = ?', [updateData, updateData.email, updateData.phone], function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else if (results && results.affectedRows > 0) {
            res.send(responseFormatter(200, "Player details updated successfully."));
        } else {
            res.send(responseFormatter(204, "Invalid data - Please enter valid player search keys (email or phone) to locate player"));
        }
    });
}

exports.getPlayers = function(req, res) {
    var playerId = (req.body && req.body.playerid) ? req.body.playerid : "";
    var searchParam = (req.body && req.body.search) ? req.body.search : "";
    var dbQuery = 'SELECT * FROM player_profile';
    if (playerId) {
        dbQuery = 'SELECT * FROM player_profile WHERE playerId = "' + playerId + '"';
    } else if (searchParam) {
        dbQuery = 'SELECT * FROM player_profile WHERE playerId = "' + searchParam + '" or name like "%' + searchParam + '%"';
    }
    connection.query(dbQuery, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            res.send(responseFormatter(200, "Success", results));
        }
    });
}

exports.registerTeam = function(req, res) {

    var nickname = req.body.nickname;
    var teamId = nickname + "-" + req.body.seriesid;

    //Commented to handle image separately
    //var imageFileName = teamId.replace(/\s/g,'_');
    //var imageFolderPath = writeImageFile(req.body.logourl,"team",imageFileName) || "";

    var data = {
        "teamId": teamId,
        "teamName": req.body.teamname,
        "teamNickName": nickname,
        "logoUrl": req.body.logourl,
        "seriesId": req.body.seriesid,
        "captain": req.body.captain ? req.body.captain : "",
        "viceCaptain": req.body.vicecaptain ? req.body.vicecaptain : "",
        "owner": req.body.owner ? req.body.owner : "",
        "matches": req.body.matches ? req.body.matches : 0,
        "won": req.body.won ? req.body.won : 0,
        "lost": req.body.lost ? req.body.lost : 0,
        "draw": req.body.draw ? req.body.draw : 0,
        "points": req.body.points ? req.body.points : 0,
        "ballsFaced": req.body.ballsfaced ? req.body.ballsfaced : 0,
        "runsScored": req.body.runsscored ? req.body.runsscored : 0,
        "ballsBowled": req.body.ballsbowled ? req.body.ballsbowled : 0,
        "runsConceded": req.body.runsconceded ? req.body.runsconceded : 0,
        "runRate": req.body.runrate ? req.body.runrate : 0
    }


    connection.query('INSERT INTO team_list SET ?', data, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            var today = new Date();
            var pwd = nickname + Math.floor(Math.random() * ((999 - 100) + 1) + 100);
            var data1 = {
                "email": nickname + "_" + req.body.seriesid + "@mailinator.com",
                "fname": nickname,
                "lname": nickname,
                "username": (nickname + req.body.seriesid).toLowerCase(),
                "password": bcrypt.hashSync(pwd, 5),
                "role": "teamUser",
                "created": today,
                "seriesId": req.body.seriesid,
                "teamId": nickname + "-" + req.body.seriesid,
                "pwd": pwd
            }

            connection.query('INSERT INTO admin SET ?', data1, function(error, results, fields) {
                if (error) {
                    res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                } else {
                    var dataObj = {}
                    dataObj.username = data1.username;
                    dataObj.pwd = data1.pwd;
                    res.send(responseFormatter(201, "New team is registered successfully.", dataObj));
                }
            });
        }
    });
}

exports.getTeamList = function(req, res) {

    var data = {
        "teamNickName": req.body.nickname,
        "teamId": (req.body.nickname + "-" + req.body.seriesid),
        "seriesId": (req.body.seriesid)
    }
    var query = 'SELECT t.teamId, t.teamName, t.teamNickName, t.logoUrl, t.seriesId, t.matches, t.won, t.lost, t.draw, t.points, t.ballsFaced, t.runsScored, t.ballsBowled, t.runsConceded, t.runRate, p1.name as captain, p2.name as viceCaptain, p3.name as owner FROM team_list t LEFT OUTER JOIN player_profile p1 ON (t.captain = p1.playerId) LEFT OUTER JOIN player_profile p2 ON (t.viceCaptain = p2.playerId) LEFT OUTER JOIN player_profile p3 ON (t.owner = p3.playerId)';
    var selectAll = query;
    var selectTeam = query + 'where teamId="' + data.teamId + '"';
    var selectTeamInSeries = query + 'where seriesId="' + data.seriesId + '"';
    connection.query((data.teamNickName && data.seriesId) ? selectTeam : (data.seriesId ? selectTeamInSeries : selectAll), function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            res.send(responseFormatter(200, "Success", results));
        }
    });
}

exports.updateTeamList = function(req, res) {
    var data = {
        "teamId": req.body.nickname + "-" + req.body.seriesid,
        "teamName": req.body.teamname,
        "teamNickName": req.body.nickname,
        "logoUrl": req.body.logourl,
        "seriesId": req.body.seriesid,
        "captain": req.body.captain ? req.body.captain : "",
        "viceCaptain": req.body.vicecaptain ? req.body.vicecaptain : "",
        "owner": req.body.owner ? req.body.owner : "",
        "matches": req.body.matches ? req.body.matches : 0,
        "won": req.body.won ? req.body.won : 0,
        "lost": req.body.lost ? req.body.lost : 0,
        "draw": req.body.draw ? req.body.draw : 0,
        "points": req.body.points ? req.body.points : 0,
        "ballsFaced": req.body.ballsfaced ? req.body.ballsfaced : 0,
        "runsScored": req.body.runsscored ? req.body.runsscored : 0,
        "ballsBowled": req.body.ballsbowled ? req.body.ballsbowled : 0,
        "runsConceded": req.body.runsconceded ? req.body.runsconceded : 0,
        "runRate": req.body.runrate ? req.body.runrate : 0
    }
    connection.query('UPDATE team_list SET ? WHERE teamId = ?', [data, (data.teamId)], function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else if (results.affectedRows && results.affectedRows > 0) {
            res.send(responseFormatter(200, "Team list is updated successfully."));
        } else {
            res.send(responseFormatter(204, "Invalid data - Please provide the valid team details.Hint."));
        }
    });
}

exports.scheduleMatch = function(req, res) {

    var data = {
        "matchNo": req.body.matchtype + " " + req.body.matchno,
        "dateTime": req.body.datetime,
        "venue": req.body.venue,
        "teamAId": req.body.teamaid,
        "teamBId": req.body.teambid,
        "seriesId": req.body.seriesid,
        "matchType": req.body.matchtype
    }
    connection.query('INSERT INTO match_schedule SET ?', data, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            res.send(responseFormatter(201, "Match is scheduled successfully."));
        }
    });
}

exports.getMatches = function(req, res) {

    var data = {
        "matchId": req.body.matchid,
        "teamId": req.body.teamid,
        "seriesId": req.body.seriesid
    }
    var selectAll = 'SELECT m.*, t1.teamName as teamAName, t2.teamName as teamBName, t1.seriesId FROM match_schedule m INNER JOIN team_list t1 ON (m.teamAId = t1.teamId) INNER JOIN team_list t2 ON (m.teamBId = t2.teamId) order by matchId ASC';
    var selectMatch = 'SELECT m.*, t1.teamName as teamAName, t2.teamName as teamBName, t1.seriesId FROM match_schedule m INNER JOIN team_list t1 ON (m.teamAId = t1.teamId) INNER JOIN team_list t2 ON (m.teamBId = t2.teamId) where m.matchId="' + data.matchId + '" order by matchId ASC';
    var selectMatchsForTeam = 'SELECT m.*, t1.teamName as teamAName, t2.teamName as teamBName, t1.seriesId FROM match_schedule m INNER JOIN team_list t1 ON (m.teamAId = t1.teamId) INNER JOIN team_list t2 ON (m.teamBId = t2.teamId) where (teamAId="' + data.teamId + '" or teamBId = "' + data.teamId + '") order by matchId ASC';
    var selectMatchsInSeries = 'SELECT m.*, t1.teamName as teamAName, t2.teamName as teamBName, t1.seriesId FROM match_schedule m INNER JOIN team_list t1 ON (m.teamAId = t1.teamId) INNER JOIN team_list t2 ON (m.teamBId = t2.teamId) where m.seriesId="' + data.seriesId + '" order by matchId ASC';
    connection.query((data.matchId) ? selectMatch : (data.teamId ? selectMatchsForTeam : (data.seriesId ? selectMatchsInSeries : selectAll)), function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            res.send(responseFormatter(200, "Success", results));
        }
    });
}

exports.updateTossInfo = function(req, res) {
    var bodyData = req.body ? req.body : "";
    if (bodyData != "") {
        var data = {
            "matchBalls": bodyData.matchBalls,
            "maxBalls": bodyData.maxBalls,
            "noBowForMaxOvers": bodyData.noBowForMaxOvers,
            "scorer": bodyData.scorer,
            "umpire1": bodyData.umpire1,
            "umpire2": bodyData.umpire2,
            "tossWonBy": bodyData.tossWonBy,
            "electedToBat": bodyData.electedToBat
        };
        connection.query('UPDATE match_schedule SET ? where matchId = ?', [data, (bodyData.matchId)], function(error, results, fields) {
            if (error) {
                res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
            } else if (results.affectedRows && results.affectedRows > 0) {
                res.send(responseFormatter(200, "Toss Details and Match Info Updated Successfully."));
            } else {
                res.send(responseFormatter(204, "Invalid Data - Match Id not exists."));
            }
        });
    } else {
        res.send(responseFormatter(400, "Cannot Update now, please try again later"));
    }
}

exports.assignMatchPlayers = function(req, res) {
    var teamId, matchId, mainEleven, players = [];

    if (req.body) {
        teamId = req.body.teamid ? req.body.teamid : "";
        matchId = req.body.matchid ? req.body.matchid : "";
        players = req.body.players ? req.body.players : [];
    }
    var str = "";
    if (players.length) {
        for (i = 0; i < players.length; i++) {
            mainEleven = players[i].maineleven == "Y" ? 1 : 0;
            str = str + ("(" + matchId + ",'" + teamId + "'," + players[i].playerid + "," + mainEleven + ")");
            str = str + ((i == (players.length - 1)) ? ";" : ",");
        }
    }
    connection.query('DELETE FROM scoreboard where matchId = "' + matchId + '" and teamId = "' + teamId + '"', function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            connection.query('INSERT INTO scoreboard (matchId, teamId, playerId, elevenType) values ' + str, function(error, results, fields) {
                if (error) {
                    res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                } else {
                    res.send(responseFormatter(201, "Scorecard details updated successfully"));
                }
            });
        }
    });
}

exports.updateBattingStatus = function(req, res) {

    var matchId = req.body.matchid;
    var teamId = req.body.teamId;
    var batsmen1Id = req.body.batsmen1id;
    var batsmen2Id = req.body.batsmen2id ? req.body.batsmen2id : "";
    var updateQuery, maxBatOrder = 0;

    connection.query('SELECT MAX(batOrder) as maxBatOrder FROM scoreboard where matchId = ? and teamId = ?', [matchId, teamId], function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            if (results && results[0].maxBatOrder) {
                maxBatOrder = results[0].maxBatOrder;
            }
            if (batsmen1Id && batsmen2Id) {
                updateQuery = "UPDATE scoreboard SET batOrder = IF(playerId=" + batsmen1Id + "," + (maxBatOrder + 1) + "," + (maxBatOrder + 2) + "), batStatus = 'Batting' WHERE playerId IN (" + batsmen1Id + "," + batsmen2Id + ") and matchId =" + matchId;
            } else {
                updateQuery = "UPDATE scoreboard SET batOrder = IF(batOrder= 0," + (maxBatOrder + 1) + ",batOrder), batStatus = 'Batting' WHERE playerId =" + batsmen1Id + " and matchId =" + matchId;
            }
            connection.query(updateQuery, function(error, results, fields) {
                if (error) {
                    res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                } else if (results.affectedRows && results.affectedRows > 0) {
                    var updateStrikerCbk = function(result) {
                        if (result.error) {
                            res.send(responseFormatter(400, (result.sqlMessage ? result.sqlMessage : ""), null, (result.code ? result.code : "")));
                        } else {
                            res.send(responseFormatter(200, "Batting order is updated successfully."));
                        }
                    };
                    updateStriker(batsmen1Id, matchId, updateStrikerCbk);
                } else {
                    res.send(responseFormatter(204, "Invalid batsmen - Please select the right batsmen."));
                }
            });
        }
    });
}

exports.updateBowlingStatus = function(req, res) {

    var matchId = req.body.matchid;
    var teamId = req.body.teamId;
    var bowlerId = req.body.bowlerid;
    var maxBowlOrder = 0;

    connection.query('SELECT MAX(bowlOrder) as maxBowlOrder FROM scoreboard where matchId = ? and teamId = ?', [matchId, teamId], function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            if (results[0] && results[0].maxBowlOrder) {
                maxBowlOrder = results[0].maxBowlOrder;
            }
            var query = "UPDATE scoreboard SET bowlOrder = IF(bowlOrder = 0, " + (maxBowlOrder + 1) + ",bowlOrder ) WHERE playerId =" + bowlerId + " and matchId =" + matchId;
            connection.query(query, function(error, results, fields) {
                if (error) {
                    res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                } else if (results.affectedRows && results.affectedRows > 0) {
                    query = "UPDATE scoreboard SET bowlStatus = IF(playerId=" + bowlerId + ",'Bowling','') WHERE matchId =" + matchId;
                    connection.query(query, function(error, results, fields) {
                        if (error) {
                            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                        } else if (results.affectedRows && results.affectedRows > 0) {
                            res.send(responseFormatter(200, "Bowling order is updated successfully."));
                        } else {
                            res.send(responseFormatter(204, "Invalid bowler - Please select the right bowler."));
                        }
                    });
                } else {
                    res.send(responseFormatter(204, "Invalid bowler - Please select the right bowler."));
                }
            });
        }
    });
}

exports.updateMatchScore = function(req, res) {
    if (req.body && req.body.matchId) {
        var data = {
            "matchId": req.body.matchId,
            "teamWon": req.body.teamWon,
            "matchStatus": req.body.matchStatus,
            "mom": req.body.mom
        }
        if (req.body.teamAScore) {
            data.teamAScore = req.body.teamAScore;
        }
        if (req.body.teamAWicket) {
            data.teamAWicket = req.body.teamAWicket;
        }
        if (req.body.teamABalls) {
            data.teamABalls = req.body.teamABalls;
        }
        if (req.body.teamBScore) {
            data.teamBScore = req.body.teamBScore;
        }
        if (req.body.teamBWicket) {
            data.teamBWicket = req.body.teamBWicket;
        }
        if (req.body.teamBBalls) {
            data.teamBBalls = req.body.teamBBalls;
        }
        connection.query('UPDATE match_schedule SET ? WHERE matchId = ?', [data, req.body.matchId], function(error, results, fields) {
            if (error) {
                res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
            } else if (results.affectedRows && results.affectedRows > 0) {
                var teamDetailsCbk = function(teamDetails) {
                    if (teamDetails.error) {
                        res.send(responseFormatter(400, (teamDetails.sqlMessage ? teamDetails.sqlMessage : ""), null, (teamDetails.code ? teamDetails.code : "Failed")));
                    } else {
                        var profilesCbk = function(profileDetails) {
                            if (profileDetails.error) {
                                res.send(responseFormatter(400, (profileDetails.sqlMessage ? profileDetails.sqlMessage : ""), null, (profileDetails.code ? profileDetails.code : "Failed")));
                            } else {
                                res.send(responseFormatter(200, "Match Score Details updated successfully."));
                            }
                        };
                        if (!req.body.teamAScore) {
                            updateBatBowlCareer(data.matchId, profilesCbk);
                        } else {
                            profilesCbk("success");
                        }
                    }
                }

                updateTeamScoreDetails(data.matchId, teamDetailsCbk);

            } else {
                res.send(responseFormatter(204, "Invalid MatchId - Please select the correct match."));
            }
        });
    } else {
        res.send(responseFormatter(400, "Cannot Update now, please try again later"));
    }
}

exports.getMatchPlayers = function(req, res) {
    if (req.body) {
        var matchId = req.body.matchId ? req.body.matchId : 0;
        var teamId = req.body.teamId ? req.body.teamId : 0;
    }
    var query = "";
    if (matchId && teamId) {
        query = 'SELECT p.name, p.playerImg, p.playerId, t1.* FROM player_profile p INNER JOIN scoreboard t1 ON (t1.playerId = p.playerId) where t1.matchId = "' + matchId + '" and t1.teamId = "' + teamId + '"';
    } else {
        query = 'SELECT p.name, p.playerImg, p.playerId, t1.* FROM player_profile p INNER JOIN scoreboard t1 ON (t1.playerId = p.playerId) where t1.matchId = "' + matchId + '"';
    }
    connection.query(query, function(error, players, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            var data = {};
            data.players = players ? players : [];
            res.send(responseFormatter(200, "Success", data));
        }
    });
}

exports.getMatchScoreboard = function(req, res) {
    if (req.body) {
        var matchId = req.body.matchid ? req.body.matchid : 0;
    }
    var matchObj = {};
    var query = 'SELECT m.*, p.name as momName, t1.teamName as teamAName, t1.teamNickName as teamANickName, t2.teamName as teamBName, t2.teamNickName as teamBNickName, t1.seriesId FROM match_schedule m INNER JOIN team_list t1 ON (m.teamAId = t1.teamId) INNER JOIN team_list t2 ON (m.teamBId = t2.teamId) LEFT OUTER JOIN player_profile p ON (m.mom = p.playerId) where m.matchId="' + matchId + '"';

    connection.query(query, function(error, match, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            if (match && match.length > 0) {
                matchObj.matchInfo = match;
                query = 'SELECT s.*,  p1.name, p1.playerImg, p1.playerId, p2.name as outByBowlerName, p3.name as outByName FROM `scoreboard` s INNER JOIN player_profile p1 on (s.playerId = p1.playerId) LEFT OUTER JOIN player_profile p2 on (s.outBowler = p2.playerId) LEFT OUTER JOIN player_profile p3 on (s.outBy = p3.playerId) where s.matchId = "' + matchId + '" and s.teamId = "' + match[0].teamAId + '"  ORDER BY batOrder';
                connection.query(query, function(error, teamAPlayers, fields) {
                    if (error) {
                        res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                    } else {
                        if (teamAPlayers && teamAPlayers.length > 0) {
                            matchObj.teamA = {};
                            matchObj.teamA.id = match[0].teamAId
                            matchObj.teamA.players = teamAPlayers;
                            query = 'SELECT s.*,  p1.name, p1.playerImg, p1.playerId, p2.name as outByBowlerName, p3.name as outByName FROM `scoreboard` s INNER JOIN player_profile p1 on (s.playerId = p1.playerId) LEFT OUTER JOIN player_profile p2 on (s.outBowler = p2.playerId) LEFT OUTER JOIN player_profile p3 on (s.outBy = p3.playerId) where s.matchId = "' + matchId + '" and s.teamId = "' + match[0].teamBId + '"  ORDER BY batOrder';
                            connection.query(query, function(error, teamBPlayers, fields) {
                                if (error) {
                                    res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
                                } else {
                                    if (teamBPlayers && teamBPlayers.length > 0) {
                                        matchObj.teamB = {};
                                        matchObj.teamB.id = match[0].teamBId
                                        matchObj.teamB.players = teamBPlayers;
                                        res.send(responseFormatter(200, "Success", matchObj));
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}

var getCurrScorecard = function(matchId, battingTeamId, bowlTeamId, callback, obj) {
    var queryBatsMen, queryBowler, m_scoreboard = obj ? obj : {};

    if (matchId && battingTeamId && bowlTeamId) {
        queryBatsMen = 'SELECT p.playerId, p.name, p.playerImg, s.runScored, s.ballsFaced, s.fours, s.sixes, s.striker from `scoreboard` s INNER JOIN player_profile  p ON (s.playerId = p.playerId) where batStatus = "Batting" and matchId = "' + matchId + '" and teamId = "' + battingTeamId + '" ORDER BY batOrder';
        queryBowler = 'SELECT p.playerId, p.name, p.playerImg, s.ballsBowled, s.runsGiven, s.maidens, s.wickets, s.noBalls, s.wides from `scoreboard` s INNER JOIN player_profile  p ON (s.playerId = p.playerId) where bowlStatus = "bowling" and matchId = "' + matchId + '" and teamId = "' + bowlTeamId + '"';
    }
    connection.query(queryBatsMen, function(error, batsMen, fields) {
        if (error) {
            error.error = true;
            callback(error);
        } else {
            m_scoreboard.batsMen = [];
            if (batsMen.length > 0) {
                if (batsMen.length == 2) {
                    if (batsMen[0].striker == "true") {
                        m_scoreboard.batsMen.push(batsMen[0]);
                        m_scoreboard.batsMen.push(batsMen[1]);
                    }
                    if (batsMen[1].striker == "true") {
                        m_scoreboard.batsMen.push(batsMen[1]);
                        m_scoreboard.batsMen.push(batsMen[0]);
                    }
                } else {
                    m_scoreboard.batsMen.push(batsMen[0]);
                }
            }

            if (m_scoreboard.batsMen.length != 2) {
                m_scoreboard.triggerBatsmenChange = true;
            }
            connection.query(queryBowler, function(error, bowler, fields) {
                if (error) {
                    error.error = true;
                    callback(error);
                } else {
                    m_scoreboard.bowler = [];
                    if (bowler && bowler[0]) {
                        m_scoreboard.bowler.push(bowler[0]);
                    }
                    if (m_scoreboard.bowler.length == 0) {
                        m_scoreboard.triggerBowlerChange = true;
                    }

                    callback(m_scoreboard);
                }
            });
        }
    });
}

exports.getCurrentScoreCard = function(req, res) {
    if (req.body) {
        var matchId = req.body.matchid ? req.body.matchid : "";
        var battingTeamId = req.body.battingteamid ? req.body.battingteamid : "";
        var bowlTeamId = req.body.bowlteamid ? req.body.bowlteamid : "";
        var getCurrScorecardCbk = function(data) {
            if (data.error) {
                res.send(responseFormatter(400, (updateRes.sqlMessage ? updateRes.sqlMessage : ""), null, (updateRes.code ? updateRes.code : "")));
            } else {
                res.send(responseFormatter(200, "success", data));
            }
        }
        getCurrScorecard(matchId, battingTeamId, bowlTeamId, getCurrScorecardCbk);

    } else {
        res.send(responseFormatter(400, "ERROR", "ERROR"));
    }
}

exports.getCurrentScoreCardIO = function(obj, callback) {
    if (obj) {
        var matchId = obj.matchid ? obj.matchid : "";
        var getCurrMatchInfoCbk = function(response) {
            if (response.error) {
                error.error = true;
                callback(error);
            } else {
                if (response) {
                    var m_scoreboard = {};
                    var firstIng = "",
                        secondIng = "",
                        batTeamId = "",
                        bowlTeamId = "",
                        batTeamBalls = "";
                    if (response.tossWonBy == response.teamBId && response.electedToBat == 1 || response.tossWonBy == response.teamAId && response.electedToBat == 0) {
                        firstIng = response.teamBId;
                        secondIng = response.teamAId;
                        if (response.teamBInngStatus == 0) {
                            batTeamId = response.teamBId;
                            batTeamBalls = response.teamBBalls;
                            bowlTeamId = response.teamAId;
                        } else if (response.teamAInngStatus == 0) {
                            batTeamId = response.teamAId;
                            batTeamBalls = response.teamABalls;
                            bowlTeamId = response.teamBId;
                        }
                    } else {
                        firstIng = response.teamAId;
                        secondIng = response.teamBId;
                        if (response.teamAInngStatus == 0) {
                            batTeamId = response.teamAId;
                            batTeamBalls = response.teamABalls;
                            bowlTeamId = response.teamBId;
                        } else if (response.teamBInngStatus == 0) {
                            batTeamId = response.teamBId;
                            batTeamBalls = response.teamBBalls;
                            bowlTeamId = response.teamAId;
                        }
                    }
                    response.firstIng = firstIng;
                    response.secondIng = secondIng;
                    response.batTeamId = batTeamId;
                    response.bowlTeamId = bowlTeamId;
                    response.batTeamBalls = batTeamBalls;
                    response.matchEnd = 0;
                    m_scoreboard.matchInfo = [];
                    if (batTeamId && bowlTeamId) {
                        m_scoreboard.matchInfo.push(response);
                        getCurrScorecard(matchId, batTeamId, bowlTeamId, callback, m_scoreboard);
                    } else {
                        response.matchEnd = 1;
                        m_scoreboard.matchInfo.push(response);
                        callback(m_scoreboard);
                    }
                }

            }
        };
        getCurrMatchInfo(matchId, getCurrMatchInfoCbk);


    } else {
        return {};
    }
}

var getCurrentScoreCardIO = function(obj, callback, dataObj) {
    if (obj) {
        var matchId = obj.matchid ? obj.matchid : "";
        var getCurrMatchInfoCbk = function(response) {
            if (response.error) {
                error.error = true;
                callback(error);
            } else {
                if (response) {
                    var m_scoreboard = dataObj ? dataObj : {};
                    var firstIng = "",
                        secondIng = "",
                        batTeamId = "",
                        bowlTeamId = "",
                        batTeamBalls = "";
                    if (response.tossWonBy == response.teamBId && response.electedToBat == 1 || response.tossWonBy == response.teamAId && response.electedToBat == 0) {
                        firstIng = response.teamBId;
                        secondIng = response.teamAId;
                        if (response.teamBInngStatus == 0) {
                            batTeamId = response.teamBId;
                            batTeamBalls = response.teamBBalls;
                            bowlTeamId = response.teamAId;
                        } else if (response.teamAInngStatus == 0) {
                            batTeamId = response.teamAId;
                            batTeamBalls = response.teamABalls;
                            bowlTeamId = response.teamBId;
                        }
                    } else {
                        firstIng = response.teamAId;
                        secondIng = response.teamBId;
                        if (response.teamAInngStatus == 0) {
                            batTeamId = response.teamAId;
                            batTeamBalls = response.teamABalls;
                            bowlTeamId = response.teamBId;
                        } else if (response.teamBInngStatus == 0) {
                            batTeamId = response.teamBId;
                            batTeamBalls = response.teamBBalls;
                            bowlTeamId = response.teamAId;
                        }
                    }
                    response.firstIng = firstIng;
                    response.secondIng = secondIng;
                    response.batTeamId = batTeamId;
                    response.bowlTeamId = bowlTeamId;
                    response.batTeamBalls = batTeamBalls;
                    response.matchEnd = 0;
                    m_scoreboard.matchInfo = [];
                    if (batTeamId && bowlTeamId) {
                        m_scoreboard.matchInfo.push(response);
                        getCurrScorecard(matchId, batTeamId, bowlTeamId, callback, m_scoreboard);
                    } else {
                        response.matchEnd = 1;
                        m_scoreboard.matchInfo.push(response);
                        callback(m_scoreboard);
                    }
                }

            }
        };
        getCurrMatchInfo(matchId, getCurrMatchInfoCbk);


    } else {
        return {};
    }
}

exports.updateStriker = function(req, res) {
    if (req.body) {
        var matchId = req.body.matchid ? req.body.matchid : "";
        var batsmenId = req.body.batsmen ? req.body.batsmen : "";
        var callback = function(updateRes) {
            if (updateRes.error) {
                res.send(responseFormatter(400, (updateRes.sqlMessage ? updateRes.sqlMessage : ""), null, (updateRes.code ? updateRes.code : "")));
            } else {
                res.send(responseFormatter(200, "success", "Striker updated successfully"));
            }
        };
        updateStriker(batsmenId, matchId, callback);
    }
}

var updateBattingData = function(data, callback) {
    if (data) {
        var query = "";
        if (data.outType) {
            var out = data.outType;
            var outBy = data.outBy ? data.outBy : data.bowler;
            if (out == "RO") {
                var runScored = "";
                if (data.run != 0) {
                    runScored = "runScored = runScored + " + data.run + ",";
                }
                query = 'UPDATE scoreboard SET ' + runScored + ' ballsFaced = ballsFaced + 1 WHERE playerId =' + data.batsmen1 + ' AND matchId =' + data.matchId;
                connection.query(query, function(error, results, fields) {
                    if (error) {
                        error.error = true;
                        callback(error);
                    } else if (results && results.affectedRows > 0) {
                        if (data.outPlayer) {
                            query = 'UPDATE scoreboard SET batStatus = "out", outType = "' + out + '", outBy = ' + data.outBy + ' WHERE playerId =' + data.outPlayer + ' AND matchId =' + data.matchId;

                            connection.query(query, function(error, results, fields) {
                                if (error) {
                                    error.error = true;
                                    callback(error);
                                } else if (results && results.affectedRows > 0) {
                                    callback(200, "Batting scoreboard updated successfully");
                                } else {
                                    callback(204, "Invalid data - Please enter valid data");
                                }
                            });
                        } else {
                            callback(200, "Batting scoreboard updated successfully");
                        }
                    } else {
                        callback(204, "Invalid data - Please enter valid data");
                    }
                });

            }
            query = "";
            if (out == "SO" || out == "CO") {
                query = 'UPDATE scoreboard SET ballsFaced = ballsFaced + 1, batStatus = "out", outBowler = ' + data.bowler + ', outType = "' + out + '", outBy = ' + outBy + ' WHERE playerId =' + data.batsmen1 + ' AND matchId =' + data.matchId;
            } else if (out == "BO" || out == "KO") {
                query = 'UPDATE scoreboard SET ballsFaced = ballsFaced + 1, batStatus = "out", outBowler = ' + data.bowler + ', outType = "' + out + '" WHERE playerId =' + data.batsmen1 + ' AND matchId =' + data.matchId;
            }
            if (query != "") {
                connection.query(query, function(error, results, fields) {
                    if (error) {
                        error.error = true;
                        callback(error);
                    } else if (results && results.affectedRows > 0) {
                        callback(200, "Batting scoreboard updated successfully");
                    } else {
                        callback(204, "Invalid data - Please enter valid data");
                    }
                });
            }

        } else if (data.extraType != "WD") {
            if (data.run == 0) {
                query = 'UPDATE scoreboard SET ballsFaced = ballsFaced + 1 WHERE playerId =' + data.batsmen1 + ' AND matchId =' + data.matchId;
            } else {
                var runScored = "runScored = runScored + " + data.run;
                var boundary = "";
                if (data.boundary) {
                    if (data.run == 4) {
                        boundary = "fours = fours + 1,"
                    } else if (data.run == 6) {
                        boundary = "sixes = sixes + 1,"
                    }
                }
                query = 'UPDATE scoreboard SET ' + runScored + ',' + boundary + ' ballsFaced = ballsFaced + 1 WHERE playerId =' + data.batsmen1 + ' AND matchId =' + data.matchId;
            }
            connection.query(query, function(error, results, fields) {
                if (error) {
                    error.error = true;
                    callback(error);
                } else if (results && results.affectedRows > 0) {
                    callback(200, "Batting scoreboard updated successfully");
                } else {
                    callback(204, "Invalid data - Please enter valid data");
                }
            });
        } else {
            callback(200, "Batting scoreboard updated successfully");
        }
    }
};

var updateBowlingData = function(data, callback) {
    if (data) {
        var query = "",
            query1 = "";
        if (data.outType && data.outType != "RO") {
            query1 = ' wickets = wickets + 1, ';
        }
        if (data.extraType) {
            if (data.extraType == "NB") {
                var runsGiven = "runsGiven + " + (data.run + 1);
                query = ' noBalls = noBalls + 1, runsGiven = ' + runsGiven;
            } else if (data.extraType == "WD") {
                var runsGiven = "runsGiven + " + (data.extraRun + 1);
                var wides = "wides + " + (data.extraRun + 1);
                query = ' wides = ' + wides + ', runsGiven = ' + runsGiven;
            } else if (data.extraType == "NBWBYES") {
                var runsGiven = "runsGiven + " + 1;
                var byes = "byes + " + data.extraRun;
                query = ' noBalls = noBalls + 1, byes = ' + byes + ', runsGiven = ' + runsGiven;
            } else {
                var byes = "byes + " + data.extraRun;
                query = ' byes = ' + byes + ', ballsBowled = ballsBowled + 1';
            }
        } else {
            if (data.run == 0) {
                query = ' ballsBowled = ballsBowled + 1 ';
            } else {
                var runsGiven = "runsGiven + " + data.run;
                query = ' ballsBowled = ballsBowled + 1, runsGiven = ' + runsGiven;
            }
        }
        queryStr = 'UPDATE scoreboard SET' + query1 + query + ' WHERE playerId =' + data.bowler + ' AND matchId =' + data.matchId;
        connection.query(queryStr, function(error, results, fields) {
            if (error) {
                error.error = true;
                callback(error);
            } else if (results && results.affectedRows > 0) {
                callback(200, "Bowling scoreboard updated successfully.");
            } else {
                callback(204, "Invalid data - Please enter valid data.");
            }
        });
    }
};

var updateMatchScore = function(data, callback) {
    var queryStr = "select * from match_schedule where matchId =" + data.matchId;
    connection.query(queryStr, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            if (results && results.length > 0) {
                var query = "UPDATE match_schedule set ";
                if (results[0].teamAId == data.teamId) {
                    var wicket = 0,
                        balls = 0,
                        score = "";
                    if ((data.extraType && (data.extraType == "BYES")) || (!data.extraType)) {
                        balls = 1;
                    }
                    if (data.outType) {
                        wicket = 1;
                    }
                    if (data.totalRun > 0) {
                        score = ", teamAScore =  teamAScore + " + data.totalRun;
                    }
                    query = query + "teamABalls = teamABalls + " + balls + ", teamAWicket = teamAWicket + " + wicket + score;
                } else {
                    var wicket = 0,
                        balls = 0,
                        score = "";
                    if ((data.extraType && (data.extraType == "BYES")) || (!data.extraType)) {
                        balls = 1;
                    }
                    if (data.outType) {
                        wicket = 1;
                    }
                    if (data.totalRun > 0) {
                        score = ", teamBScore =  teamBScore + " + data.totalRun;
                    }
                    query = query + "teamBBalls = teamBBalls + " + balls + ", teamBWicket = teamBWicket + " + wicket + score;
                }
                query = query + " where matchId=" + data.matchId;
                connection.query(query, function(error, results, fields) {
                    if (error) {
                        error.error = true;
                        callback(error);
                    } else if (results && results.affectedRows > 0) {
                        callback(200, "Match details updated successfully.");
                    } else {
                        callback(204, "Invalid data - Please enter valid data");
                    }
                });
            }
        }
    });
};

var getCurrBatsmenDetails = function(playerId, matchId) {
    var query = "select p.playerId, p.name, p.playerImg, s.runScored, s.ballsFaced, s.fours, s.sixes from scoreboard s INNER JOIN player_profile on (p.playerId = s.playerId) where s.playerId = " + playerId + "and matchId =" + matchId;
    connection.query(query, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            if (results && results.length > 0) {
                return results[0];
            }
        }
    });
};

var getCurrBowlerDetails = function(playerId, matchId) {
    var query = "select p.playerId, p.name, p.playerImg, s.ballsBowled, s.runsGiven, s.maidens, s.wickets, s.noBalls, s.wides from scoreboard s INNER JOIN player_profile on (p.playerId = s.playerId) where s.playerId = " + playerId + "and matchId =" + matchId;
    connection.query(query, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            if (results && results.length > 0) {
                return results[0];
            }
        }
    });
};

var getCurrMatchInfo = function(matchId, callback) {
    var query = 'SELECT m.*, p.name as momName, t1.teamName as teamAName, t1.teamNickName as teamANickName, t2.teamName as teamBName, t2.teamNickName as teamBNickName, t1.seriesId FROM match_schedule m INNER JOIN team_list t1 ON (m.teamAId = t1.teamId) INNER JOIN team_list t2 ON (m.teamBId = t2.teamId) LEFT OUTER JOIN player_profile p ON (m.mom = p.playerId) where m.matchId="' + matchId + '"';
    connection.query(query, function(error, results, fields) {
        if (error) {
            error.error = true;
            callback(error);
        } else {
            if (results && results.length > 0) {
                callback(results[0]);
            }
        }
    });
};

var updateStriker = function(batsman, matchId, callback) {
    var query = 'UPDATE scoreboard SET striker = IF(playerId=' + batsman + ',"true","false") where matchId=' + matchId;
    connection.query(query, function(error, results, fields) {
        if (error) {
            error.error = true;
            callback(error);
        } else {
            callback("success");
        }
    });
};

var updateMaidenOver = function(matchId, bowler, callback) {
    var query = 'UPDATE scoreboard SET maidens = maidens + 1 where matchId=' + matchId + ' and playerId = ' + bowler;
    connection.query(query, function(error, results, fields) {
        if (error) {
            error.error = true;
            callback(error);
        } else {
            callback("success");
        }
    });
}


var updateBatBowlCareer = function(matchId, updateProfilesCbk) {

    var selectAll = 'SELECT playerId from scoreboard where matchId =' + matchId;

    connection.query(selectAll, function(error, playerList, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            if (playerList && playerList.length) {
                var i = 0;
                var updateCbk = function(resObj) {
                    if (resObj.error) {
                        updateProfilesCbk(resObj);
                    } else {
                        if (i + 1 < playerList.length) {
                            i = i + 1;
                            updateProfiles(playerList[i].playerId, updateCbk);
                        } else {
                            updateProfilesCbk("success");
                        }
                    }
                }
                updateProfiles(playerList[i].playerId, updateCbk);
            }
        }
    });

    var updateProfiles = function(playerId, callback) {

        var query = 'SELECT * from scoreboard where playerId=' + playerId + ' and matchId =' + matchId;
        var scoreBoardError = "";
        connection.query(query, function(error, playerScoreboardData, fields) {
            if (error) {
                scoreBoardError = error;
            } else {
                //Common params
                var matches = 0
                    //Batting params
                var batInnings = 0,
                    notout = 0,
                    runs = 0,
                    batBalls = 0,
                    hscore = 0,
                    fours = 0,
                    sixes = 0,
                    thirtyplus = 0,
                    fiftyplus = 0,
                    hundredplus = 0;
                //Bowling params
                var bowlInnings = 0,
                    bowledBalls = 0,
                    bowledRuns = 0,
                    wickets = 0,
                    bbm = "",
                    bbmWkts = 0,
                    bbmRuns = 0,
                    threeplus = 0,
                    fiveplus = 0;

                for (obj in playerScoreboardData) {
                    var currMatch = playerScoreboardData[obj];
                    if (currMatch) {
                        //Common param to bat and bowl
                        matches++;

                        //Batting params
                        batInnings = (currMatch.batStatus && currMatch.batStatus.toLowerCase() != "yet to bat") ? (batInnings + 1) : batInnings;
                        notout = (currMatch.batStatus && currMatch.batStatus.toLowerCase() != "out") ? (notout + 1) : notout;
                        runs += currMatch.runScored;
                        batBalls += currMatch.ballsFaced;
                        hscore = (hscore < currMatch.runsScored) ? currMatch.runsScored : hscore;
                        fours += currMatch.fours;
                        sixes += currMatch.sixes;
                        thirtyplus = (currMatch.runScored >= 30) ? (thirtyplus + 1) : thirtyplus;
                        fiftyplus = (currMatch.runScored >= 50) ? (fiftyplus + 1) : fiftyplus;
                        hundredplus = (currMatch.runScored >= 100) ? (hundredplus + 1) : hundredplus;
                        //Batting params

                        //Bowling params
                        bowlInnings = (currMatch.bowlStatus && (currMatch.bowlStatus.toLowerCase() == "bowling" || currMatch.bowlStatus.toLowerCase() == "bowled")) ? (bowlInnings + 1) : bowlInnings;
                        bowledBalls += currMatch.ballsBowled;
                        bowledRuns += currMatch.runsGiven;
                        wickets += currMatch.wickets;
                        if (currMatch.wickets > bbmWkts) {
                            bbmWkts = currMatch.wickets;
                            bbmRuns = currMatch.runsGiven;
                        } else if (currMatch.wickets == bbmWkts && bbmRuns > currMatch.runsGiven) {
                            bbmWkts = currMatch.wickets;
                            bbmRuns = currMatch.runsGiven;
                        }
                        bbm = "" + bbmRuns + "/" + bbmWkts;
                        threeplus += (currMatch.wickets >= 3) ? (threeplus + 1) : threeplus;
                        fiveplus += (currMatch.wickets >= 5) ? (fiveplus + 1) : fiveplus;
                        //Bowling params
                    }
                }

                var batUpdateQuery = 'UPDATE batting_career SET matches=' + matches + ',innings=' + batInnings + ',notout=' + notout + ',runs=' + runs + ',balls=' + batBalls + ',hscore=' + hscore + ',fours=' + fours + ',sixes=' + sixes + ',30plus=' + thirtyplus + ',50plus=' + fiftyplus + ',100plus=' + hundredplus + ' where playerId=' + playerId;

                connection.query(batUpdateQuery, function(error, results, fields) {
                    if (error) {
                        error.error = true;
                        callback(error);
                    } else if (results && results.affectedRows > 0) {
                        var bowlUpdateQuery = 'UPDATE bowling_career SET matches=' + matches + ',innings=' + bowlInnings + ',balls=' + bowledBalls + ',runs=' + bowledRuns + ',wickets=' + wickets + ',bbm="' + bbm + '",3plus=' + threeplus + ',5plus=' + fiveplus + ' where playerId=' + playerId;
                        connection.query(bowlUpdateQuery, function(error, results, fields) {
                            if (error) {
                                error.error = true;
                                callback(error);
                            } else if (results && results.affectedRows > 0) {
                                callback("success");
                            } else {
                                error.error = true;
                                callback(error);
                            }
                        });
                    } else {
                        error.error = true;
                        callback(error);
                    }
                });


            }
        });
    }

};

var updateTeamScoreDetails = function(matchId, callback) {
    if (matchId) {
        var matchScheduleQuery = "SELECT * from match_schedule where matchId =" + matchId;
        connection.query(matchScheduleQuery, function(error, matchDetail, fields) {
            if (error) {
                error.error = true;
                callback(error);
            } else {
                var matchDetail = (matchDetail.length && matchDetail[0]) ? matchDetail[0] : null;
                if (matchDetail) {
                    var teamA = matchDetail.teamAId;
                    var teamB = matchDetail.teamBId;

                    var teamAScoreboardQuery = "SELECT count(*) as teamACount from scoreboard where matchId =" + matchId + " and teamId = '" + teamA + "' and elevenType = 1";
                    connection.query(teamAScoreboardQuery, function(error, teamAScoreBoardDetail, fields) {
                        if (error) {
                            error.error = true;
                            callback(error);
                        } else {
                            var teamBScoreboardQuery = "SELECT count(*) as teamBCount from scoreboard where matchId =" + matchId + " and teamId = '" + teamB + "' and elevenType = 1";
                            connection.query(teamBScoreboardQuery, function(error, teamBScoreBoardDetail, fields) {
                                if (error) {
                                    error.error = true;
                                    callback(error);
                                } else {
                                    var teamListQuery = 'SELECT matches,won,lost,draw,points,ballsFaced,runsScored,ballsBowled,runsConceded,runRate,teamId from team_list where teamId ="' + teamA + '" or teamId = "' + teamB + '"';
                                    connection.query(teamListQuery, function(error, teamDetail, fields) {
                                        if (error) {
                                            error.error = true;
                                            callback(error);
                                        } else {
                                            if ((teamA == matchDetail.tossWonBy && matchDetail.electedToBat == 1) || matchDetail.electedToBat == 0) {
                                                batTeam = teamA;
                                                bowlTeam = teamB;
                                            } else {
                                                batTeam = teamB;
                                                bowlTeam = teamA;
                                            }

                                            var batTeamObj = {},
                                                bowlTeamObj = {};

                                            for (i = 0; i < teamDetail.length; i++) {
                                                if (teamDetail[i].teamId == batTeam) {
                                                    batTeamObj = teamDetail[i];
                                                } else {
                                                    bowlTeamObj = teamDetail[i];
                                                }
                                            }

                                            batTeamObj.matches += 1;
                                            batTeamObj.won = (matchDetail.teamWon == batTeam) ? (batTeamObj.won + 1) : batTeamObj.won;
                                            batTeamObj.lost = (matchDetail.teamWon != batTeam) ? (batTeamObj.lost + 1) : batTeamObj.lost;
                                            batTeamObj.draw = (matchDetail.teamWon != batTeam && matchDetail.teamWon != bowlTeam) ? (batTeamObj.draw + 1) : batTeamObj.draw;
                                            batTeamObj.points = (matchDetail.teamWon == batTeam) ? (batTeamObj.points + 2) : batTeamObj.points;

                                            batTeamObj.runsScored += (batTeam == teamA) ? matchDetail.teamAScore : matchDetail.teamBScore;
                                            batTeamObj.ballsBowled += (batTeam == teamA) ? matchDetail.teamBBalls : matchDetail.teamABalls;
                                            batTeamObj.runsConceded += (batTeam == teamA) ? matchDetail.teamBScore : matchDetail.teamAScore;

                                            bowlTeamObj.matches += 1;
                                            bowlTeamObj.won = (matchDetail.teamWon == bowlTeam) ? (bowlTeamObj.won + 1) : bowlTeamObj.won;
                                            bowlTeamObj.lost = (matchDetail.teamWon != bowlTeam) ? (bowlTeamObj.lost + 1) : bowlTeamObj.lost;
                                            bowlTeamObj.draw = (matchDetail.teamWon != batTeam && matchDetail.teamWon != bowlTeam) ? (bowlTeamObj.draw + 1) : bowlTeamObj.draw;
                                            bowlTeamObj.points = (matchDetail.teamWon == bowlTeam) ? (bowlTeamObj.points + 2) : bowlTeamObj.points;

                                            var ballsFaced = 0,
                                                wickets = 0,
                                                ballsFaced = 0;
                                            if (batTeam == teamA) {
                                                wickets = matchDetail.teamAWicket;
                                                if (wickets && wickets == (teamAScoreBoardDetail.teamACount - 1)) {
                                                    batTeamObj.ballsFaced += matchDetail.matchBalls;
                                                } else {
                                                    batTeamObj.ballsFaced += matchDetail.teamABalls;
                                                }
                                            } else {
                                                wickets = matchDetail.teamBWicket;
                                                if (wickets && wickets == (teamBScoreBoardDetail.teamBCount - 1)) {
                                                    batTeamObj.ballsFaced += matchDetail.matchBalls;
                                                } else {
                                                    batTeamObj.ballsFaced += matchDetail.teamBBalls;
                                                }
                                            }

                                            if (bowlTeam == teamA) {
                                                wickets = matchDetail.teamAWicket;
                                                if (wickets && wickets == (teamAScoreBoardDetail.teamACount - 1)) {
                                                    bowlTeamObj.ballsFaced += matchDetail.matchBalls;
                                                } else {
                                                    bowlTeamObj.ballsFaced += matchDetail.teamABalls;
                                                }
                                            } else {
                                                wickets = matchDetail.teamBWicket;
                                                if (wickets && wickets == (teamBScoreBoardDetail.teamBCount - 1)) {
                                                    bowlTeamObj.ballsFaced += matchDetail.matchBalls;
                                                } else {
                                                    bowlTeamObj.ballsFaced += matchDetail.teamBBalls;
                                                }
                                            }

                                            bowlTeamObj.runsScored += (bowlTeam == teamA) ? matchDetail.teamAScore : matchDetail.teamBScore;
                                            bowlTeamObj.ballsBowled += (bowlTeam == teamA) ? matchDetail.teamBBalls : matchDetail.teamABalls;
                                            bowlTeamObj.runsConceded += (bowlTeam == teamA) ? matchDetail.teamBScore : matchDetail.teamAScore;
                                            batTeamObj.runRate = batTeamObj.runsScored / (batTeamObj.ballsFaced / 6);
                                            bowlTeamObj.runRate = bowlTeamObj.runsScored / (bowlTeamObj.ballsFaced / 6);

                                            connection.query('UPDATE team_list SET ? where teamId = ?', [batTeamObj, batTeam], function(error, results, fields) {
                                                if (error) {
                                                    error.error = true;
                                                    callback(error);
                                                } else if (results.affectedRows && results.affectedRows > 0) {
                                                    connection.query('UPDATE team_list SET ? where teamId = ?', [bowlTeamObj, bowlTeam], function(error, results, fields) {
                                                        if (error) {
                                                            error.error = true;
                                                            callback(error);
                                                        } else {
                                                            callback("success");
                                                        }
                                                    });
                                                } else {
                                                    error.error = true;
                                                    callback(error);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    } else {
        error.error = true;
        callback(error);
    }
};
exports.updateTeamScoreDetails = function(req, res) {
    var reqBody = req.body;
    var matchId = reqBody.matchId;
    var callback = function(resObj) {
        if (resObj.error) {
            res.send(responseFormatter(400, (resObj.sqlMessage ? resObj.sqlMessage : ""), null, (resObj.code ? resObj.code : "")));
        } else {
            res.send(200, "Success", " Updated Successfully");
        }
    };
    updateTeamScoreDetails(matchId, callback);
}

exports.insertScoreToMongo = function(req, res) {

    var data = {}
    var reqBody = req.body;
    data.matchId = reqBody.matchid; //int
    data.teamId = reqBody.teamid; //str
    data.battingTeamId = reqBody.battingteamid; //str
    data.bowlTeamId = reqBody.bowlteamid; //str
    data.seriesId = reqBody.seriesid; //str
    data.batsmen1 = reqBody.batsmen1; //int
    data.batsmen1Name = reqBody.batsmen1name; //str
    data.batsmen2 = reqBody.batsmen2;
    data.batsmen2Name = reqBody.batsmen2name;
    data.bowler = reqBody.bowler; //int
    data.bowlerName = reqBody.bowlername;
    data.ballsfaced = reqBody.ballsfaced;
    data.overno = parseInt(data.ballsfaced / 6);
    data.ballno = parseInt(data.ballsfaced % 6);
    data.run = reqBody.run; //int
    data.extraType = reqBody.extratype; // str
    data.extraRun = reqBody.extrarun; //int
    data.outType = reqBody.outtype; //str
    data.outPlayer = reqBody.outplayer; //int
    data.outBy = reqBody.outby; //int
    data.boundary = reqBody.boundary; //bool
    data.droppedCatch = reqBody.droppedCatch; //true or false
    data.commentary = reqBody.commentary; //str

    var m_scoreboard = {};

    var getNextBowler = function(ballno, extraType) {
        if (ballno == 6 && !checkReBall(extraType)) {
            m_scoreboard.triggerBowlerChange = true;
        }
    };

    var getNextBatsmen = function() {
        m_scoreboard.triggerBatsmenChange = true;
    };

    var checkReBall = function(extraType) {
        var reBall = false;
        if (extraType == "NB" || extraType == "WD") {
            reBall = true;
        }
        return reBall;
    };
    var t_run = data.extraRun;
    if (data.extraType) {
        var eType = data.extraType;
        if (eType == "NB") {
            data.totalRun = data.run + 1;
            t_run = data.run;
        } else if (eType == "BYES") {
            data.totalRun = data.extraRun;
        } else {
            // WD or NBWBYES
            data.totalRun = data.extraRun + 1;
        }
    } else {
        data.totalRun = data.run;
        t_run = data.run;
    }

    var updateForNext = function() {
        if (data.outType) {
            getNextBatsmen();
            getNextBowler(data.ballno, data.extraType);
            updateInMongo();
        } else if ((((data.ballno < 5) || checkReBall(data.extraType)) && (t_run % 2 == 1)) || ((data.ballno == 5) && !checkReBall(data.extraType) && (t_run % 2 == 0))) {
            var updateStrikerCbk = function(updateRes) {
                if (updateRes.error) {
                    res.send(responseFormatter(400, (updateRes.sqlMessage ? updateRes.sqlMessage : ""), null, (updateRes.code ? updateRes.code : "")));
                } else {
                    getNextBowler(data.ballno, data.extraType);
                    updateInMongo();
                }
            };
            updateStriker(data.batsmen2, data.matchId, updateStrikerCbk);
        } else {
            updateInMongo();
        }
    };

    var getCurrScorecardCbk = function(resObj) {
        if (resObj.error) {
            res.send(responseFormatter(400, (resObj.sqlMessage ? resObj.sqlMessage : ""), null, (resObj.code ? resObj.code : "")));
        } else {

            var afterMaidenOverUpdate = function() {
                res.send({
                    "code": 200,
                    "success": "successMsg",
                    "result": "Score updated",
                    "data": resObj
                });
            };
            var afterMaidenOverCheck = function(isMaiden) {
                if (isMaiden) {
                    updateMaidenOver(data.matchId, data.bowler, afterMaidenOverUpdate);
                } else {
                    afterMaidenOverUpdate();
                }
            }
            if (data.ballno == 6 && !checkReBall(data.extraType)) {
                checkMaidenOver(data, afterMaidenOverCheck)
            } else {
                afterMaidenOverUpdate();
            }
        }
    }

    var updateInMongo = function() {
        MongoClient.connect(mongoDBUrl, function(err, db) {
            var collection = db.db(mongoDBName).collection("match" + data.matchId + data.seriesId.replace("-", ""));
            collection.insertOne(data, function(err, results) {
                if (err) console.log(err);
                var callbackMaiden = function(isMaiden) {
                    var getCurrScorecardCbk = function(resObj) {
                        if (resObj.error) {
                            res.send(responseFormatter(400, (resObj.sqlMessage ? resObj.sqlMessage : ""), null, (resObj.code ? resObj.code : "")));
                        } else {
                            res.send({
                                "code": 200,
                                "success": "successMsg",
                                "result": "Score updated",
                                "data": resObj
                            });
                        }
                    }
                    var afterMaidenOverUpdate = function() {
                        getCurrScorecard(data.matchId, data.battingTeamId, data.bowlTeamId, getCurrScorecardCbk, m_scoreboard);
                    };
                    if (isMaiden) {
                        updateMaidenOver(data.matchId, data.bowler, afterMaidenOverUpdate);
                    } else {
                        afterMaidenOverUpdate();
                    }
                }
                if (data.ballno == 6 && !checkReBall(data.extraType)) {
                    // Check Maiden
                    collection.find({ $and: [{ 'teamId': data.teamId }, { 'overno': data.overno }] }).toArray(function(err, results) {
                        if (err) console.log(err);
                        db.close();
                        var maiden = true;
                        if (results && results.length) {
                            for (var i = 0; i < results.length; i++) {
                                if (results[i].run != 0 || results[i].extraRun != 0 || results[i].extraType) {
                                    maiden = false;
                                    break;
                                }
                            }
                        } else {
                            maiden = false;
                        }
                        callbackMaiden(maiden);
                    });
                } else {
                    callbackMaiden(false);
                }

            });
        });
    };

    var updateBattingDataCbk = function(batRes, resCode) {
        if (batRes.error) {
            res.send(responseFormatter(400, (batRes.sqlMessage ? batRes.sqlMessage : ""), null, (batRes.code ? batRes.code : "")));
        } else {
            var updateBowlingDataCbk = function(bowlRes, resCode) {
                if (bowlRes.error) {
                    res.send(responseFormatter(400, (bowlRes.sqlMessage ? bowlRes.sqlMessage : ""), null, (bowlRes.code ? bowlRes.code : "")));
                } else {
                    var updateMatchScoreCbk = function(matchDet, resCode) {
                        if (matchDet.error) {
                            res.send(responseFormatter(400, (matchDet.sqlMessage ? matchDet.sqlMessage : ""), null, (matchDet.code ? matchDet.code : "")));
                        } else {
                            updateForNext();
                        }
                    };
                    updateMatchScore(data, updateMatchScoreCbk);
                }
            };
            updateBowlingData(data, updateBowlingDataCbk);
        }
    };
    updateBattingData(data, updateBattingDataCbk);
}

exports.getPlayerDetails = function(req, res) {
    var playerId = (req.body && req.body.playerid) ? req.body.playerid : "";
    if (playerId) {
        /*connection.query('SELECT p.*, batc.*, bowc.*  FROM player_profile p INNER JOIN batting_career batc ON (batc.playerId = p.playerId) INNER JOIN bowling_career bowc ON (bowc.playerId = p.playerId) where p.playerId =' + playerId, function(error, playerDetailObj, fields) {
			if (error) {
	            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
	        } else {
	            res.send(responseFormatter(200, "Success", playerDetailObj));
	        }
	    });	*/
        var playerProfQuery = "SELECT * FROM player_profile where playerId=" + playerId;
        var playerBatQuery = "SELECT * FROM batting_career where playerId=" + playerId;
        var playerBowlQuery = "SELECT * FROM bowling_career where playerId=" + playerId;
        var respObj = {};
        var errMsg = "";

        connection.query(playerProfQuery, function(error, playerProfObj, fields) {
            if (error) {
                errMsg = (error.sqlMessage) ? error.sqlMessage : "";
            } else {
                respObj.playerProfile = playerProfObj;

                connection.query(playerBatQuery, function(error, playerBatObj, fields) {
                    if (error) {
                        errMsg = (error.sqlMessage) ? error.sqlMessage : "";
                    } else {
                        respObj.battingCareer = playerBatObj;

                        connection.query(playerBowlQuery, function(error, playerBowlObj, fields) {
                            if (error) {
                                errMsg = (error.sqlMessage) ? error.sqlMessage : "";
                            } else {
                                respObj.bowlingCareer = playerBowlObj;

                                if (!errMsg) {
                                    res.send(responseFormatter(200, "Success", respObj));
                                } else {
                                    res.send(responseFormatter(400, errMsg, null, "FAILED"));
                                }
                            }
                        });
                    }
                });
            }
        });
    } else {
        res.send(responseFormatter(400, "PlayerId parameter is missing", null, "FAILED"));
    }
}

exports.insertScoreToMongoIO = function(obj, callback) {

    var data = {}
    var reqBody = obj;
    data.matchId = reqBody.matchid; //int
    data.teamId = reqBody.teamid; //str
    data.battingTeamId = reqBody.battingteamid; //str
    data.bowlTeamId = reqBody.bowlteamid; //str
    data.seriesId = reqBody.seriesid; //str
    data.batsmen1 = reqBody.batsmen1; //int
    data.batsmen1Name = reqBody.batsmen1name; //str
    data.batsmen2 = reqBody.batsmen2;
    data.batsmen2Name = reqBody.batsmen2name;
    data.bowler = reqBody.bowler; //int
    data.bowlerName = reqBody.bowlername;
    data.ballsfaced = reqBody.ballsfaced;
    data.overno = parseInt(data.ballsfaced / 6);
    data.ballno = parseInt(data.ballsfaced % 6);
    data.run = reqBody.run; //int
    data.extraType = reqBody.extratype; // str
    data.extraRun = reqBody.extrarun; //int
    data.outType = reqBody.outtype; //str
    data.outPlayer = reqBody.outplayer; //int
    data.outBy = reqBody.outby; //int
    data.boundary = reqBody.boundary; //bool
    data.droppedCatch = reqBody.droppedCatch; //true or false
    data.commentary = reqBody.commentary; //str

    var m_scoreboard = {};

    var getNextBowler = function(ballno, extraType) {
        if (ballno == 6 && !checkReBall(extraType)) {
            m_scoreboard.triggerBowlerChange = true;
        }
    };

    var getNextBatsmen = function() {
        m_scoreboard.triggerBatsmenChange = true;
    };

    var checkReBall = function(extraType) {
        var reBall = false;
        if (extraType == "NB" || extraType == "WD") {
            reBall = true;
        }
        return reBall;
    };
    var t_run = data.extraRun;
    if (data.extraType) {
        var eType = data.extraType;
        if (eType == "NB") {
            data.totalRun = data.run + 1;
            t_run = data.run;
        } else if (eType == "BYES") {
            data.totalRun = data.extraRun;
        } else {
            // WD or NBWBYES
            data.totalRun = data.extraRun + 1;
        }
    } else {
        data.totalRun = data.run;
        t_run = data.run;
    }

    var updateForNext = function() {
        if (data.outType) {
            getNextBatsmen();
            getNextBowler(data.ballno, data.extraType);
            updateInMongo();
        } else if ((((data.ballno < 5) || checkReBall(data.extraType)) && (t_run % 2 == 1)) || ((data.ballno == 5) && !checkReBall(data.extraType) && (t_run % 2 == 0))) {
            var updateStrikerCbk = function(updateRes) {
                if (updateRes.error) {
                    callback(responseFormatter(400, (updateRes.sqlMessage ? updateRes.sqlMessage : ""), null, (updateRes.code ? updateRes.code : "")));
                } else {
                    getNextBowler(data.ballno, data.extraType);
                    updateInMongo();
                }
            };
            updateStriker(data.batsmen2, data.matchId, updateStrikerCbk);
        } else {
            updateInMongo();
        }
    };

    var getCurrScorecardCbk = function(resObj) {
        if (resObj.error) {
            callback(responseFormatter(400, (resObj.sqlMessage ? resObj.sqlMessage : ""), null, (resObj.code ? resObj.code : "")));
        } else {

            var afterMaidenOverUpdate = function() {
                callback({
                    "code": 200,
                    "success": "successMsg",
                    "result": "Score updated",
                    "data": resObj
                });
            };
            var afterMaidenOverCheck = function(isMaiden) {
                if (isMaiden) {
                    updateMaidenOver(data.matchId, data.bowler, afterMaidenOverUpdate);
                } else {
                    afterMaidenOverUpdate();
                }
            }
            if (data.ballno == 6 && !checkReBall(data.extraType)) {
                checkMaidenOver(data, afterMaidenOverCheck)
            } else {
                afterMaidenOverUpdate();
            }
        }
    }

    var updateInMongo = function() {
        MongoClient.connect(mongoDBUrl, function(err, db) {
            var collection = db.db(mongoDBName).collection("match" + data.matchId + data.seriesId.replace("-", ""));
            collection.insertOne(data, function(err, results) {
                if (err) console.log(err);
                var callbackMaiden = function(isMaiden) {
                    var getCurrScorecardCbk = function(resObj) {
                        if (resObj.error) {
                            callback(responseFormatter(400, (resObj.sqlMessage ? resObj.sqlMessage : ""), null, (resObj.code ? resObj.code : "")));
                        } else {
                            callback({
                                "code": 200,
                                "success": "successMsg",
                                "result": "Score updated",
                                "data": resObj
                            });
                        }
                    }
                    var afterMaidenOverUpdate = function() {
                        getCurrentScoreCardIO({ "matchid": data.matchId }, getCurrScorecardCbk, m_scoreboard)
                    };
                    if (isMaiden) {
                        updateMaidenOver(data.matchId, data.bowler, afterMaidenOverUpdate);
                    } else {
                        afterMaidenOverUpdate();
                    }
                }
                if (data.ballno == 6 && !checkReBall(data.extraType)) {
                    // Check Maiden
                    collection.find({ $and: [{ 'teamId': data.teamId }, { 'overno': data.overno }] }).toArray(function(err, results) {
                        if (err) console.log(err);
                        db.close();
                        var maiden = true;
                        if (results && results.length) {
                            for (var i = 0; i < results.length; i++) {
                                if (results[i].run != 0 || results[i].extraRun != 0 || results[i].extraType) {
                                    maiden = false;
                                    break;
                                }
                            }
                        } else {
                            maiden = false;
                        }
                        callbackMaiden(maiden);
                    });
                } else {
                    callbackMaiden(false);
                }

            });
        });
    };

    var updateBattingDataCbk = function(batRes, resCode) {
        if (batRes.error) {
            callback(responseFormatter(400, (batRes.sqlMessage ? batRes.sqlMessage : ""), null, (batRes.code ? batRes.code : "")));
        } else {
            var updateBowlingDataCbk = function(bowlRes, resCode) {
                if (bowlRes.error) {
                    callback(responseFormatter(400, (bowlRes.sqlMessage ? bowlRes.sqlMessage : ""), null, (bowlRes.code ? bowlRes.code : "")));
                } else {
                    var updateMatchScoreCbk = function(matchDet, resCode) {
                        if (matchDet.error) {
                            callback(responseFormatter(400, (matchDet.sqlMessage ? matchDet.sqlMessage : ""), null, (matchDet.code ? matchDet.code : "")));
                        } else {
                            updateForNext();
                        }
                    };
                    updateMatchScore(data, updateMatchScoreCbk);
                }
            };
            updateBowlingData(data, updateBowlingDataCbk);
        }
    };
    updateBattingData(data, updateBattingDataCbk);
}

exports.updateStrikerIO = function(batsman, matchId, callback) {
    var query = 'UPDATE scoreboard SET striker = IF(playerId=' + batsman + ',"true","false") where matchId=' + matchId;
    connection.query(query, function(error, results, fields) {
        if (error) {
            error.error = true;
            callback(error);
        } else {
            callback("success");
        }
    });
};
exports.changeBatsmen = function(req, res) {
    var batsman = req.body.batsmen;
    var matchId = req.body.matchid;
    var query = 'UPDATE scoreboard SET batStatus = "not out" where playerId = ' + batsman + ' and matchId=' + matchId;
    connection.query(query, function(error, results, fields) {
        if (error) {
            res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
        } else {
            res.send(responseFormatter(200, "Success", "Updated Sucessfully"));
        }
    });
};

exports.endInningsUpdate = function(req, res) {
    var team = req.body.team ? req.body.team : "";
    var matchId = req.body.matchid ? req.body.matchid : "";
    if (team) {
        var query = 'UPDATE match_schedule SET ' + team + ' = true where matchId=' + matchId;
        connection.query(query, function(error, results, fields) {
            if (error) {
                res.send(responseFormatter(400, (error.sqlMessage ? error.sqlMessage : ""), null, (error.code ? error.code : "")));
            } else {
                var data = {}
                data.redirectUrl = "matchinfo?mId=" + matchId;
                res.send(responseFormatter(200, "Updated Sucessfully", data, "Success"));
            }
        });
    } else {
        res.send(responseFormatter(400, "Missing", null, "Missing team"));
    }

};

exports.getLiveMatchesIO = function(obj, callback) {
    var query = "";
    if (obj && obj.seriesId) {
        query = "SELECT t1.teamName as teamAName, t1.teamNickName as teamANickName, t2.teamName as teamBName, t2.teamNickName as teamBNickName, m.* FROM match_schedule m INNER JOIN team_list t1 ON (m.teamAId = t1.teamId) INNER JOIN team_list t2 ON (m.teamBId = t2.teamId)  where tossWonBy IS NOT NULL and teamWon IS NULL and seriesId = " + obj.seriesId;
    } else {
        query = "SELECT t1.teamName as teamAName, t1.teamNickName as teamANickName, t2.teamName as teamBName, t2.teamNickName as teamBNickName, m.* FROM match_schedule m INNER JOIN team_list t1 ON (m.teamAId = t1.teamId) INNER JOIN team_list t2 ON (m.teamBId = t2.teamId)  where tossWonBy IS NOT NULL and teamWon IS NULL";
    }
    connection.query(query, function(error, results, fields) {
        if (error) {
            error.error = true;
            callback(error);
        } else {
            callback(results);
        }
    });
}

exports.logout = function(req, res) {
    req.session.destroy();
    res.send({
        "code": 302,
        "success": "Logout Success",
        "redirectUrl": "login",
    });
}