var ip = require('ip');

module.exports = {
    'secret': "ilovescotchyscotch",
    'host': ip.address(),
    'mysqlDBHost': "192.168.234.132",
    'port': "8080",
    'dbUser': "root",
    'dbPwd': 'skava',
    'dbName': 'crickzz',
    'mongoDBUrl': 'mongodb://192.168.234.132:27017/crickzz',
    'mongoDBName': 'crickzz',
    'sessionExpTime': 120,
    'superAdminRegexUrl': "/(dashboard)|(createseries)|(playeralloc)|(selectmatch)|(startinnings)|(updatescore)|(matchinfo)/g", //"/(playerprofile)|(dashboard)|(teamreg)|(playeralloc)|(schedule)|(selectmatch)|(createseries)/g",
    'superAdminHtmlRegexUrl': /(\/playerprofile)|(\/teamregistration)|(\/matchschedule)|(\/ballbyballscore)/,
    'adminRegexUrl': "/(/selectmatch)|(/dashboard)/g",
    'teamUserRegexUrl': "/(/playerprofile)|(/dashboard)|(/playeralloc)/g",
    'clientPages': /(\/home)|(\/livescore)|(\/chat2me)/, //|(\/teamlist)|(\/schedules)|(\/pointstable)|(\/gallery)|(\/scorecard)|(\/teamSquad)|(\/profiles)
    'superAdmin': [
        { 'name': "Player Profile", 'url': "playerprofile" },
        { 'name': "Create Series", 'url': "createseries" },
        { 'name': "Team Registration", 'url': "teamregistration" },
        { 'name': "Match Schedule", 'url': "matchschedule" },
        { 'name': "Match List", 'url': "selectmatch" }
    ],
    'admin': [
        { 'name': "Match List", 'url': "selectmatch" }
    ],
    'teamUser': [
        { 'name': "Player Profile", 'url': "playerprofile" },
        { 'name': "Player Allocation", 'url': "playeralloc" }
    ],
    "responseCode": { "200": "SUCCESS", "201": "CREATED", "204": "NOT_EXIST", "400": "ERROR", "401": "UN_AUTHORIZED" }
};