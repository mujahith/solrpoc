$(document).ready(function() {
    var mId = getParameterByName('mId');
    var reqConf = {};
    reqConf.matchId = mId;
    renderMatchInfoPage();

    function renderMatchInfoPage() {
        var matchConfig = {
            "matchid": reqConf.matchId
        }
        var successCbk = function(response) {
            if (response) {
                console.log(response);
                if (response.data && response.data.length)
                {
                    reqConf.matchInfo = response.data;                    
                    
                    var respData = response.data[0];
                    var dateTime = new Date(respData.dateTime);
                    var date = dateTime.toDateString();
                    var time = dateTime.toLocaleTimeString();
                    
                    var tossWonTeamName = "";
                    if (respData.tossWonBy == respData.teamAId) {
                        tossWonTeamName = respData.teamAName;
                    } else {
                        tossWonTeamName = respData.teamBName;
                    }

                    var template = ""+                                          
                    '<div class="fieldGroup">'+
                        '<div class="matchNo">' + respData.matchNo + '</div>'+
                        '<div class="teamVs"> - </div>'+
                        '<div class="teamAName">' + respData.teamAName + '</div>'+
                        '<div class="teamVs"> vs </div>'+
                        '<div class="teamBName"> ' + respData.teamBName + '</div>'+
                    '</div>'+
                    '<div class="fieldGroup">'+
                        '<div class="matchDate"><label class="matchInfoLabel">Date: </label>'+ date +'</div>'+
                        '<div class="matchTime"><label class="matchInfoLabel">Time: </label>'+ time +'</div>'+
                        '<div class="matchVenue"><label class="matchInfoLabel">Venue: </label>' + respData.venue + '</div>'+
                    '</div>'+
                    '<div class="fieldGroup">'+                            
                        '<div class="matchTossWon"><label class="matchInfoLabel">Toss Won By: </label>'+ tossWonTeamName +' </div>'+                            
                        '<div class="matchElectedTo"><label class="matchInfoLabel">Elected To: </label>'+ (respData.electedToBat && respData.electedToBat == 1 ? "Bat" : "Bowl") +' </div>'+ 
                    '</div>'+
                    '<div class="fieldGroup">'+
                    '<div class="matchOver"><label class="matchInfoLabel">Overs: </label>' + parseInt(respData.matchBalls / 6) + "." + (respData.matchBalls % 6) + ' </div>'+
                        '<div class="matchUmpiresList"><label class="matchInfoLabel">Umpires: </label>' + respData.umpire1 + ', ' + respData.umpire2 + '</div>'+
                        '<div class="matchScorer"><label class="matchInfoLabel">Scorer: </label>' + respData.scorer + '</div>'+
                    '</div>';
                        
                    var startInningsFirst = "",
                        startInningsSecond = "", showSecInng = false, showFirstIng = true,showSecInng = "";
                    var batTeam = (respData.electedToBat && respData.electedToBat == 1 ? true : false);
                    if (batTeam) {
                        startInningsFirst = respData.tossWonBy;
                        if (respData.tossWonBy == respData.teamAId) {
                            startInningsSecond = respData.teamBId;
                            showSecInng = "teamBInngStatus";
                        } else {
                            startInningsSecond = respData.teamAId;
                            showSecInng = "teamAInngStatus";                                
                        }
                    } else {
                        if (respData.tossWonBy == respData.teamAId) {
                            startInningsFirst = respData.teamBId;
                            startInningsSecond = respData.teamAId;
                            showSecInng = "teamAInngStatus";
                        } else {
                            startInningsFirst = respData.teamAId;
                            startInningsSecond = respData.teamBId;
                            showSecInng = "teamBInngStatus";
                        }
                    }
                    if(showSecInng == "teamAInngStatus"){
                        showSecondIng = respData.teamAInngStatus ? false: true;
                        showFirstIng = respData.teamBInngStatus ? false: true;
                    }else if(showSecInng == "teamBInngStatus"){
                        showSecondIng = respData.teamBInngStatus ? false: true;
                        showFirstIng = respData.teamAInngStatus ? false: true;
                    }
                    reqConf.InngFirst = startInningsFirst;
                    reqConf.InngSecnd = startInningsSecond;
                    var teamWonName = "",updateTeam = false;
                    if( !(showFirstIng || showSecondIng ) && respData.teamWon)
                    {
                        if(respData.teamWon == respData.teamAId)
                        {
                            teamWonName = respData.teamAName;
                        }
                        else
                        {
                            teamWonName = respData.teamBName;
                        }
                        template += '<div class="fieldGroup teamWonDetails"><div class="teamWonName">'+ teamWonName +' Won</div></div>';
                    }
                    else
                    {
                        updateTeam = true;
                    }
                    template += '<div class="fieldGroup">'+
                        (showFirstIng ? '<button class="firstStartInngs" teamId="' + startInningsFirst + '">Start Innings First</button>':"")+
                        ((!showFirstIng && showSecondIng) ? '<button class="secStartInngs" teamId="' + startInningsSecond + '">Start Innings Second</button>':"")+
                        ((!showFirstIng && !showSecondIng && updateTeam) ? '<button class="updateMatchStatus" matchId="' + reqConf.matchId + '">Update Match Status</button>' : "")+'</div>';
                    $(".matchInfoCont").append(template);
                    registerEvents();
                }
            }
        };
        var errorCbk = function(data) {
            console.log(data);
        };
        ajaxReq(window.location.origin + "/api/getMatches", "POST", successCbk, errorCbk, null, matchConfig);
    }

    $('.updtBattingStatus').off("click").on("click", function() {
        var b1 = $("#firstBatDropDown option:selected").val();
        var b2 = $("#secBatDropDown option:selected").val();
        if (b1 != b2) {
            var updtBatConf = {
                "matchid": reqConf.matchId,
                "teamId": reqConf.teamId,
                "batsmen1id": b1,
                "batsmen2id": b2
            };
            makeUpdateBatCall(updtBatConf);
        } else {
            $('body').append(errorPopup("please select different two player"));
        }
    });

    $('.updtBowlingStatus').off("click").on("click", function(e) {
        var bowler = $("#bowlerList option:selected").val();
        var updtBowlConf = {
            "matchid": reqConf.matchId,
            "teamId": reqConf.currBowlerTeamId,
            "bowlerid": bowler
        };
        makeUpdateBowlCall(updtBowlConf);
    });

    function registerEvents() {
        $(".firstStartInngs").off("click").on("click", function(e) {
            reqConf.teamId = $(e.currentTarget).attr('teamid');
            reqConf.currBowlerTeamId = reqConf.InngSecnd;
            var currScoreCardConf = {
                "matchid": reqConf.matchId,
                "battingteamid": reqConf.teamId,
                "bowlteamid": reqConf.currBowlerTeamId
            };
            getCurrentScoreCard(currScoreCardConf);
        });
        $(".secStartInngs").off("click").on("click", function(e) {
        	reqConf.teamId = $(e.currentTarget).attr('teamid');
        	reqConf.currBowlerTeamId = reqConf.InngFirst;
        	var currScoreCardConf = {
                "matchid": reqConf.matchId,
                "battingteamid": reqConf.teamId,
                "bowlteamid": reqConf.currBowlerTeamId
            };
            getCurrentScoreCard(currScoreCardConf);
        });
        $(".updateMatchStatus").off("click").on("click",function(e)
        {
            getMatchStatusPopup();
        });
    }

    function getCurrentScoreCard(config) {
        var succCbk = function(response) {
            if (response) {
                var batsMan = ((response.data && response.data.batsMen) ? response.data.batsMen : "");
                var bowler = ((response.data && response.data.bowler) ? response.data.bowler : "");
                if (batsMan.length || bowler.length) {

                    window.location = "/ballbyballscore?mId=" + config.matchid + "&batId=" + config.battingteamid + "&bowId=" + config.bowlteamid;
                } else {
                    var matchPlayConf = {
                        "matchId": reqConf.matchId,
                        "teamId": reqConf.teamId
                    }
                    makeGetMatchPlayerCall(matchPlayConf, true);
                }
            }
        };
        var errorCbk = function(data) {
            console.log(data);
        };
        ajaxReq(window.location.origin + "/api/getcurrentscorecard", "POST", succCbk, errorCbk, null, config);
    }

    function makeGetMatchPlayerCall(config, isBat) {
        var succCbk = function(response) {
            if (response) {
                var playerList = ((response.data && response.data && response.data.players && response.data.players) ? response.data.players : "");
                if (playerList && playerList.length) {
                    renderDropDown(playerList, isBat);
                    updatePlayerList(isBat);
                } else {
                    $('body').append(errorPopup('Player is not available for the teamId "' + config.teamId + '"'));
                }
            }
        }
        var errorCbk = function(data) {
            console.log(data);
        };
        ajaxReq(window.location.origin + "/api/getmatchplayers", "POST", succCbk, errorCbk, null, config);
    }

    function renderDropDown(playerList, isBat) {
        var template = "";
        for (var i = 0; i < playerList.length; i++) {
            if(isBat)
            {
            	template += '<option class="batter" name="batter_' + i + '" value="' + playerList[i].playerId + '" status_batting="' + playerList[i].batStatus + '"' + ((playerList[i].batStatus == "out"  || playerList[i].batStatus == "Batting") ? 'disabled="disabled"' : "") + '>' + playerList[i].name + '</option>';
            }
            else
            {
            	template += '<option class="batter" name="batter_' + i + '" value="' + playerList[i].playerId + '" status_bowling="' + playerList[i].bowlStatus + '"' + ((playerList[i].bowlStatus == "Bowling") ? 'disabled="disabled"' : "") + '>' + playerList[i].name + '</option>';
            }
        }
        if (isBat) {
            $('#firstBatDropDown').append(template);
            $('#secBatDropDown').append(template);
        } else {
            $("#bowlerList").append(template);
        }
    }

    function updatePlayerList(isBat) {
        if (isBat) {
            $('.parentContMatchInfo').addClass('hide');
            $('.parentContBatterInfo').removeClass('hide');
        } else {
            $('.parentContBatterInfo').addClass('hide');
            $('.parentContBowlerInfo').removeClass('hide')
        }
    }

    function makeUpdateBatCall(config) {
        var successCbk = function(response) {
            var matchPlayConf = {
                "matchId": reqConf.matchId,
                "teamId": reqConf.currBowlerTeamId
            };
            makeGetMatchPlayerCall(matchPlayConf, false);
        };
        var errorCbk = function(data) {
            console.log(data);
        };
        ajaxReq(window.location.origin + "/api/updatebattingstatus", "POST", successCbk, errorCbk, null, config);
    }

    function makeUpdateBowlCall(config) {
        var successCbk = function(response) {
            var currScoreCardConf = {
                "matchid": reqConf.matchId,
                "battingteamid": reqConf.teamId,
                "bowlteamid": reqConf.currBowlerTeamId
            };
            getCurrentScoreCard(currScoreCardConf);
        };
        var errorCbk = function(data) {
            console.log(data);
        };
        ajaxReq(window.location.origin + "/api/updatebowlingstatus", "POST", successCbk, errorCbk, null, config);
    }

    function getMatchStatusPopup() {
        $('body').append(renderMatchStatusPopup());
        registerMatchStatusPopupEvents();
        renderMatchStatusDropDown(reqConf.matchInfo, true);
        getMatchPlayer();
    }
    function registerMatchStatusPopupEvents()
    {
    	$(".matchStatusContentCont .matchStatusbtn").off("click").on("click", function(e)
    	{
    		var tossWonVal = $("#matchWonList option:selected").attr('id');
    		var matchStatusVal = $('.matchStatusCont #matchStatus').val();
    		var manOfMatchVal = $("#manOfMatchList option:selected").attr('playerid');
    		if(tossWonVal && matchStatusVal && manOfMatchVal)
    		{
    			var config = {
                        "matchId": parseInt(reqConf.matchId),
                        "teamWon": tossWonVal.toString(),
                    	"matchStatus": matchStatusVal.toString(),
                        "mom": parseInt(manOfMatchVal)
                    };
        		updateMatchScore(config);  
    		}
    		else
    		{
    			$('body').append(errorPopup("please enter all details"));
    		}
    	});
    	$(".cancelPopup").click(function(){
    		$(".parentContMatchStatus").hide();
    	});
    }
    function updateMatchScore(config)
    {
    	var successCbk = function(resp)
    	{
    		if(resp)
    		{
    			$('body').append(errorPopup("success"));
    			window.location.reload();
    		}
    	};
    	var errorCbk = function(data) {
            console.log(data);
        };
        ajaxReq(window.location.origin + "/api/updatematchscore", "POST", successCbk, errorCbk, null, config);
    }
    function getMatchPlayer() {
        var config = {
            "matchId": reqConf.matchId
        };
        var successCbk = function(response) {
            if (response) {
                var playerList = ((response.data && response.data.players && response.data.players.length) ? response.data.players : "");
                renderMatchStatusDropDown(playerList, false);
            }
        };
        var errorCbk = function(error) {
            console.log(error);
        };
        ajaxReq(window.location.origin + "/api/getmatchplayers", "POST", successCbk, errorCbk, null, config, "json");
    }
    
    function renderMatchStatusDropDown(resp, isToss) {
        var temp = "";
        if (resp) {
            if (isToss) {
                for (var i = 0; i < 1; i++) {
                    temp = '<option name="' + resp[i].teamAName + '" id="' + resp[i].teamAId + '">' + resp[i].teamAName + '</option>' +
                        '<option name="' + resp[i].teamBName + '" id="' + resp[i].teamBId + '">' + resp[i].teamBName + '</option>';
                }
                $('.parentContMatchStatus #matchWonList').append(temp);
            } else {
                for (var i = 0; i < resp.length; i++) {
                    temp += '<option name="' + resp[i].name + '" teamId="' + resp[i].teamId + '" playerId="' + resp[i].playerId + '">' + resp[i].name + '</option>';
                }
                $('.parentContMatchStatus #manOfMatchList').append(temp);
            }
        }
    }
});