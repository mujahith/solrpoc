$(document).ready(function(){
	teamDetails.init();
});

var teamDetails = {
	teamData:{},
	teamdataUrl : window.location.origin + "/api/getteamlist",
	serieslistUrl : window.location.origin + "/api/serieslist",
	squadRedirectionUrl : window.location.origin + "/teamSquad",
	currentTime : 0,
	currentYear : 0,
	init : function()
	{
		var _self = this;
		teamDetails.getSeriesData(_self.serieslistUrl);
		teamDetails.registerEvents();
	},
	registerEvents : function()
	{
		var _self = this;
		$(".sclDrpSelectedYr, .sclDrpArrow").off("click").on("click", function(){
			$(".sclYearElmCont").slideToggle();
		});
	
		$(document).off("click").on("click", ".sclYearElm", function(){
			$(".teamInfo").removeClass('show');
			var selectedYr = $(this).html();
			_self.getTeamData(_self.teamdataUrl, selectedYr);
			$(".sclDrpSelectedYr").html(selectedYr);
			$(".sclYearElmCont").slideToggle();
		});
		$(document).off("click").on("click", ".f1_container", function(){
			var teamId = $(this).attr('teamId');
			var seriesId = $(this).attr('seriesId');
			window.location.href = _self.squadRedirectionUrl + "?teamId=" + teamId + "&seriesId=" + seriesId;
		});
		
	},
	getSeriesData : function(url)
	{
		var _self = this;
		ajaxReq(url, "POST", _self.renderYrDropdown, null, null);
	},
	getTeamData : function(url, seriesId)
	{
		var _self = this, postparam = {};
		postparam.seriesid = seriesId;
		ajaxReq(url, "POST", _self.renderTeamList, null, null, postparam);
	},
	renderYrDropdown : function(seriesData)
	{
		var _self = this;
		var drpElm = "";
		
		if(seriesData && seriesData.data.length > 0)
		{
			for (var i = 0; i < seriesData.data.length; i++)
			{
				if(seriesData.data[i] && seriesData.data[i].year)
				{
					drpElm = "<div class='sclYearElm'>" + seriesData.data[i].seriesId + "</div>";
					$(".sclYearElmCont").append(drpElm);
				}
			}
			_self.initialSeriesId = seriesData.data[0].seriesId ? seriesData.data[0].seriesId:"";
			$(".sclDrpSelectedYr").html(_self.initialSeriesId);
			teamDetails.getTeamData(teamDetails.teamdataUrl, _self.initialSeriesId);
		}
	},
	renderTeamList : function(teamData)
	{
		if(teamData.data && teamData.data.length > 0)
		{
			var strDiv = "";
			for (i=0; i < teamData.data.length ; i++)
			{
				strDiv+='<div class="f1_container" teamId="'+ teamData.data[i].teamId +'" seriesId="'+ teamData.data[i].seriesId  +'">';
					strDiv+='<div id="f1_card" class="shadow">';
						strDiv+='<div class="back face center">';
							strDiv+='<div class="teamLogo">';
							if(teamData.data[i].logoUrl && teamData.data[i].logoUrl != "")
							{
								strDiv+='<img src="'+teamData.data[i].logoUrl+'">';
							}
							strDiv+='</div>';
							if(teamData.data[i].teamName)
							{
								strDiv+='<div class="teamName">';
								strDiv+=teamData.data[i].teamName;	
								strDiv+='</div>';
							}
							if(teamData.data[i].captain)
							{
								strDiv+='<div class="teamCaptain">Captain  :  ';
								strDiv+=teamData.data[i].captain;	
								strDiv+='</div>';
							}
							if(teamData.data[i].viceCaptain)
							{
								strDiv+='<div class="teamVCaptain">Vice Captain  :  ';
								strDiv+=teamData.data[i].viceCaptain;	
								strDiv+='</div>';
							}
							if(teamData.data[i].owner && (teamData.data[i].owner.trim() != "unstated" ))
							{
								strDiv+='<div class="teamOwner">Owner  :  ';
								strDiv+=teamData.data[i].owner;	
								strDiv+='</div>';
							}
						strDiv+='</div>';
					strDiv+='</div>';
				strDiv+='</div>';
			}
			$(".teamInfo").html(strDiv);
			setTimeout(function(){
				$('.f1_container').addClass("rotate");	
			}, 500);
			setTimeout(function() {
				$(".teamInfo").addClass('show');
			}, 1000);
		}
	}

}
