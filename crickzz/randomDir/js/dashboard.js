$(document).ready(function() {

    $(".clickEnabled").off("click").on("click", function() {
        _self = this;
        if (!$(this).hasClass("isExtended")) {
            var username = $(this).attr("value");
            var successCbk = function(data) {
                console.log(data);
                if (data && data.code == 200) {
                    $ele = $(_self).find(".contentContAppend");
                    $ele.append(data.data);
                    $(_self).addClass("isExtended");
                    $ele.slideDown("slow");
                } else {
                   $('body').append(errorPopup(data.success));
                }
            };
            var errorCbk = function(data) {
                console.log(data);
            };
            ajaxReq(window.location.origin + "/api/getUserData?username=" + username, "GET", successCbk, errorCbk);
        } else {
            $(this).removeClass("isExtended");
            $(this).slideDown("slow");
            $ele = $(this).find(".contentContAppend");
            $ele.html("");
            $ele.hide();
        }
    });
	$('.categoryLink').off().on('click',function() {
		var href = $(this).find('a').attr('href');
		window.location.href = href;
	});

});