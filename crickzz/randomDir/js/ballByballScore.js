/*$(document).ready(function(){
	//$('#cr_run_id, #cr_boundary_id, #cr_extrarun_id, #cr_extratype_id, #cr_outby_id, #cr_batsmanout_id').parent().hide();
	$('#cr_outby_id, #cr_batsmanout_id').parent().hide();
	$('input[name="outtype"]').click(function(e)
	{
		if(e.currentTarget.value != 'KO' && e.currentTarget.value != 'BO')//e.currentTarget.value != 'LB'
		{
			$('#cr_outby_id').parent().show();
		}
		else
		{
			$('#cr_outby_id').parent().hide();
		}
		if(e.currentTarget.value == 'RO')
		{
			$('#cr_run_id, #cr_boundary_id, #cr_extrarun_id, #cr_extratype_id, #cr_batsmanout_id').parent().show();
		}
		else 
		{
			$('#cr_run_id, #cr_boundary_id, #cr_extrarun_id, #cr_extratype_id, #cr_batsmanout_id').parent().hide();
		}
	});
	$('input[name="extratype"]').click(function(e)
	{
		if(e.currentTarget.value == 'NB')
		{
			$('#cr_run_id, #cr_boundary_id').parent().show(); 
			$('#cr_extrarun_id').parent().show(); 
		}
		else
		{
			$('#cr_extrarun_id').parent().show(); 
			$('#cr_boundary_id, #cr_run_id').parent().hide();
		}
	});
});*/
var requiredConfig = {};

function getMatchStatusPopup() {
    var matchid = getParameterByName('mId');
    var batId = getParameterByName('batId');
    var bowId = getParameterByName('bowId');
    requiredConfig.matchId = matchid;
    requiredConfig.batId = batId;
    requiredConfig.bowId = bowId;
    $('body').append(renderMatchStatusPopup());
    registerPopupEvents();
    getMatch();
    requiredConfig.batRender = true;
    requiredConfig.bowRender = true;
    requiredConfig.teamId = batId;
    getMatchPlayer();
}

function registerPopupEvents() {
    $(".matchStatusContentCont .matchStatusbtn").off("click").on("click",function(e) {
        var tossWonVal = $("#matchWonList option:selected").attr('id');
        var matchStatusVal = $('.matchStatusCont #matchStatus').val();
        var manOfMatchVal = $("#manOfMatchList option:selected").attr('playerid');
        if (tossWonVal && matchStatusVal && manOfMatchVal) {
            var config = {
                "matchId": parseInt(reqConf.matchId),
                "teamWon": tossWonVal.toString(),
                "matchStatus": matchStatusVal.toString(),
                "mom": parseInt(manOfMatchVal)
            };
            updateMatchScore(config);
        } else {
            alert("please enter all details");
        }
    });
    $(".parentContMatchStatus .cancelPopup").off("click").on("click",function() {
        $(".parentContMatchStatus").hide();
    });
}

function getMatch(config) {
    var config = {
        "matchid": requiredConfig.matchId
    };
    var successCbk = function(response) {
        if (response) {
            var matchInfo = (response.data && response.data.length ? response.data : "");
            renderMatchStatusDropDown(matchInfo, true);
        }
    };
    var errorCbk = function(error) {
        console.log(error);
    };
    ajaxReq(window.location.origin + "/api/getmatches", "POST", successCbk, errorCbk, null, config, "json");
}

function getMatchPlayer() {
    var config = {
        "matchId": requiredConfig.matchId,
        "teamId": requiredConfig.teamId
    };
    var successCbk = function(response) {
        if (response) {
            if (requiredConfig.batRender) {
                var playerList = ((response.data && response.data.players && response.data.players.length) ? response.data.players : "");
                renderMatchStatusDropDown(playerList, false);
                requiredConfig.batRender = false;
            } else if (requiredConfig.bowRender) {
                var playerList = ((response.data && response.data.players && response.data.players.length) ? response.data.players : "");
                renderMatchStatusDropDown(playerList, false);
                requiredConfig.bowRender = false;
            }
        }
    };
    var errorCbk = function(error) {
        console.log(error);
    };
    ajaxReq(window.location.origin + "/api/getmatchplayers", "POST", successCbk, errorCbk, null, config, "json");
}

function renderMatchStatusDropDown(resp, isToss) {
    var temp = "";
    if (resp) {
        if (isToss) {
            for (var i = 0; i < 1; i++) {
                temp = '<option name="' + resp[i].teamAName + '" id="' + resp[i].teamAId + '">' + resp[i].teamAName + '</option>' +
                    '<option name="' + resp[i].teamBName + '" id="' + resp[i].teamBId + '">' + resp[i].teamBName + '</option>';
            }
            $('.parentContMatchStatus #tossWonList').append(temp);
        } else {
            for (var i = 0; i < resp.length; i++) {
                temp += '<option name="' + resp[i].name + '" teamId="' + resp[i].teamId + '" playerId="' + resp[i].playerId + '">' + resp[i].name + '</option>';
            }
            $('.parentContMatchStatus #manOfMatchList').append(temp);
            requiredConfig.teamId = requiredConfig.bowId;
            if (requiredConfig.bowRender || requiredConfig.batRender) {
                getMatchPlayer();
            }
        }
    }
}

function cancelPopup() {
    $(".popupBatBow").remove();
    $('body').removeClass('popup').removeClass('showMask');
}
$(document).ready(function() {
    $('input[name="run"], input[name="extrarun"]').off("click").on("click",function(e) {
        if (e.currentTarget.value == 'OT') {
            $('input.' + e.currentTarget.name).show();
        } else {
            $('input.' + e.currentTarget.name).hide();
        }
    });


    $(document).off("click").on('click', '.popupBatBow .cancelPopup', function() {
        cancelPopup();
    });
});