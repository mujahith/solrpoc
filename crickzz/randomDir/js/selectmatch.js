$(document).ready(function()
{
	var seriesId = "";
	var config = {};
	var successCbk = function(response) {
		if(response)
		{
			console.log(response);
			if(response.data && response.data.length)
			{
				for(var i=0;i<response.data.length;i++)
				{
					$(".matchSeriesList").append('<option>'+response.data[i].seriesId+'</option>');
				}
				seriesId = $(".matchSeriesList").val();
				if(seriesId)
				{	config.seriesid = seriesId;
					config.initialSeriesId = seriesId;
					renderMatchSeries(config);
				}
			}
			else
			{
				$('body').append(errorPopup("No Series Available"));
			}
		}
	};
	var errorCbk = function(data) {
        console.log(data);
    };
	ajaxReq(window.location.origin + "/api/serieslist", "POST", successCbk, errorCbk, null);
	
	$('select').on('change', function() {
		seriesId = $(".matchSeriesList").val();
		if(seriesId)
		{
			config.seriesid = seriesId;
			renderMatchSeries(config);
		}
	});
	function renderMatchSeries(config)
	{
		var succCbk = function(response)
		{
			if(response)
			{
				console.log(response);
				if(response.data && response.data.length)
				{
					$(".matchDetailContainer").html("");

					for(var i=0; i<response.data.length; i++)
					{
						var respData = response.data[i];
						var dateTime = new Date(respData.dateTime);
						var date = dateTime.toDateString();
						var time = dateTime.toLocaleTimeString();

						var  matchDate = '<div class="matchListDate">'+ date +'</div>';
						var matchTime = '<div class="matchListTime">'+ time +'</div>';

						var str = "";
						str += '<div class="matchNo">';
						str += respData.matchNo ;
						str +='</div>';
						
						str += '<div class="matchAgaist">';
						str +=  '<div><span class="matchAgaistTeam">'+respData.teamAName +'</span> vs <span class="matchAgaistTeam">'+respData.teamBName+'</span></div>' ;
						str +='</div>'
						
						str += '<div class="matchVenue">';
						str += respData.venue ;
						str +='</div>';
						
						var matchDet = '<div class="matchListDet">'+ str +'</div>';
						
						var matchDetailsContent = "";
						
						if(!respData.tossWonBy)
						{
							matchDetailsContent = '<div class="matchDetailsCont matchUpdate" inngStatus="start">Start Match</div>';
						}
						else if(respData.tossWonBy && !respData.teamWon)
						{
							matchDetailsContent = '<div class="matchDetailsCont matchStart inngStatus="update"">Update Match</div>';
						}
						else if(respData.tossWonBy && respData.teamWon)
						{
							matchDetailsContent = '<div class="matchDetailsCont matchInfo inngStatus="info"">Match Info</div>';
						}

						var matchDetailsHtml = '<div class="matchIdBtn" id="updateDet" matchid ="'+respData.matchId+'">'+ matchDetailsContent +'</div>';

						$(".matchDetailContainer").append('<div class="match_'+i+'"><div class="mDateTime">'+ matchDate + matchTime +'</div>'+ matchDet +'<div class="matchSummaryDet">'+ matchDetailsHtml +'</div></div>');
						$(".matchListCont").removeClass('hide');
						registerEvents();
					}
				}
				else if(response.data)
				{
					$('body').append(errorPopup('Data is not available for seriesId: "'+config.seriesid+'" Please Select Other Than this'));
					$(".matchSeriesList").val(config.initialSeriesId);
				}
			}
		};
		var errorCbk = function(data)
		{
			console.log(data);
		};
		if(!$(".matchDetailContainer").children().hasClass(seriesId))
		{
			ajaxReq(window.location.origin + "/api/getMatches", "POST", succCbk, errorCbk, null, config);
		}
	}
	function registerEvents()
	{
		$(".matchDetailsCont").off("click").on("click",function(e)
		{
			var url = "";
			var matchid = $(this).parent().attr('matchid');
			var currStatus = $(this).attr('inngStatus');

			if(currStatus == "start")
			{
				url = '/startinnings?matchid=' + matchid ;				
			}
			else
			{
				url = "/matchinfo?mId=" + matchid;
			}
			window.location = url;
		});
	}
	
});