function initHomePage()
{
	renderSliderImages();
	renderFooterContent();
}

function renderSliderImages()
{
	var imgArr = ["/images/scl_1.jpg","/images/scl_2.jpg","/images/scl_3.jpg","/images/scl_4.jpg"];

	var sliderContent = ''+
		'<div u="slides" style="position: absolute; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">';

		for(var i=0; i<imgArr.length; i++)
		{
			sliderContent += '<div><img u=image src="'+ imgArr[i] +'" /></div>';
		}
            
        sliderContent += '</div>';

	$('#slider1_container').html(sliderContent);

	jssor_slider1_init();
}

function jssor_slider1_init(containerId) 
{
	//Reference https://www.jssor.com/development/slider-with-slideshow.html
	//Reference https://www.jssor.com/development/tool-slideshow-transition-viewer.html

	var _SlideshowTransitions = [
		//Fade
		{ $Duration: 1200, $Opacity: 2 }
	];
	
	var options = {
		$SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
		$DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $Cols is greater than 1, or parking position is not 0)
		$AutoPlay: 1,                                    //[Optional] Auto play or not, to enable slideshow, this option must be set to greater than 0. Default value is 0. 0: no auto play, 1: continuously, 2: stop at last slide, 4: stop on click, 8: stop on user navigation (by arrow/bullet/thumbnail/drag/arrow key navigation)
		$Idle: 1500,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
		$SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
			$Class: typeof($JssorSlideshowRunner$) != "undefined" ? $JssorSlideshowRunner$ : '',                 //[Required] Class to create instance of slideshow
			$Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
			$TransitionsOrder: 1                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
		}
	};
		
		var jssor_slider1 = typeof($JssorSlider$) != "undefined" ? new $JssorSlider$('slider1_container', options) : '';
	//responsive code begin
	//remove responsive code if you don't want the slider scales
	//while window resizing
	function ScaleSlider() {
		var parentWidth = $('#slider1_container').parent().width();
		if (parentWidth) {
			jssor_slider1.$ScaleWidth(parentWidth);
		}
		else
			window.setTimeout(ScaleSlider, 30);
	}
	//Scale slider after document ready
	ScaleSlider();
									
	//Scale slider while window load/resize/orientationchange.
	$(window).bind("load", ScaleSlider);
	$(window).bind("resize", ScaleSlider);
	$(window).bind("orientationchange", ScaleSlider);
	//responsive code end
}

function renderFooterContent()
{
	var footerContentHtml = ''+
		'<div class="footerMenuWrapper">'+
			'<div class="footerContText"><strong>Skava Champions League</strong>&nbsp;(<strong>SCL</strong>), is an intra-corporate cricket tournament which is been conducted every year for the employees working in Skava by OCS.</div>'+
			'<div class="footerContText">&nbsp;</div>'+
			'<div class="footerContText"><strong><strong>SCL </strong></strong>is started in the year 2012 and its mainly aimed at building good relationship among the employees in our company. Also, to build good team spirit and have lots of fun, exercise and entertainment.</div>'+
			'<div class="footerContText">&nbsp;</div>'+
			'<div class="footerContText">We have number of players, young, energetic and with lots of spirit, this series has been an good opportunity to bring people together, also a good pitch for the players to be a part of the SKAVA cricket team.</div>'+
			'<div class="footerContText">&nbsp;</div>'+
			'<div class="footerContText">We have started <strong>SCL</strong> with</div>'+
			'<ul>'+
			'<li>3 teams in 2012 season</li>'+
			'<li>4 teams in 2013, 2014 and 2015 seasons</li>'+
			'<li>5 teams in 2016 season</li>'+
			'<li>6 teams in 2017 season</li>'+
			'<li>10 teams in 2018 season</li>'+
			'</ul>'+
			'<div class="footerContText">Year by year we have grown, achieved great team spirit, found great matches, seen more professionalism, followed good standards. We still growing to great heights... As year grows we are getting more participation which helped us introduce a open platform for one theme which is "<strong>SCL</strong>". You have "CRICKET" in you then you have <strong><strong>SCL 2.0</strong></strong>&nbsp;for you in this year 2018.</div>'+
			'<div class="footerContText">&nbsp;</div>'+
			'<div class="footerContText">Please join us in this journey.</div>'+
		'</div>';

		$('.footerMenuContainer').html(footerContentHtml);
}


$(document).off("click").on('click', '.scoreDetails', function(e)
{
	var battingTeamId = $(this).find($('.teamAscoreDetails')).attr('id');
	var bowlingTeamId = $(this).find($('.teamBscoreDetails')).attr('id');
	var matchId = $(this).attr('id');
	location.href = '/livescore?mId='+matchId;
});

initHomePage();