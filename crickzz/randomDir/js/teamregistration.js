$(document).ready(function(){
	var successCbk = function(seriesList)
	{
		if(seriesList && seriesList.data)
		{
			for (i = 0; i < seriesList.data.length; i++) { 
				$('#cr_pSeriesVal_id').append('<option value='+seriesList.data[i].seriesId+'>'+seriesList.data[i].seriesName+'  ('+seriesList.data[i].seriesId+')</option>')
			}
		}
	};
	var errorCbk = function(error)
	{
		console.log(error);
	};
	ajaxReq(window.location.origin + "/api/serieslist", "POST", successCbk, errorCbk, null, null,"json");
	$("#teamregContainer #saveBtn").off("click").on("click",function(){
		var pteamname = $("#teamregContainer #cr_pTeamNameVal_id").val();
		var pnickname = $("#teamregContainer #cr_pNickNameVal_id").val();
		var pteamlogo = $("#teamregContainer #cr_pTeamLogoPath_id").val();
		var pseries = $("#teamregContainer #cr_pSeriesVal_id").val();
		if(pteamname && pnickname && pteamlogo && pseries)
		{
			var postData = {"teamname": pteamname, "nickname": pnickname, "logourl": pteamlogo, "seriesid": pseries}
			var successCbk = function(succResp)
			{
				if(succResp.code && succResp.code != 400)
				{
					$('body').append(errorPopup(succResp.message));
					$("#cancelBtn").trigger('click');
				}
				else
				{
					$('body').append(errorPopup("Error"));
				}
			};
			var errorCbk = function(errResp)
			{
				$('body').append(errorPopup(errResp.message));
			};
			ajaxReq(window.location.origin + "/api/registerteam", "POST", successCbk, errorCbk, null, postData);
		}
		else
		{
			$('body').append(errorPopup("Please Enter All Detail"));
		}
	});
	$("input:file").change(function (e){
		if(($(this).val()).indexOf('.jpg') != -1 || ($(this).val()).indexOf('.jpeg') != -1 || ($(this).val()).indexOf('.png') != -1)
		{
			/*var localpath = URL.createObjectURL(e.target.files[0]);
			console.log(localpath);
			var xhr = new XMLHttpRequest;
			xhr.responseType = 'blob';
			xhr.onload = function() {
			   var recoveredBlob = xhr.response;
			   var reader = new FileReader;
			   reader.onload = function() {
				 $("#cr_pTeamLogoPath_id").val(reader.result);
			   };
			   reader.readAsDataURL(recoveredBlob);
			};
			xhr.open('GET', localpath);
			xhr.send();*/
			
			//Logo Upload
			var image = $(this)[0].files[0];
		    var formdata = new FormData();
		    formdata.append('image', image);
		    $.ajax({
		        url: '/api/uploadteamimg',
		        data: formdata,
		        contentType: false,
		        processData: false,
		        type: 'POST',
		        'success':function(data){
		        	if(data.imagepath)
		        	{
		        		$("#cr_pTeamLogoPath_id").val(data.imagepath);
		        	}
		        }
		    });
			//Logo upload ends
		}
		else
		{
			$(this).val('');
			$("#cr_pTeamLogoPath_id").val('');
		}
	});
	$("#teamregContainer #cancelBtn").off("click").on("click", function(){
		$('#cr_FieldGroup_id input').val("");
		$('#cr_FieldGroup_id select').val("");
	});
});