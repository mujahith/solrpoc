$(function() {
    var socket = io();
    var $messageForm = $('#messageForm');
    var $message = $('#message');
    var $chat = $('#chats');
    var $usernameForm = $('#usernameForm');
    var $users = $('#users');
    var $username = $('#username');
    var $error = $('#error');
    var $send = $("#send");

    function scrollTolastmsg() {
        var elmnt = jQuery("#chats span:last-of-type")[0];
        (elmnt ? elmnt.scrollIntoView() : "");
    }

    $usernameForm.submit(function(e) {
        e.preventDefault();
        socket.emit('new user', $username.val(), function(data) {
            if (data) {
                $('#namesWrapper').hide();
                $('#mainWrapper').show();
            } else {
                $error.html('Username is taken');
            }
        });
    });

    socket.on('usernames', function(data) {
        var html = '';
        for (i = 0; i < data.length; i++) {
            html += '<div class="onlineIcon"></div>' + data[i] + '<br>';
        }
        $users.html(html);
        scrollTolastmsg();
    });

    $messageForm.submit(function(e) {
        e.preventDefault();
        if ($message.val() != "") {
            socket.emit('send message', $message.val());
            $message.val('');
        }
    });

    socket.on('new message', function(data) {
        if ($(".common").length) {
            var user = $(".common").text();
            if (user == data.user) {
                $chat.append('<span class="space">' + data.msg + '</span><br>');
            } else {
                $(".common").removeClass("common");
                $chat.append('<strong class="common">' + data.user + '</strong>: <span>' + data.msg + '</span><br>');
            }
        } else {
            $chat.append('<strong class="common">' + data.user + '</strong>: <span>' + data.msg + '</span><br>');
        }
        scrollTolastmsg();
    });

    $message.keyup(function() {
        if ($message.val() == "") {
            $send.removeClass("enable");
        } else {
            $send.addClass("enable");
        }
    });

    $send.click(function() {
        setTimeout(function() {
            $message.trigger("keyup");
        }, 100);

    });
});