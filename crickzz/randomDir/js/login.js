$(document).ready(function() {
    $(".submitButton").off("click").on("click", function() {
        var config = {
            "username": $("#username").val(),
            "password": $("#password").val()
        };
        var successCbk = function(data) {
            console.log(data);
            if (data && data.code == 302) {
                location = window.location;
                host = location.origin;
                url = host + "/" + data.redirectUrl;
                console.log(url)
                window.location.href = url;
            } else {
                $('body').append(errorPopup(data.success ? data.success : "Invalid user name or password"));
            }
        };
        var errorCbk = function(data) {
            console.log(data);
        };
        ajaxReq(window.location.origin + "/api/login", "POST", successCbk, errorCbk, null, config);
    });
});