$(document).ready(function(){
	$("#profileContainer #saveBtn").off("click").on("click", function(){
		if(($('#cr_pProfilePictureVal_id').val().toLowerCase()).indexOf('.jpg') != -1 || ($('#cr_pProfilePictureVal_id').val().toLowerCase()).indexOf('.jpeg') != -1 || ($('#cr_pProfilePictureVal_id').val().toLowerCase()).indexOf('.png') != -1)
		{
			/*var localpath = URL.createObjectURL(e.target.files[0]);
			console.log(localpath);
			var xhr = new XMLHttpRequest;
			xhr.responseType = 'blob';
			xhr.onload = function() {
			   var recoveredBlob = xhr.response;
			   var reader = new FileReader;
			   reader.onload = function() {
				 $("#cr_pProfilePath_id").val(reader.result);
			   };
			   reader.readAsDataURL(recoveredBlob);
			};
			xhr.open('GET', localpath);
			xhr.send();*/
			
			//Player Image Upload
			var image = $('#cr_pProfilePictureVal_id')[0].files[0];
		    var formdata = new FormData();
		    formdata.append('image', image);
		    $.ajax({
		        url: '/api/uploadplayerimg',
		        data: formdata,
		        contentType: false,
		        processData: false,
		        type: 'POST',
		        'success':function(data){
		        	if(data.imagepath)
		        	{
		        		$("#cr_pProfilePath_id").val(data.imagepath);
						var pname = $("#profileContainer #cr_pNameVal_id").val();
						var pprofilepicture = $("#cr_pProfilePath_id").val();
						var pdob = $("#profileContainer #cr_pDobVal_id").val();
						var prole = $("#profileContainer #cr_pRoleVal_id").val();
						var pbatsty = $("#profileContainer #cr_pBatStyVal_id").val();
						var pbowsty = $("#profileContainer #cr_pBowStyVal_id").val();
						var pemail = $("#profileContainer #cr_pEmailVal_id").val();
						var ptel = $("#profileContainer #cr_pTelVal_id").val();
						if(pname && pprofilepicture && pdob && prole && pbatsty && pbowsty && pemail && ptel)
						{
							var postData = {"name": pname, "playerImg": pprofilepicture, "dob": pdob, "role": prole, "batStyle": pbatsty, "bowlStyle": pbowsty, "email": pemail, "phone": ptel}
							var successCbk = function(succResp)
							{
								if(succResp.code && succResp.code != 400)
								{
									$('body').append(errorPopup(succResp.message));
									$("#profileContainer #cancelBtn").trigger('click');
								}
								else if(succResp && succResp.message)
								{
									$('body').append(errorPopup(succResp.message));
								}
							};
							var errorCbk = function(errResp)
							{
								$('body').append(errorPopup(errResp.message));
							};
							ajaxReq(window.location.origin + "/api/registerplayer", "POST", successCbk, errorCbk, null, postData);
						}
						else
						{
							$('body').append(errorPopup("Please Enter All Detail"));
						}
		        	}
		        }
		    });
			//Player Image upload ends
		}
		else
		{
			$('#cr_pProfilePictureVal_id').val('');
			$("#cr_pProfilePath_id").val('');
		}
	});
	$("#profileContainer #cancelBtn").off("click").on("click", function(){
		$('#cr_FieldGroup_id input').val("");
		$('#cr_FieldGroup_id select').val("");
	});
});