$(document).ready(function() {
    renderDropDown("select#overs", 50);
    renderDropDown("select#deliveries", 5);
    renderDropDown("select#maxOvers", 10);
    renderDropDown("select#numBowlForMaxOver", 11);
    var matchid = getParameterByName('matchid');
    //var changeDet = getParameterByName('chngDet');
    var matchConfig = {
        "matchid": matchid
    };
	var iosocket = io();
    var seriesId = "",
        tossInfoConf = "";
    var successCbk = function(response) {
        if (response) {
            console.log(response);
            if (response.data && response.data.length) {
                var i = 0;
                seriesId = response.data[i].seriesId;
                while (i < 1) {
                    $("input.teamAID").attr('value', response.data[i].teamAId);
                    $("input.teamBID").attr('value', response.data[i].teamBId);
                    $(".fieldGroup #teamAID").html(response.data[i].teamAName);
                    $(".fieldGroup #teamBID").html(response.data[i].teamBName);
                    /*if(changeDet == "true")
                    {
                    	$("input#tossWonTeamId[value="+response.data[i].tossWonBy+"]").attr('checked',true);
                    	var electRole = (response.data[i].electedToBat == 1 ? "Yes" : "No");
                    	$("input.electAccept[value="+electRole+"]").attr('checked',true);
                    	$("input.umpire1").val(response.data[i].umpire1);
                    	$("input.umpire2").val(response.data[i].umpire2);
                    	$("input.scorer").val(response.data[i].scorer);
                    	var overs = response.data[i].overs ? response.data[i].overs.toString().split('.') : "";
                    	$("select#overs").val(overs[0]);
                    	$("select#deliveries").val(overs[1]);
                    	$("select#maxOvers").val(response.data[i].maxOver);
                    	$("select#numBowlForMaxOver").val(response.data[i].noBowForMaxOvers);
                    }*/
                    i++;
                }
            }
        }
    };
    var errorCbk = function(data) {
        console.log(data);
    };
    ajaxReq(window.location.origin + "/api/getMatches", "POST", successCbk, errorCbk, null, matchConfig);

    function renderDropDown(ele, n) {
        var i = ((ele == "select#deliveries") ? 0 : 1);
        while (i <= n) {
            $(ele).append('<option name="option' + i + '" value = "' + i + '">' + i + '</option>');
            i++;
        }
    }
    var teamNo = "first";

    $("#parentContStartInnings .fieldGroup #submitBtn").off("click").on("click", function() {
        var tossWonTeamId = $("input[name=tossWonTeam]:checked").val();
        var electedBatTeam = $("input[name=electedBatTeam]:checked").val();
        var umpire1 = $("input.umpire1").val();
        var umpire2 = $("input.umpire2").val();
        var scorer = $("input.scorer").val();
        var over = $("select#overs option:selected").val();
        var deliveries = $("select#deliveries option:selected").val();
        var maxOvers = $("select#maxOvers option:selected").val();
        var numBowlForMaxOver = $("select#numBowlForMaxOver option:selected").val();

        if (tossWonTeamId && electedBatTeam && umpire1 && umpire2 && scorer && over && deliveries && maxOvers && numBowlForMaxOver) {
            tossInfoConf = {
                "matchId": (matchid ? matchid : getParameterByName('matchid')),
                "matchBalls": parseInt(over) * 6 + parseInt(deliveries),
                "maxBalls": parseInt(maxOvers) * 6,
                "noBowForMaxOvers": numBowlForMaxOver,
                "scorer": scorer,
                "umpire1": umpire1,
                "umpire2": umpire2,
                "tossWonBy": tossWonTeamId,
                "electedToBat": (electedBatTeam == "Yes" ? 1 : 0)
            };
            var teamConf = {
                "tId": $('input.teamAID').val(),
                "teamName": $("#teamAID").text()
            };
            $('#parentContStartInnings').addClass('hide');
            $('.parentContPlayingEleven').removeClass('hide');
            renderPlayingLevenPage(teamConf);
        } else {
            $('body').append(errorPopup("please fill all fields"));
        }
    });

    function renderPlayingLevenPage(teamConf) {
        var teamSquadConfig = {
            "teamId": teamConf.tId,
            "seriesId": seriesId
        };
        var successCbk = function(response) {
            if (response) {
                console.log(response);
                var assignPlayer = (response.data && response.data.assignedPlayers ? response.data.assignedPlayers : "");
                if (assignPlayer.length) {
                    $(".playingLevenInfoCont").html("");
					var teamName  = '<div class="teamNameDiv"><div class="teamName">'+teamConf.teamName+' (<span class="playingX1">0</span> playing XI & <span class = "sub">0</span> subs )</div></div>';
					$(".playingLevenInfoCont").append(teamName);
					for(var i=0;i<assignPlayer.length;i++)
					{
						var template = ''+
							'<div class="fieldGroup">'+	
								'<div class="fieldGroupLeftCont">'+	
									'<div class="playerImg">'+
										'<img class="playImg" src="'+ assignPlayer[i].playerImg +'"></img>'+
									'</div>'+
									'<div class="playName">'+ assignPlayer[i].name +'</div>'+
									'<div class="playRole"> ('+ assignPlayer[i].role +')</div>'+
								'</div>'+
								'<div class="fieldGroupRightCont">'+
									'<input type="checkbox" name="'+ assignPlayer[i].playerId +'" playerId="'+ assignPlayer[i].playerId +'" class="playerMainOrSub" id = "mainPlayer" value="Y" />'+
									'<div id="mainLeven" class="radioText">MainEleven</div>'+
									'<input type="checkbox" name="'+ assignPlayer[i].playerId +'" playerId="'+ assignPlayer[i].playerId +'" class="playerMainOrSub" id = "benchPlayer" value="N" />'+
									'<div id="substitute" class="radioText">Substitute</div>'+
								'</div>'+
							'</div>';
						$(".playingLevenInfoCont").append(template);
					}
					$(".playingLevenInfoCont").append('<button id="submitBtn" teamId="'+teamConf.tId+'" teamNo="'+teamNo+'">'+(teamNo == "first" ?'NEXT':'SUBMIT')+'</button>');
					registerEvents();
                } 
				else {
                   $('body').append(errorPopup("No assigned Players for teamId: " + teamConf.tId));
                }
            }
        };
        var errorCbk = function(data) {
            console.log(data);
        };
        ajaxReq(window.location.origin + "/api/getteamsquad?type=squad", "POST", successCbk, errorCbk, null, teamSquadConfig);
    }

    function registerEvents()
	{
		$(".parentContPlayingEleven #submitBtn").off("click").on("click",function(e){
			var players = [], pcount = 0 , scount = 0;
			$.each($(".playingLevenInfoCont .fieldGroup"), function(){
				var pId = $(this).find('input[class="playerMainOrSub"]:checked').attr('playerid');
				
				if(pId)
				{
					var pRole = $(this).find('input[class="playerMainOrSub"]:checked').val();
					
					if(pRole == "Y")
					{
						pcount++;
					}
					else
					{
						scount++;
					}
					players.push({"playerid":pId,"maineleven":pRole});					
				}
			});
			if(pcount != 11)
			{
				$('body').append(errorPopup("please select 11 main players only."));
			}
			else if(scount > 3)
			{
				$('body').append(errorPopup("please select max 3 substitude only."));
			}			
			else
			{
				var tId = $(e.currentTarget).attr('teamId');
				var teamOrder = $(e.currentTarget).attr('teamNo');
								
				var asgnMatchPlayConf = {
					"matchid": matchid,
					"teamid": tId,
					"players": players
				}
				var sccssCbk = function(response)
				{
					teamNo = "second";
					if(teamOrder == "first")
					{
						var teamConf = {
							"tId" : $('input.teamBID').val(),
							"teamName" : $("#teamBID").text()
						};
						renderPlayingLevenPage(teamConf);
					}
					else
					{
						var succCbk = function(response)
						{
							iosocket.emit('getlivematches');
							window.location = "/matchinfo?mId="+matchid;
						};
						var errorCbk = function(data)
						{
							console.log(data);
						};
						ajaxReq(window.location.origin + "/api/updatetossinfo", "POST", succCbk, errorCbk, null, tossInfoConf);
					}
				};
				var errorCbk = function(data){
			        console.log(data);
			    };
				ajaxReq(window.location.origin + "/api/assignmatchplayers", "POST", sccssCbk, errorCbk,null,asgnMatchPlayConf);
			}
		});
		
		$('.playerMainOrSub').off().on('change',function(event) {
			if ($(this).val() == "Y" )
			{
				val = $(this).attr("name");
				$(":checkbox[name='"+val+"'][id='benchPlayer']").prop("checked", false);
			}
			else {
				val = $(this).attr("name");
				$(":checkbox[name='"+val+"'][id='mainPlayer']").prop("checked", false);
			}
			$('.sub').text($(":checkbox[id='benchPlayer']:checked").length);
			$('.playingX1').text($(":checkbox[id='mainPlayer']:checked").length);
		});
	}
});