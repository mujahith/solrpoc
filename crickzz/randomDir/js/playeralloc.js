$(document).ready(function(){
	ss = new playeralloc ();
	ss.registerEvents();
});
function playeralloc(){
	
}
playeralloc.prototype.registerEvents = function(){
	$('.AvailablePlayer input[type=checkbox]').off().on('change',function() {
		var $self = this;
		var a=$($self).parent().html();
		$('.playerlistCount').text(parseInt($('.playerlistCount').text())+1);
		$('.selectedPlayer .teamAllocation').append('<div class="playerDetails">'+a+'</div>');
		$($self).parent().remove();
		ss.registerEvents();
	});
	$('.selectedPlayer input[type=checkbox]').off().on('change',function() { 
		var $self = this;
		var a=$($self).parent().html();
		$('.playerlistCount').text(parseInt($('.playerlistCount').text())-1);
		$('.AvailablePlayer .teamAllocation').append('<div class="playerDetails">'+a+'</div>');
		$($self).parent().remove();
		ss.registerEvents();
	});
	$('#saveBtn').off().click(function() {
		var cap = 1,vice = 1 ,owner = 1 ,error = 0,count=0,assignTeam = 0;
		var capId , ViceCapId , ownerId ;
		var maxSquard = productListData.data.maxPlayers;
		var players =[];
		$('.selectedPlayer input[type=checkbox]').each(function () {
			//alert($(this).val());
			var playerId = $(this).val();
			var playerRole = $('#'+playerId+' option:selected').val();
			//alert(b);
			if (playerRole == "captain")
			{
				if(cap==1)
				{
					cap = 0;
					capId = playerId;
					playerRole = "C";
				}
				else {
					error = 1;
				}
			}
			else if (playerRole == "vice-captain")
			{
				if(vice==1)
				{
					vice = 0;
					ViceCapId = playerId;
					playerRole = "VC";
				}
				else {
					error = 1;
				}
			}
			else if (playerRole == "owner")
			{
				owner = 0;
				ownerId = playerId;
				playerRole = "O";
			}
			else {
				playerRole = "P";
			}
			var playerObj = {};
			playerObj.playerId = playerId;
			playerObj.role = playerRole;
			players.push(playerObj);
			count++;
		});
		if(maxSquard < count)
		{
			$('body').append(errorPopup("please select only "+maxSquard+" playes"));
		}
		else if (cap == 0 && vice == 0 && error == 0)
		{
			if(count>=9)
			{
				assignTeam = 1;
			}
			else {
				assignTeam = 0;
			}
			var successCbk = function(data) {
				if(data.code != 400)
				{
					$('body').append(errorPopup("Successfully Updated"))
				}
				else
				{
					$('body').append(errorPopup("Duplicate Entry"));
				}
			}
			var errorCbk = function(data) {
				$('body').append(errorPopup("Site is temporary down please do some other time"));
			}
			if(assignTeam == 1)
			{
				console.log(players);
				var config = {};
				var roles = {};
				roles.captain = capId;
				roles.viceCaptain = ViceCapId ;
				if(owner == 0)
				{
					roles.owner = ownerId;
				}
				else {
					roles.owner = 1;
				}
				config.teamId = getCookie("teamId");
				config.seriesId = getCookie("seriesId");
				config.players = players;
				config.roles = roles;
				ajaxReq(window.location.origin + "/api/assignplayers", "POST", successCbk, errorCbk, null, config); 
			}
			else
			{
				$('body').append(errorPopup("please select player role 1 captain & 1 vice-captain and 7 remaining playes"));
			}
			
				
		}
		else
		{
			$('body').append(errorPopup("please select player role 1 captain & 1 vice-captain & 1 owner and remaining playes"));
		}
	});
	$('#cancelBtn').off().on('click',function() {
		location.reload();
	});
}
