$(document).ready(function() {
    var successCbk = function(response) {
        if (response) {
            console.log(response);
            if (response.data && response.data.length) {
                for (var i = 0; i < response.data.length; i++) {
                    $(".matchSeriesList").append('<option>' + response.data[i].seriesId + '</option>')
                }
            }
        }
        seriesSelect();
    };
    var errorCbk = function(data) {
        console.log(data);
    };
    ajaxReq(window.location.origin + "/api/serieslist", "POST", successCbk, errorCbk, null);

    var successResponse = function(response) {
        if (response) {
            console.log(response);
            if (response.data && response.data.length) {
                for (var i = 0; i < response.data.length; i++) {
                    var dateTime = new Date(response.data[i].dateTime);
                    var date = dateTime.toDateString();
                    var time = dateTime.toLocaleTimeString();
                    var arr = time.split(':')
                    var Meridiem = arr[2].split(" ");
                    var str = "";
                    str += '<div class="ScheduleResult">';
                    str += '<div class="dateTime">';
                    str += '<div class="date">';
                    str += date;
                    str += '</div>';
                    str += '<div class="time">';
                    str += arr[0].trim() + ":" + arr[1].trim() + " " + Meridiem[1];
                    str += '</div>';
                    str += '</div>';
                    str += '<div class="matchDetails">';
                    str += '<div class = "matchName">';
                    str += '<a href="/teamSquad?teamId=' + response.data[i].teamAId + '&seriesId=' + response.data[i].seriesId + '">' + response.data[i].teamAName + '</a> Vs <a href="/teamSquad?teamId=' + response.data[i].teamBId + '&seriesId=' + response.data[i].seriesId + '">' + response.data[i].teamBName + '</a>';
                    str += '</div>';
                    str += '<div class = "venue">';
                    str += response.data[i].venue + ' Venue (' + response.data[i].matchNo + ')';
                    str += '</div>';
                    str += '</div>';
                    str += '<div class="result">';
                    if (response.data[i].tossWonBy != null && response.data[i].teamWon == null) {
                        str += '<a class="liveMatch" href="/livescore?mId=' + response.data[i].matchId + '">Live Match</a>';
                    } else if (response.data[i].matchStatus == null) {
                        str += 'Not yet Played';
                    } else {
                        str += '<a href="/scorecard?mId=' + response.data[i].matchId + '">' + response.data[i].matchStatus + '</a>'; // TODO
                    }
                    str += '</div>';
                    str += '</div>';
                    $('.scheduleHeader').css("display", "block");
                    $('.ScheduleCont').css("color", "black");
                    $(".ScheduleCont").append(str);
                }
            } else if (response.data) {
                $(".ScheduleCont").html("Data is not available in this seriesId , Please select other than this.");
                $('.scheduleHeader').css("display", "none");
                $('.ScheduleCont').css("color", "red");
            }
        }
    };

    var seriesSelect = function() {
        var config = {};
        config.seriesid = $('.matchSeriesList').val();
        $(".ScheduleCont").html("");
        ajaxReq(window.location.origin + "/api/getMatches", "POST", successResponse, errorCbk, "", config);
    }

    $(".matchSeriesList").off().on('change', function() {
        seriesSelect();
    });

});