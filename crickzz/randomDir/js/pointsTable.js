$(document).ready(function()
{
	var successCbk = function(response) {
		if(response)
		{
			console.log(response);
			if(response.data && response.data.length)
			{
				for(var i=0;i<response.data.length;i++)
				{
					$(".matchSeriesList").append('<option>'+response.data[i].seriesId+'</option>')
				}
			}
		}
		seriesSelect();
	};
	var errorCbk = function(data) {
        console.log(data);
    };
	ajaxReq(window.location.origin + "/api/serieslist", "POST", successCbk, errorCbk, null);
	
	var successResponse = function(response) {
		var str = "";
		for(i=0 ; i < response.data.length ; i++ ) {
			str+='<div class = "pointsDetails">';
				str+='<div class="team">'+response.data[i].teamName+'</div>';
				str+='<div class="mat">'+response.data[i].matches+'</div>';
				str+='<div class="won">'+response.data[i].won+'</div>';
				str+='<div class="lost">'+response.data[i].lost+'</div>';
				str+='<div class="tied">'+response.data[i].draw+'</div>';
				str+='<div class="nr">0</div>';
				str+='<div class="pts">'+response.data[i].points+'</div>';
				str+='<div class="nrr">'+response.data[i].runRate.toFixed(3)+'</div>';
			str+='</div>';
		}
		$('.pointsContent').html(str);
		resize();
	};
	$(".matchSeriesList").off().on('change',function() {
		seriesSelect();
	});
	var seriesSelect = function() {
		var config = {};
		$('.pointsContent').html("");
		config.seriesid = $('.matchSeriesList').val();
		ajaxReq(window.location.origin + "/api/getteamlist", "POST", successResponse, errorCbk,"",config );
	}
	var resize = function() {
		var w = window.innerWidth;
		if(w <= 415)
		{
			$('.pointsHeader .mat').text('P');
			$('.pointsHeader .won').text('W');
			$('.pointsHeader .lost').text('L');
			$('.team').css('width','53%');
			$('.tied').css('display','none');
			$('.nr').css('display','none');
		}
		else {
			$('.pointsHeader .mat').text('Mat');
			$('.pointsHeader .won').text('Won');
			$('.pointsHeader .lost').text('Lost');
			$('.tied').css('display','block');
			$('.team').css('width','38%');
			$('.nr').css('display','block');
		}
	};
	resize();
	window.addEventListener("resize", function() {
		resize();
	}, false);
});