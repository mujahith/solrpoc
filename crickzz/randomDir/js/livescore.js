var matchId = getParameterByName('mId');
var currentResult = {};
currentResult.battingTeamId = '';
currentResult.bowlingTeamId = '';
currentResult.score = 0;
currentResult.wicket = 0;
currentResult.balls = 0;
currentResult.teamAName = '';
currentResult.teamBName = '';
currentResult.matchNo = '';
currentResult.tossWonBy = '';
currentResult.electedToBat = 0;
currentResult.currentStatus = '';
$(function() {
    var iosocket = io();
    iosocket.on('updatescore', function(result) {
        var result = (result && result.data) ? result.data : result;
        stateUpdate(result);
    });
    var obj = {
        "matchid": matchId
    };
    iosocket.emit('initialload', obj);
});

function stateUpdate(result) {
    debugger;
    this.setState({
        Batsman: result.batsMen,
        Bowler: result.bowler,
        MatchInfo: result.matchInfo
    })
}