$(document).ready(function(){
	var matchType1 = ["League","Knock Out","Quarter Final","Semi Final" ];
	var matchType2 = ["3rd Place","Final"];
	var matchNumber = 0;
	var successCbk = function(seriesList)
	{
		for (i = 0; i < matchType1.length; i++) { 
			$('#cr_pMatchTypeVal_id').append('<option value='+matchType1[i].replace(/ /g,'').toLowerCase()+'>'+matchType1[i]+'</option>')
		}
		for (i = 0; i < matchType2.length; i++) { 
			$('#cr_pMatchTypeVal_id').append('<option value='+matchType2[i].replace(/ /g,'').toLowerCase()+'>'+matchType2[i]+'</option>')
		}
		if(seriesList && seriesList.data)
		{
			for (i = 0; i < seriesList.data.length; i++) { 
				$('#cr_pSeriesVal_id').append('<option value='+seriesList.data[i].seriesId+'>'+seriesList.data[i].seriesName+'  ('+seriesList.data[i].seriesId+')</option>')
			}
		}
	};
	var errorCbk = function(error)
	{
		console.log(error);
	};
	ajaxReq(window.location.origin + "/api/serieslist", "POST", successCbk, errorCbk, null, null,"json");
	$('select#cr_pSeriesVal_id').on('change', function() {
		var successCbk = function(teamList)
		{
			if(teamList && teamList.data)
			{
				$('#cr_pTeamAVal_id').html('<option value="">&nbsp;</option>');
				$('#cr_pTeamBVal_id').html('<option value="">&nbsp;</option>');
				for (i = 0; i < teamList.data.length; i++) { 
					$('#cr_pTeamAVal_id').append('<option value='+teamList.data[i].teamId+'>'+teamList.data[i].teamName+'</option>');
					$('#cr_pTeamBVal_id').append('<option value='+teamList.data[i].teamId+'>'+teamList.data[i].teamName+'</option>');
				}
			}
		};
		var errorCbk = function(error)
		{
			console.log(error);
		};
		var postData = {"seriesid": $(this).val()};
		ajaxReq(window.location.origin + "/api/getteamlist", "POST", successCbk, errorCbk, null, postData,"json");
	});
	$('select#cr_pTeamAVal_id, select#cr_pTeamBVal_id').on('change', function() {
		if($('#cr_pTeamAVal_id').val() != '' && $('#cr_pTeamBVal_id').val() != '' && $('#cr_pTeamAVal_id').val() == $('#cr_pTeamBVal_id').val())
		{
			$('body').append(errorPopup("Team A should be differ from Team B"));
			$(this).val('');
		}
	});
	$('#cr_pMatchTypeVal_id').on('change', function() {
		matchNumber = 0;
		if($('#cr_pMatchTypeVal_id').val().trim() !="")	{
			for (i = 0; i < matchType1.length; i++) { 
				if( matchType1[i].replace(/ /g,'').toLowerCase() == $('#cr_pMatchTypeVal_id').val().trim() ) {
					$('.matchNumber').css('pointer-events','auto');
					matchNumber = 1;
				}
			}
			if(matchNumber == 0) {
				$('.matchNumber').css('pointer-events','none');
				$('#cr_pMatchnoVal_id').val("");
			}
		}
	});
	$("#profileContainer #saveBtn").off("click").on("click", function(){
		var pMatchNo = $("#profileContainer #cr_pMatchnoVal_id").val().trim();
		if (matchNumber == 0){
			pMatchNo = 1;
		}
		var pDate = $("#profileContainer #cr_pDateVal_id").val().trim();
		var pTime = $("#profileContainer #cr_pTimeVal_id").val().trim();
		var pVenue = $("#profileContainer #cr_pVenueVal_id").val().trim();
		var pMatchType = $("#cr_pMatchTypeVal_id").val().trim();
		var pSeries = $("#profileContainer #cr_pSeriesVal_id").val().trim();
		var pTeamA = $("#profileContainer #cr_pTeamAVal_id").val().trim();
		var pTeamB = $("#profileContainer #cr_pTeamBVal_id").val().trim();
		var pdateTime = pDate + " " + pTime + ":00";
		if(pMatchNo && pdateTime && pVenue && pMatchType && pSeries && pTeamA && pTeamB)
		{
			var postData = {"matchno": pMatchNo, "datetime": pdateTime, "venue": pVenue, "teamaid": pTeamA, "teambid": pTeamB, "seriesid": pSeries, "matchtype": pMatchType}
			var successCbk = function(succResp)
			{
				if(succResp.code && succResp.code != 400)
				{
					$('body').append(errorPopup(succResp.message));
					$('#cr_FieldGroup_id input').val("");
					$('#cr_FieldGroup_id select').not('#cr_pSeriesVal_id').val("");
					$('.matchNumber').css('pointer-events','none');
					$('#cr_pMatchnoVal_id').val("");
				}
				else
				{
					$('body').append(errorPopup("Error"));
				}
			};
			var errorCbk = function(errResp)
			{
				$('body').append(errorPopup(errResp.message));
			};
			ajaxReq(window.location.origin + "/api/scheduleMatch", "POST", successCbk, errorCbk, null, postData,"json");
		}
		else
		{
			$('body').append(errorPopup("Please Enter All Detail"));
		}
	});
	$("#profileContainer #cancelBtn").off("click").on("click", function(){
		$('#cr_FieldGroup_id input').val("");
		$('#cr_FieldGroup_id select').val("");
		$('.matchNumber').css('pointer-events','none');
		$('#cr_pMatchnoVal_id').val("");
	});
});