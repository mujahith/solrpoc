var ajaxReq = function(url, type, successCbk, errorCbk, contentType, postparam, datatype, xhrfields, crossDomain, headers) {

    var sCbk = function(data) {
        successCbk && successCbk(data);
    };
    var eCbk = function(err, textStatus) {
        var cbk = function() {
            errorCbk && errorCbk(err);
        };
        if (textStatus == "timeout") {
            _self.customPopup("skllb_ajaxTimeout", "Network timeout - Please try again", cbk);
        } else {
            cbk();
        }
    };
    var ajaxConfig = {
        "url": url,
        "type": (type ? type : "GET"),
        "success": sCbk,
        "error": eCbk,
    };

    if (contentType) {
        ajaxConfig.contentType = contentType;
    }
    if (postparam) {
        ajaxConfig.data = postparam;
    }
    if (datatype) {
        ajaxConfig.dataType = datatype;
    }
    if (headers) {
        ajaxConfig.headers = headers;
    }
    if (xhrfields) {
        $.extend(ajaxConfig.xhrFields, xhrfields);
    }
    if (typeof(crossDomain) != "undefined") {
        ajaxConfig.crossDomain = crossDomain;
    }
    $.ajax(ajaxConfig);
};
var getCookie = function(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

$(document).ready(function() {

});

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function renderBattingPopup() {
    var template = '<div class="parentContBatterInfo popupBatBow">' +
        '<div class="batterInfoCont">' +
        '<div class="headerContainer">' +
        '<div class="popupMatchInfoTit">' +
        '<h1>Batter Selection</h1><div class="cancelPopup">X</div>' +
        '</div>' +
        '</div>' +
        '<div class="batterContentCont">' +
        '<div class="fieldGroup">' +
        '<div class="batFirst">' +
        '<div class="batFirstTxt labelText">Select BatsMen:</div>' +
        '<select id="firstBatDropDown">' +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="fieldGroup">' +
        '<button class="updtBattingStatus">UPDATE</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return template;
}

function renderBowlingPopup() {
    var template = '<div class="parentContBowlerInfo popupBatBow">' +
        '<div class="bowlerInfoCont">' +
        '<div class="headerContainer">' +
        '<div class="popupMatchInfoTit">' +
        '<h1>Bowler Selection</h1><div class="cancelPopup">X</div>' +
        '</div>' +
        '</div>' +
        '<div class="bowlerContentCont">' +
        '<div class="fieldGroup">' +
        '<div class="bowlerCont">' +
        '<div class="bowlerText labelText">Bowler:</div>' +
        '<select id="bowlerList">' +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="fieldGroup">' +
        '<button class="updtBowlingStatus">UPDATE</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return template;
}

function renderMatchStatusPopup() {
    var template = '<div class="parentContMatchStatus popupMatchInfo">' +
        '<div class="matchStatusCont">' +
        '<div class="matchStatusHeader">' +
        '<div class="matchStatusTit">' +
        '<h1>Match Status</h1><div class="cancelPopup">X</div>' +
        '</div>' +
        '</div>' +
        '<div class="matchStatusContentCont">' +
        '<div class="fieldGroup">' +
        '<div class="matchWonByCont">' +
        '<div class="matchWonByText labelText">Match Won By:</div>' +
        '<select id="matchWonList">' +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="fieldGroup">' +
        '<div class="matchStatusCont">' +
        '<div class="matchStatusText labelText">Match Status:</div>' +
        '<textarea id="matchStatus" rows="4" cols="50"/></textarea>' +
        '</div>' +
        '</div>' +
        '<div class="fieldGroup">' +
        '<div class="manOfMatchCont">' +
        '<div class="manOfMatchText labelText">Man Of Match:</div>' +
        '<select id="manOfMatchList">' +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="fieldGroup">' +
        '<button class="matchStatusbtn">SUBMIT</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return template;
}

function errorPopup(msg) {
    var template = '<div class="errorPopupCont">' +
        '<div class="errorPopupHeader">' +
        '<div class = "cancelErrDiv"><div class="cancelErrPopup">X</div></div>' +
        '</div>' +
        '<div class="errMsg"><h1>' + msg + '</h1></div>' +
        '<div class="submitBtn"><div class="submitError">Ok</div></div>' +
        '</div>';
    setTimeout(function() {
        $('body').addClass("showMask");
        registerPopupEvts();
    }, 50);
    return template;
}

function registerPopupEvts() {
    $(".cancelErrPopup,.submitError").off("click").on("click", function() {
        $(".errorPopupCont").remove();
        $('body').removeClass("showMask");
    });
}