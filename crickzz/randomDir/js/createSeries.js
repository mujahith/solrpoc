$(document).ready(function(){
	var year = (new Date()).getFullYear();
	var str = "";
	for(i=0; i < 5 ; i++,year++)
	{
		str+='<option value="'+year+'">'+year+'</option>';
	}
	$("#seriesYear").append(str);
	ss = new createSeries ();
	ss.registerEvents();
});
function createSeries(){
	
}
createSeries.prototype.registerEvents = function(){
	$('#saveBtn').off().on('click',function() {
		var seriesName = $('#seriesName').val().trim();
		var seriesNickName = $('#seriesNickName').val().trim();
		var seriesYear = $('#seriesYear').val();
		var squardLimit = $('#seriesMaxPlayer').val();
		if ( seriesName != "" && seriesNickName != "" && seriesYear != "" && squardLimit !="")
		{
			var successCbk = function(data) {
				if(data.code != 400)
				{
					$('body').append(errorPopup("Successfully Updated"));
					$("#cancelBtn").trigger('click');
				}
				else
				{
					$('body').append(errorPopup("This series is already registered"));
				}
				
			}
			var errorCbk = function(data) {
				$('body').append(errorPopup("Site is temporary down please do some other time"));
			}
			var config = {};
			config.seriesname = seriesName;
			config.seriesnickname = seriesNickName;
			config.year = seriesYear;
			config.maxPlayers = squardLimit;
			ajaxReq(window.location.origin + "/api/createseries", "POST", successCbk, errorCbk, null, config); 
		}
		else 
		{
			$('body').append(errorPopup("Please Fill All Details"));
		}
	});
	$('#cancelBtn').off().on('click',function() {
		$('#cr_FieldGroup_id input').val("");
		$('#cr_FieldGroup_id select').val((new Date()).getFullYear()        );
	});
}