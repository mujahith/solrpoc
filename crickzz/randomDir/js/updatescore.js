$(document).ready(function(){
	$("#updatescoreContainer #saveBtn").off("click").on("click",function(){
		var matchId = getParameterByName('matchid');
		var teamAscore = $("#updatescoreContainer #cr_TeamAScore_id").val();
		var teamAovers = $("#updatescoreContainer #cr_TeamAOvers_id").val();
		var teamBscore = $("#updatescoreContainer #cr_TeamBScore_id").val();
		var teamBovers = $("#updatescoreContainer #cr_TeamBOvers_id").val();
		var teamWon = $("#updatescoreContainer #cr_TeamWonVal_id").val();
		var matchStatus = $("#updatescoreContainer #cr_MatchStatus_id").val();
		var manOftheMatch = $("#updatescoreContainer #cr_Mom_id").val();
		if(matchId && teamAscore && teamAovers && teamBscore && teamBovers && teamWon && matchStatus && manOftheMatch)
		{
			var postData = {"matchId": matchId, "teamAScore": teamAscore, "teamAOvers": teamAovers, "teamBScore": teamBscore, "teamBOvers": teamBovers, "teamWon": teamWon, "matchStatus": matchStatus, "mom": manOftheMatch}
			var successCbk = function(succResp)
			{
				if(succResp.code && succResp.code == 200)
				{
					$('body').append(errorPopup(succResp.success));
				}
				else
				{
					$('body').append(errorPopup(succResp.code));
				}
			};
			var errorCbk = function(errResp)
			{
				$('body').append(errorPopup(errResp.message));
			};
			ajaxReq(window.location.origin + "/api/updatematchscore", "POST", successCbk, errorCbk, null, postData);
		}
		else
		{
			$('body').append(errorPopup("Please Enter All Detail"));
		}
	});
	$("#updatescoreContainer #cancelBtn").off("click").on("click", function(){
		location.reload();
	});
});