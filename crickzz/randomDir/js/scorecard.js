$(document).ready(function()
{
	var config = {
		"matchid": getParameterByName("mId")
	};
	getMatchScoreBoard(config);
});

function getMatchScoreBoard(config)
{
	var successCbk = function(response)
	{
		if(response && response.data)
		{
			getMatchDetailConfig(response);
			//renderInningsCont(response);
		}
	};
	var errorCbk = function(data)
	{
		console.log(data);
	};
	ajaxReq(window.location.origin + "/api/getMatchScoreboard", "POST", successCbk, errorCbk, null, config);
}
function getMatchDetailConfig(response)
{
	var matchInfo = ((response.data.matchInfo && response.data.matchInfo && response.data.matchInfo[0] ) ? response.data.matchInfo[0] : "");
	
	if(matchInfo)
	{
		$('.scoreCardTit').html("Scoreboard ( "+ matchInfo.teamANickName +" vs "+ matchInfo.teamBNickName +" )");
		
		var matchInfoAllDetails = {};		
		var firstBattingTeam = (matchInfo.tossWonBy == matchInfo.teamAId ? (matchInfo.electedToBat == 1 ? "teamA" : "teamB") : (matchInfo.electedToBat == 1 ? "teamB" : "teamA"));
		var secondBattingTeam = (firstBattingTeam == "teamA" ? "teamB" : "teamA");		
		
		var teamDetails =
		{
			"teamA" :
			{
				"id" : matchInfo.teamAId,
				"name" : matchInfo.teamAName,
				"score" : matchInfo.teamAScore,
				"wicket" : matchInfo.teamAWicket,
				"balls" : matchInfo.teamABalls,
				"inngStatus" : matchInfo.teamAInngStatus,
			},
			"teamB" :
			{
				"id" : matchInfo.teamBId,
				"name" : matchInfo.teamBName,
				"score" : matchInfo.teamBScore,
				"wicket" : matchInfo.teamBWicket,
				"balls" : matchInfo.teamBBalls,
				"inngStatus" : matchInfo.teamBInngStatus,
			}
		};
		
		matchInfoAllDetails["battingTeam"] = teamDetails[firstBattingTeam]["id"];
		matchInfoAllDetails["bowlingTeam"] = teamDetails[secondBattingTeam]["id"];	
		matchInfoAllDetails["battingTeamName"] = teamDetails[firstBattingTeam]["name"];
		matchInfoAllDetails["bowlingTeamName"] = teamDetails[secondBattingTeam]["name"];
		matchInfoAllDetails["firstBatScore"] = teamDetails[firstBattingTeam]["score"];
		matchInfoAllDetails["secondBatScore"] = teamDetails[secondBattingTeam]["score"];		
		matchInfoAllDetails["firstBatWicket"] = teamDetails[firstBattingTeam]["wicket"];
		matchInfoAllDetails["secondBatWicket"] = teamDetails[secondBattingTeam]["wicket"];
		matchInfoAllDetails["firstBatBalls"] = teamDetails[firstBattingTeam]["balls"];
		matchInfoAllDetails["secondBatBalls"] = teamDetails[secondBattingTeam]["balls"];
		matchInfoAllDetails["firstBatInngs"] = teamDetails[firstBattingTeam]["inngStatus"];
		matchInfoAllDetails["secondBatInngs"] = teamDetails[secondBattingTeam]["inngStatus"];
		matchInfoAllDetails["firstBatWkts"] = teamDetails[firstBattingTeam]["wicket"];
		matchInfoAllDetails["secondBatWkts"] = teamDetails[secondBattingTeam]["wicket"];
	}
	
	var battingResponse = response.data[firstBattingTeam]["players"];
	var bowlingResponse = response.data[secondBattingTeam]["players"];
	
	renderBattingInnings(battingResponse, matchInfoAllDetails.battingTeamName, matchInfoAllDetails.firstBatScore,matchInfoAllDetails.firstBatInngs,matchInfoAllDetails.firstBatBalls,matchInfoAllDetails.firstBatWkts);
	renderBowlingInnings(bowlingResponse, matchInfoAllDetails.bowlingTeamName);
	
	renderBattingInnings(bowlingResponse, matchInfoAllDetails.bowlingTeamName, matchInfoAllDetails.secondBatScore,matchInfoAllDetails.secondBatInngs,matchInfoAllDetails.secondBatBalls,matchInfoAllDetails.secondBatWkts);
	renderBowlingInnings(battingResponse,matchInfoAllDetails.battingTeamName);
	
	renderMatchInfoDetails(matchInfo,battingResponse,matchInfoAllDetails.battingTeamName,bowlingResponse,matchInfoAllDetails.bowlingTeamName);
}

function renderBattingInnings(battingResponse,battingTeamName,firstBatScore,inngStatus,balls,wkts) {
	
	battingResponse = battingResponse.sort(function(a, b){
		return b.batOrder - a.batOrder;
	});
	if (battingResponse[0].batOrder != 0 ) {
		battingResponse = battingResponse.sort(function(a, b){
			return a.batOrder - b.batOrder;
		});
		var outType = ["RO", "CO", "BO", "SO", "KO"];
		var battingStatusBar = ['Batsman','R','B','4s','6s','SR'];
		var	bowlingStatusBar =['O','M','R','W','NB','WD','ECO'];
		var battingTeamNameHtml = '<div class="innings">'+battingTeamName+' Innings</div>';
		$('.scoreCardInfoCont').append(battingTeamNameHtml);
		var battingHtml = '<div class = "battingStatusBar" >'+
								'<div class="batsman">'+battingStatusBar[0]+'</div>';
								 for(i=1;i<battingStatusBar.length;i++) {
									 battingHtml+='<div class="batstatusBar">'+battingStatusBar[i]+'</div>';
								 }
						   battingHtml+='</div>';
		$('.scoreCardInfoCont').append(battingHtml);
		var battingOrderHtml = '';
		for (i=0;i<battingResponse.length;i++) {
			if(battingResponse[i].batOrder != 0 && battingResponse[i].elevenType == 1) {
				var outstatus = "";
				if(battingResponse[i].batStatus == "out") {
					if(battingResponse[i].outType == outType[0]) {
						outstatus = 'run out (<span class="batsName">'+battingResponse[i].outByName+'</span>)';
					}
					else if(battingResponse[i].outType == outType[1]) {
						outstatus = 'c <span class="batsName">'+battingResponse[i].outByName+'</span> b <span class="batsName">'+battingResponse[i].outByBowlerName+'</span>';
					}
					else if(battingResponse[i].outType == outType[2]) {
						outstatus = ' b <span class="batsName">'+battingResponse[i].outByBowlerName+'</span>';
					}
					else if(battingResponse[i].outType == outType[3]) {
						outstatus = 'st <span class="batsName">'+battingResponse[i].outByName+'</span> b <span class="batsName">'+battingResponse[i].outByBowlerName+'</span>';
					}
					else if(battingResponse[i].outType == outType[4]) {
						outstatus = ' hit wkt <span class="batsName">'+battingResponse[i].outByBowlerName+'</span>';
					}
				}
				else {
					outstatus = "not out";
				}
				battingOrderHtml+='<div class = "battingStatus" >'+
										'<div class="batsman"><div class="batsmanName"><a href="/profiles?playerId='+battingResponse[i].playerId+'">'+battingResponse[i].name+'</a></div><div class="batsmanstatus">'+outstatus+'</div></div>'+
										'<div class="batstatusBar">'+battingResponse[i].runScored+'</div>'+
										'<div class="batstatusBar">'+battingResponse[i].ballsFaced+'</div>'+
										'<div class="batstatusBar">'+battingResponse[i].fours+'</div>'+
										'<div class="batstatusBar">'+battingResponse[i].sixes+'</div>'+
										'<div class="batstatusBar">';
											if(battingResponse[i].runScored == 0){
												battingOrderHtml+=0;
											}else 
											{
												battingOrderHtml+=((battingResponse[i].runScored/battingResponse[i].ballsFaced)*100).toFixed(2);
											}
											battingOrderHtml+='</div>'+
									'</div>';
			}	
		}
		battingOrderHtml+='<div class = "battingStatus" ><div class="total">Total</div><div class="score">'+firstBatScore+' ( '+wkts+' Wkts, '+Math.floor(balls/6)+'.'+(balls%6)+' Ov)</div></div>';
		
		battingOrderHtml+='<div class="didNotBat"><div class="YetToBat">';
			if(inngStatus == 0) {
				battingOrderHtml+='Yet to Bat:</div>';
			}
			else {
				battingOrderHtml+='Did Not Bat:</div>';
			}
			battingOrderHtml+='<div class="remainingbatsman">';
				for (i=0;i<battingResponse.length;i++) {
					if(battingResponse[i].batOrder == 0 && battingResponse[i].elevenType == 1) {
						battingOrderHtml+='<a href="/profiles?playerId='+battingResponse[i].playerId+'">'+battingResponse[i].name+'</a><span>, </span>';
					}
				}
			battingOrderHtml+='</div>';
		battingOrderHtml+='</div>';
		$('.scoreCardInfoCont').append(battingOrderHtml);
	}
}	

function renderBowlingInnings(bowlingResponse,bowlingTeamName) {
	
	bowlingResponse = bowlingResponse.sort(function(a, b){
		return b.bowlOrder - a.bowlOrder;
	});
	if(bowlingResponse[0].bowlOrder != 0 ) {
		bowlingResponse = bowlingResponse.sort(function(a, b){
			return a.bowlOrder - b.bowlOrder;
		});
		var	bowlingStatusBar =['Bowler','O','M','R','W','NB','WD','ECO'];
		var bowlingHtml = '<div class = "bowlingStatusBar" >'+
								'<div class="bowler">'+bowlingStatusBar[0]+'</div>';
								 for(i=1;i<bowlingStatusBar.length;i++) {
									 bowlingHtml+='<div class="bowlstatusBar">'+bowlingStatusBar[i]+'</div>';
								 }
						   bowlingHtml+='</div>';
		$('.scoreCardInfoCont').append(bowlingHtml);
		var bowlingOrderHtml = '';
		for (i=0;i<bowlingResponse.length;i++) {
			if(bowlingResponse[i].bowlOrder != 0 ) {
				bowlingOrderHtml+='<div class = "bowlingStatus" >'+
										'<div class="bowler"><a href="/profiles?playerId='+bowlingResponse[i].playerId+'">'+bowlingResponse[i].name+'</a></div>'+
										'<div class="bowlstatusBar">'+Math.floor(bowlingResponse[i].ballsBowled/6)+'.'+(bowlingResponse[i].ballsBowled%6)+'</div>'+
										'<div class="bowlstatusBar">'+bowlingResponse[i].maidens+'</div>'+
										'<div class="bowlstatusBar">'+bowlingResponse[i].runsGiven+'</div>'+
										'<div class="bowlstatusBar">'+bowlingResponse[i].wickets+'</div>'+
										'<div class="bowlstatusBar">'+bowlingResponse[i].noBalls+'</div>'+
										'<div class="bowlstatusBar">'+bowlingResponse[i].wides+'</div>'+
										'<div class="bowlstatusBar">';
											if(bowlingResponse[i].runsGiven == 0){
												bowlingOrderHtml+=0;
											}else 
											{
												bowlingOrderHtml+=(bowlingResponse[i].runsGiven/((bowlingResponse[i].ballsBowled/6)+(bowlingResponse[i].ballsBowled%6))).toFixed(2);
											}
											bowlingOrderHtml+='</div>'+
									'</div>';
			}	
		}
		$('.scoreCardInfoCont').append(bowlingOrderHtml);
	}
}

function renderMatchInfoDetails(response,teamAresponse,teamAName,teamBresponse,teamBName) {
	var toss = (response.tossWonBy == response.teamAId)?response.teamAName : response.teamBName;
	var elected = (response.electedToBat == 1)?"opted to bat":"elected to field";
	var dateTime = new Date(response.dateTime);
	var date = dateTime.toDateString();
	var time = dateTime.toLocaleTimeString();
	var arr = time.split(':')
	var Meridiem = arr[2].split(" ");
	var matchDetailHtml = '<div class="innings">Match Info</div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Match</div><div class="detailsNameCont">'+response.teamANickName +" vs "+ response.teamBNickName+' ('+response.matchNo+')</div></div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Date</div><div class="detailsNameCont">'+date+'</div></div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Time</div><div class="detailsNameCont">'+arr[0].trim()+':'+arr[1].trim()+' '+Meridiem[1]+'</div></div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Venue</div><div class="detailsNameCont">'+response.venue+'</div></div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Toss</div><div class="detailsNameCont">'+toss+' won the toss and '+elected+'</div></div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Umpires</div><div class="detailsNameCont">'+response.umpire1+','+response.umpire2+'</div></div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Third Umpire/Scorer</div><div class="detailsNameCont">'+response.scorer+'</div></div>';
	if(response.teamWon != null){
		matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Result</div><div class="detailsNameCont">'+response.matchStatus+'</div></div>';
	}
	if(response.teamWon != null){
		matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Man Of The Match</div><div class="detailsNameCont"><a href="/profiles?playerId='+response.mom+'">'+response.momName+'</a></div></div>';
	}
	matchDetailHtml+='<div class="detailsTeamName">'+teamAName+' Squard</div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Playing XI</div><div class="detailsNameCont">';
	for(i=0;i<teamAresponse.length;i++){
		if(teamAresponse[i].elevenType == 1){
			matchDetailHtml+='<a href="/profiles?playerId='+teamAresponse[i].playerId+'">'+teamAresponse[i].name+'</a><span>, </span>';
		}
	}
	matchDetailHtml+='</div></div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Subs</div><div class="detailsNameCont">';
	for(i=0;i<teamAresponse.length;i++){
		if(teamAresponse[i].elevenType == 0){
			matchDetailHtml+='<a href="/profiles?playerId='+teamAresponse[i].playerId+'">'+teamAresponse[i].name+'</a><span>, </span>';
		}
	}
	matchDetailHtml+='</div></div>';
	matchDetailHtml+='<div class="detailsTeamName">'+teamBName+' Squard</div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Playing XI</div><div class="detailsNameCont">';
	for(i=0;i<teamBresponse.length;i++){
		if(teamBresponse[i].elevenType == 1){
			matchDetailHtml+='<a href="/profiles?playerId='+teamBresponse[i].playerId+'">'+teamBresponse[i].name+'</a><span>, </span>';
		}
	}
	matchDetailHtml+='</div></div>';
	matchDetailHtml+='<div class="matchInfo"><div class="detailsName">Subs</div><div class="detailsNameCont">';
	for(i=0;i<teamBresponse.length;i++){
		if(teamBresponse[i].elevenType == 0){
			matchDetailHtml+='<a href="/profiles?playerId='+teamBresponse[i].playerId+'">'+teamBresponse[i].name+'</a><span>, </span>';
		}
	}
	matchDetailHtml+='</div></div>';
	$('.scoreCardInfoCont').append(matchDetailHtml);
}


