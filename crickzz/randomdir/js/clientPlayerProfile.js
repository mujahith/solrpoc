$(document).ready(function()
{
	function _calculateAge(birthday) { // birthday is a date
		var ageDifMs = Date.now() - birthday.getTime();
		var ageDate = new Date(ageDifMs); // miliseconds from epoch
		return Math.abs(ageDate.getUTCFullYear() - 1970);
	}
	if(playerData && playerData.data && playerData.data.playerProfile && playerData.data.playerProfile[0] && playerData.data.playerProfile[0].dob != "")
	{
		var dateTime = new Date(playerData.data.playerProfile[0].dob);
		var date = dateTime.toDateString();
		var arr1 = date.split(' ');
		$('.playerDOB').text(arr1[1]+" "+arr1[2]+", "+arr1[3]+" ("+_calculateAge(dateTime)+" Years)");
	}
});
